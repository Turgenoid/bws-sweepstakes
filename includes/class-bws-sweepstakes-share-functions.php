<?php
class BWS_Sweepstakes_Share_Functions {
    public static $option_name = 'bws_sweepstakes';

    /**
     * Get vendors ids and zip-codes
     *
     * @return array
     */
    public static function get_vendors( $simple_prizes_strict = false, $needle_time = 'current' ) {

        $users = get_users();
        $vendors = array();
        $options_zip_codes = get_option( 'bws_sweepstakes_zip_codes_values' );

        foreach ( $users as $user ) {

            if( in_array( 'vendor', $user->roles ) || in_array( 'administrator', $user->roles ) ) {

                $xprofile_zip_code = xprofile_get_field_data(5, $user->ID);

                $vendor = new stdClass();
                $vendor->id = $user->ID;
                $vendor->zip_code = $xprofile_zip_code;

                $prizes = self::get_prizes( $vendor->id, $simple_prizes_strict, $needle_time );

                if ( count( $prizes ) == 0 && $simple_prizes_strict === true ) {
                    continue;
                }

                if( count( $prizes ) > 0 ) {
                    $vendor->prizes = $prizes;
                }

                $products = self::get_products( $vendor->id );

                if( count( $products ) > 0 ) {
                    $vendor->products = $products;
                }

                foreach ( $options_zip_codes as $state => $cities ) {

                    foreach ( $cities as $city => $zip_codes ) {

                        foreach( $zip_codes as $zip_code ) {

                            if( $vendor->zip_code == $zip_code ) {

                                $vendor->city = $city;
                                $vendor->state = $state;

                                break 3;

                            }

                        }

                    }

                }

                if( empty( $vendor->city ) || empty ( $vendor->state ) ) {
                    continue;
                }

                $vendors[] = $vendor;

            }

        }

        return $vendors;

    }

    public static function get_vendors_by_zip( $simple_prizes_strict, $zip_code_needle, $needle_time ) {

        $vendors = self::get_vendors( $simple_prizes_strict, $needle_time );

        foreach ( $vendors as $key => $vendor ) {

            if( $vendor->zip_code != $zip_code_needle ) {
                unset( $vendors[$key] );
            }

        }

        return $vendors;

    }

    public static function get_vendors_by_city_state( $simple_prizes_strict, $city_needle, $state_needle, $needle_time ) {

        $vendors = self::get_vendors( $simple_prizes_strict, $needle_time );

        foreach ( $vendors as $key => $vendor ) {

            if( $vendor->city != $city_needle || $vendor->state != $state_needle ) {
                unset( $vendors[$key] );
            }

        }

        return $vendors;

    }

    /**
     * Get contestants
     *
     * @return array
     */
    public static function get_contestants() {

        $users = get_users();
        $contestants = array();
        $options_zip_codes = get_option( 'bws_sweepstakes_zip_codes_values' );

        foreach ( $users as $user ) {

            if ( in_array( 'subscriber', $user->roles ) ) {

                $xprofile_zip_code = xprofile_get_field_data(5, $user->ID);
                $first_name = xprofile_get_field_data(1, $user->ID);

                $contestant = new stdClass();
                $contestant->id = $user->ID;
                $contestant->zip_code = $xprofile_zip_code;
                $contestant->points = mycred_get_users_balance($user->ID);
                $contestant->first_name = $first_name;

                foreach ( $options_zip_codes as $state => $cities ) {

                    foreach ( $cities as $city => $zip_codes ) {

                        foreach( $zip_codes as $zip_code ) {

                            if ( $contestant->zip_code == $zip_code ) {

                                $contestant->city = $city;
                                $contestant->state = $state;

                                break 3;

                            }

                        }

                    }

                }

                if( empty( $contestant->city ) || empty ( $contestant->state ) || $contestant->points == 0 ) {
                    continue;
                }

                $contestants[] = $contestant;

            }

        }

        return $contestants;

    }

    public static function get_contestants_by_zip( $zip_code_needle ) {

        $contestants = self::get_contestants();

        foreach ( $contestants as $key => $contestant ) {

            if( $contestant->zip_code != $zip_code_needle ) {
                unset( $contestants[$key] );
            }

        }

        return $contestants;

    }

    public static function get_contestants_by_city_state( $city_needle, $state_needle ) {

        $contestants = self::get_contestants();

        foreach ( $contestants as $key => $contestant ) {

            if( $contestant->city != $city_needle || $contestant->state != $state_needle ) {
                unset( $contestants[$key] );
            }

        }

        return $contestants;

    }

    /**
     * Get three-dimensional Zip-Code array filtered by existing vendors Zip-Codes
     *
     * @param null $vendors
     * @return array
     */
    public static function get_zip_codes_by_vendors( $vendors = null ) {

        if ( is_null( $vendors ) ) {
            $vendors = self::get_vendors();
        }

        $zip_codes_option = get_option( 'bws_sweepstakes_zip_codes_values' );

        // Get existing vendors zip-codes
        $existing_zip_codes = array();

        foreach ( $vendors as $vendor ) {

            $prizes = self::get_prizes( $vendor->id, true );

            if ( count( $prizes ) > 0 ) {
                if ( !in_array( $vendor->zip_code, $existing_zip_codes ) && $vendor->zip_code != "" ) {
                    $existing_zip_codes[] = $vendor->zip_code;
                }
            }

        }

        // Filter state and cities by existing zip-codes & prizes
        $zip_codes_filtered = array();

        foreach ( $zip_codes_option as $state => $cities ) {

            foreach ( $cities as $city => $zip_codes ) {

                foreach ( $zip_codes as $zip_code ) {

                    if ( in_array( $zip_code, $existing_zip_codes ) ) {

                        $zip_codes_filtered[$state][$city][] = $zip_code;

                    }

                }

            }

        }

        return $zip_codes_filtered;

    }

    /**
     * Get three-dimensional Zip-Code array filtered by existing contestants Zip-Codes
     *
     * @param null $contestants
     * @return array
     */
    public static function get_zip_codes_by_contestants( $contestants = null ) {

        if ( is_null( $contestants ) ) {
            $contestants = self::get_contestants();
        }

        $zip_codes_option = get_option( 'bws_sweepstakes_zip_codes_values' );

        // Get existing contestants zip-codes
        $existing_zip_codes = array();

        foreach ( $contestants as $contestant ) {

            if ( strlen( $contestant->zip_code ) > 0 ) {
                if ( !in_array( $contestant->zip_code, $existing_zip_codes ) && $contestant->zip_code != "" ) {
                    $existing_zip_codes[] = $contestant->zip_code;
                }
            }

        }

        // Filter state and cities by existing zip-codes
        $zip_codes_filtered = array();

        foreach ( $zip_codes_option as $state => $cities ) {

            foreach ( $cities as $city => $zip_codes ) {

                foreach ( $zip_codes as $zip_code ) {

                    if ( in_array( $zip_code, $existing_zip_codes ) ) {

                        $zip_codes_filtered[$state][$city][] = $zip_code;

                    }

                }

            }

        }

        return $zip_codes_filtered;

    }

    /**
     * Add zeros to the left Zip-Code side
     *
     * @param $zip_code
     * @return string
     */
    public static function zip_code_add_zeros( $zip_code ) {

        $zip_code = strval( $zip_code );
        $zero_count = 5 - strlen( $zip_code );
        $zeros = "";

        for ( $digit_number = 0; $digit_number < $zero_count; $digit_number++ ) {

            $zeros .= "0";

        }

        $zip_code = $zeros . $zip_code;

        return $zip_code;

    }

    /**
     * Returns data object about Zip-Code
     * @field zip_code
     * @field city
     * @field state
     *
     * Or returns false if Zip-Code does not exist
     *
     * @param $zip_code_needle
     * @param null $zip_codes_values
     * @return bool|string
     */
    public static function get_zip_code_data( $zip_code_needle, $zip_codes_values = null ) {

        if ( is_null( $zip_codes_values ) ) {
            $zip_codes_values = get_option('bws_sweepstakes_zip_codes_values');
        }

        $zip_code_found = false;

        foreach ( $zip_codes_values as $state => $cities ) {
            foreach ( $cities as $city => $zip_codes ) {
                foreach ( $zip_codes as $zip_code ) {

                    if ( $zip_code_needle == $zip_code ) {

                        $zip_code_found = true;
                        break 3;
                    }

                }
            }
        }

        if ( $zip_code_found ) {

            $zip_code_object = "";
            $zip_code_object->zip_code = $zip_code;
            $zip_code_object->city = $city;
            $zip_code_object->state = $state;

            return $zip_code_object;

        } else {

            return false;

        }

    }

    public static function get_prizes( $vendor_id, $simple_prizes_strict, $needle_time ) {

        /*$meta_query = array(
            'relation' => 'OR',
            array(
                'key'       => '_simple_prize',
                'value'     => 'yes',
                'compare'   =>  '=='
            ),
            array(
                'key'       => '_external_prize',
                'value'     => 'yes',
                'compare'   =>  '=='
            )
        );*/

        $simple_prizes = get_posts( array(
            'author'        =>  $vendor_id,
            'post_type'     =>  'product',
            'meta_key'      =>  '_simple_prize',
            'meta_value'    =>  'yes',
        ) );

        $external_prizes = get_posts( array(
            'author'        =>  $vendor_id,
            'post_type'     =>  'product',
            'meta_key'      =>  '_external_prize',
            'meta_value'    =>  'yes',
            'orderby'       =>  'date',
            'order'         =>  'ASC',
        ) );

        // Filter simple prizes by remain count and end date
        foreach ( $simple_prizes as $key => $prize ) {

            $drawing_frequency = get_post_meta( $prize->ID,'bws_sweepstakes_drawing_frequency', true );
            $drawing_end_date = get_post_meta( $prize->ID,'bws_sweepstakes_drawing_end_date', true );
            $endless_drawing = get_post_meta( $prize->ID,'bws_sweepstakes_endless_drawing', true );
            $drawing_last_run_date = get_post_meta( $prize->ID,'bws_sweepstakes_last_drawing_date', true );
            $drawing_interval = $drawing_frequency * 604800 - 86400;

            //var_dump(empty( $drawing_frequency )) /*|| $drawing_frequency == 'stop' || ( isset( $drawing_end_date ) && $drawing_end_date < time() && $drawing_frequency != 'stop' )*/;

            if ( empty( $drawing_frequency ) || $drawing_frequency == 'stop' || ( isset( $drawing_end_date ) && $drawing_end_date < time() && $endless_drawing != 'yes' ) ) {
                unset( $simple_prizes[$key] );
                continue;
            }

            if( $needle_time != 'current' ) {

                if ( $simple_prizes_strict && !empty( $drawing_last_run_date ) && $needle_time - $drawing_interval < $drawing_last_run_date ) {
                    unset( $simple_prizes[$key] );
                }

            } else {

                if ( $simple_prizes_strict && !empty( $drawing_last_run_date ) && time() - $drawing_interval < $drawing_last_run_date ) {
                    unset( $simple_prizes[$key] );
                }

            }

        }

        $prizes = array_merge( $simple_prizes, $external_prizes );

        return $prizes;

    }

    public static function get_products( $vendor_id ) {

        // TODO: Fix meta_query for get_posts
        $meta_query = array(
            array(
                'key'       => '_simple_prize',
                'value'     => 'no',
                'compare'   => 'LIKE'
            ),
            array(
                'key'       => '_external_prize',
                'value'     => 'no',
                'compare'   => 'LIKE'
            )
        );

        $products = get_posts( array(
            'author'        =>  $vendor_id,
            'post_type'     =>  'product',
            'meta_query'    =>  $meta_query
        ) );

        /*foreach ( $products as $key => $product ) {

            if( get_post_meta( $product->ID, '_simple_prize', true ) == 'yes' || get_post_meta( $product->ID, '_external_prize', true ) == 'yes' ) {
                unset( $products[$key] );
            }

        }*/

        return $products;

    }

    public static function get_user_winnings( $user_id ) {

        $drawings = BWS_Sweepstakes_Drawing::get_all_drawings_from_db();
        $prizes = array();

        foreach ( $drawings as $drawing ) {

            if ( $drawing->get_state() == 'temp' ) {
                continue;
            }

            $winners_and_prizes = $drawing->get_winnings();

            if ( key_exists( $user_id, $winners_and_prizes ) ) {

                $drawing_received_prizes = $drawing->get_received_prizes();

                foreach ( $drawing_received_prizes as $prize_id => $order_id ) {

                    if ( !in_array( $prize_id, $winners_and_prizes[$user_id] ) ) {
                        continue;
                    }

                    if ( $order_id === false ) {

                        $prizes[] = array(
                            'prize_id' => $prize_id,
                            'drawing_id' => $drawing->get_id(),
                            'drawing_time' => $drawing->get_time(),
                        );

                    } else if ( $order_id !== false && get_post_meta( $prize_id, '_woo_vou_enable', true) == 'yes' ) {

                        $download_link = "";

                        foreach ( wc_get_order( $order_id )->get_downloadable_items() as $item ) {

                            if ( $item['product_id'] == $prize_id ) {
                                $download_link = $item['download_url'];
                                break;
                            }

                        }

                        $prizes[] = array(
                            'prize_id' => $prize_id,
                            'drawing_id' => $drawing->get_id(),
                            'drawing_time' => $drawing->get_time(),
                            'link_to_download' => $download_link,
                        );

                    }

                }

            }

        }

        return $prizes;

    }

    /**
     * Returns an array of vouchers by user id
     *
     * @param $user_id
     * @return array
     */
    public static function get_user_vouchers( $user_id ) {
        global $woo_vou_voucher;

        // Get completed orders by $user_id
        $user_orders_posts = get_posts( array(
            'numberposts' => -1,
            'meta_key'    => '_customer_user',
            'meta_value'  => $user_id,
            'post_type'   => wc_get_order_types(),
            'post_status' => 'Completed',
        ) );

        // Get orders
        $orders = array();
        foreach( $user_orders_posts as $user_order_post  ) {
            $order      = wc_get_order( $user_order_post->ID );
            $orders[]   = $order;
        }

        // Get vouchers
        $vouchers = array();
        foreach ( $orders as $order ) {
            foreach ( $order->get_downloadable_items() as $item ) {

                if ( get_post_meta( $item['product_id'], '_woo_vou_enable', true ) == 'yes' ) {

                    $voucher = array();

                    $order_id = $order->get_id();

                    $purchased_codes = $woo_vou_voucher->woo_vou_get_purchased_codes_by_product_id( $item['product_id'] );

                    foreach ( $purchased_codes as $pc ) {
                        if ( $pc['order_id'] != $order_id ) {
                            continue;
                        }

                        $voucher['codes'] = explode( ', ', $pc['vou_codes'] );

                        $voucher['purchase_date'] = $pc['order_date'];
                    }

                    if ( count( $voucher['codes'] ) == 0 ) {
                        continue;
                    }

                    $voucher['product_id'] = $item['product_id'];

                    $vendor_id = get_post_meta( get_post( $item['product_id'] )->ID, '_woo_vou_vendor_user', true );
                    $voucher['vendor_address'] = get_user_meta( $vendor_id, '_woo_vou_address_phone', true );
                    $voucher['store'] = get_user_meta( $vendor_id, 'pv_shop_name', true );

                    $voucher['download_url'] = $item['download_url'];
                    $voucher['product_name'] = $item['product_name'];

                    $voucher_id = $item['download_id'][12];
                    $voucher['code'] = $voucher['codes'][$voucher_id-1];
                    $voucher['status'] = $woo_vou_voucher->woo_vou_get_voucher_code_status( $voucher['code'] );

                    $expiration_date = get_post_meta( $item['product_id'], '_woo_vou_exp_date', true );
                    $expiration_date = empty( $expiration_date ) ? 'Never' : $expiration_date;
                    $voucher['expiration_date'] = $expiration_date;

                    unset( $voucher['codes'] );

                    $vouchers[] = $voucher;

                }

            }

        }

        return $vouchers;

    }

    public static function save_products_meta_fields( $post_id ) {

        // SIMPLE PRIZES

        if ( isset( $_POST['_simple_prize'] ) ) {

            $drawing_frequency = $_POST[self::$option_name . '_drawing_frequency'];
            $drawing_end_date_str = $_POST[self::$option_name . '_drawing_end_date'];
            $endless_drawing = $_POST[self::$option_name . '_endless_drawing'];
            $drawing_end_date = strtotime( $drawing_end_date_str );

            $drawing_frequency_old = get_post_meta( $post_id, self::$option_name . '_drawing_frequency', true );
            $drawing_end_date_old = get_post_meta( $post_id, self::$option_name . '_drawing_end_date', true );
            $endless_drawing_old = get_post_meta( $post_id, self::$option_name . '_endless_drawing', true );

            if ( !empty( $drawing_end_date_old ) && !empty( $drawing_frequency_old ) && !empty( $endless_drawing_old ) && $drawing_frequency == $drawing_frequency_old && $drawing_end_date == $drawing_end_date_old && $endless_drawing == $endless_drawing_old ) {
                return;
            }

            update_post_meta( $post_id, '_simple_prize', 'yes' );
            update_post_meta( $post_id, self::$option_name . '_drawing_last_run_date', 0 );
            update_post_meta( $post_id, self::$option_name . '_drawing_frequency', $drawing_frequency );

            if ( isset(  $_POST[self::$option_name . '_endless_drawing'] ) ) {
                update_post_meta( $post_id, self::$option_name . '_endless_drawing', 'yes' );
            } else {
                update_post_meta( $post_id, self::$option_name . '_endless_drawing', 'no' );
            }

            update_post_meta( $post_id, self::$option_name . '_drawing_end_date', $drawing_end_date );

        } else {

            update_post_meta( $post_id, '_simple_prize', 'no' );

        }

        // EXTERNAL PRIZES

        if ( isset( $_POST['_external_prize'] ) ) {

            update_post_meta( $post_id, '_external_prize', 'yes' );

        } else {

            update_post_meta( $post_id, '_external_prize', 'no' );

        }

        // DISCOUNT PERCENTAGE

        if ( isset( $_POST['_regular_price'] ) && isset( $_POST['_sale_price'] ) && $_POST['_regular_price'] != "" && $_POST['_sale_price'] != "" ) {

            $discount_percentage = 100 - round( ( $_POST['_sale_price'] * 100 ) / $_POST['_regular_price'] );
            update_post_meta( $post_id, '_discount_percentage', $discount_percentage );

        } else {

            update_post_meta( $post_id, '_discount_percentage', 0 );

        }

    }

}