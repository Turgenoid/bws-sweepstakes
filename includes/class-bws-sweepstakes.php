<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/includes
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */
class BWS_Sweepstakes {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      BWS_Sweepstakes_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'BWS_SWEEPSTAKES_VERSION' ) ) {
			$this->version = BWS_SWEEPSTAKES_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'bws-sweepstakes';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - BWS_Sweepstakes_Loader. Orchestrates the hooks of the plugin.
	 * - BWS_Sweepstakes_i18n. Defines internationalization functionality.
	 * - BWS_Sweepstakes_Admin. Defines all hooks for the admin area.
	 * - BWS_Sweepstakes_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bws-sweepstakes-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bws-sweepstakes-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-bws-sweepstakes-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-bws-sweepstakes-public.php';

        /**
         * The class responsible for working with Drawings.
         */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bws-sweepstakes-drawing.php';

        /**
         * The class responsible for defining all support functions that occur in the admin area and public-facing.
         */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-bws-sweepstakes-share-functions.php';

		$this->loader = new BWS_Sweepstakes_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the BWS_Sweepstakes_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new BWS_Sweepstakes_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new BWS_Sweepstakes_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		// Menus
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'add_menu' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_drawings_settings' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_manual_drawings_settings' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_zip_codes_settings' );
		$this->loader->add_action( 'admin_post_handle_zip_codes_settings', $plugin_admin, 'handle_zip_codes_settings' );
        $this->loader->add_action( 'admin_init', $plugin_admin, 'register_plugin_pages_settings' );
        $this->loader->add_action( 'admin_init', $plugin_admin, 'register_users_points_settings' );
        $this->loader->add_action( 'admin_post_handle_awards_points_settings', $plugin_admin, 'handle_awards_points_settings' );

        // Automatic Drawings
        $this->loader->add_filter( 'cron_schedules', $plugin_admin, 'register_weekly_cron' );

        // WooCommerce
        $this->loader->add_action( 'woocommerce_product_options_general_product_data', $plugin_admin, 'add_simple_prize_custom_fields' );
        $this->loader->add_action( 'woocommerce_product_options_general_product_data', $plugin_admin, 'add_external_prize_custom_fields' );
        $this->loader->add_filter( 'product_type_options', $plugin_admin, 'add_prize_product_type' );
        $this->loader->add_action( 'woocommerce_process_product_meta', $plugin_admin, 'save_products_meta_fields' );
        $this->loader->add_action( 'woocommerce_process_product_meta', $plugin_admin, 'save_terms_for_external_prize' );
        $this->loader->add_action( 'init', $plugin_admin, 'custom_taxonomy_city' );

        // Drawing Notifications
        $this->loader->add_action( 'bp_core_install_emails', $plugin_admin, 'register_winner_notification_template' );
        $this->loader->add_action( 'bp_core_install_emails', $plugin_admin, 'register_vendor_notification_template' );
        $this->loader->add_action( 'bp_core_install_emails', $plugin_admin, 'register_contestant_notification_template' );

        // Broadcast Messages
        $this->loader->add_action( 'bp_core_install_emails', $plugin_admin, 'register_broadcast_message_to_vendor_template' );
        $this->loader->add_action( 'bp_core_install_emails', $plugin_admin, 'register_broadcast_message_to_contestant_template' );

        // MyCRED Points Notifications
        $this->loader->add_action( 'bp_core_install_emails', $plugin_admin, 'register_notification_about_receive_points_template' );

        // Send Drawing Notifications
        $this->loader->add_action( 'bws_sweepstakes_after_drawing', $plugin_admin, 'send_winner_notification', 1, 1 );
        $this->loader->add_action( 'bws_sweepstakes_after_drawing', $plugin_admin, 'send_vendors_notifications', 2, 1 );
        $this->loader->add_action( 'bws_sweepstakes_after_drawing', $plugin_admin, 'send_contestants_notifications', 3, 1 );

        // Send Broadcast Messages
        $this->loader->add_action( 'admin_post_handle_broadcast_message', $plugin_admin, 'handle_broadcast_message' );

        // Send MyCRED Points Notifications
        $this->loader->add_action( 'mycred_update_user_balance', $plugin_admin, 'send_notification_about_receive_points', 10, 4 );

	    // Disable activation code
        $this->loader->add_filter( 'bp_registration_needs_activation', $plugin_admin, 'get_false' );

        // Save manual drawings
        if( is_admin() ) {
            $this->loader->add_action( 'wp_ajax_save_manual_drawings', $plugin_admin, 'save_manual_drawings_cb' );
            $this->loader->add_action( 'wp_ajax_get_manual_drawings', $plugin_admin, 'get_manual_drawings_cb' );
        }

        $this->loader->add_action( 'wp_ajax_resend_notifications_for_certain_winner', $plugin_admin, 'resend_notifications_for_certain_winner_cb' );

        // Deprecated
        //$this->loader->add_action( 'signup_referral', $plugin_admin, 'save_ref_user_id', 10, 3 );
        //$this->loader->add_action( 'admin_print_footer_scripts', $plugin_admin, 'admin_add_wysiwyg_terms_textarea', 99 );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new BWS_Sweepstakes_Public( $this->get_plugin_name(), $this->get_version() );

		// Styles & Scripts
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'login_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		// Shortcodes
        $this->loader->add_action( 'init', $plugin_public, 'register_shortcodes' );

        // Custom Pages
        $this->loader->add_filter( 'body_class', $plugin_public, 'add_class_for_body_users_dashboard' );
        $this->loader->add_filter( 'body_class', $plugin_public, 'add_class_for_body_voucher_tracking_page' );
        $this->loader->add_filter( 'body_class', $plugin_public, 'add_class_for_body_sales_page' );
        $this->loader->add_filter( 'woocommerce_login_redirect', $plugin_public, 'redirect_users_after_login_to_dashboard' );
        $this->loader->add_filter( 'login_redirect', $plugin_public, 'redirect_after_buddypress_login' );

		// MyCRED features
        $this->loader->add_filter( 'mycred_setup_hooks', $plugin_public, 'mycred_add_hooks' );
        $this->loader->add_action( 'mycred_load_hooks', $plugin_public, 'mycred_load_hooks' );
        $this->loader->add_action( 'woocommerce_payment_complete', $plugin_public, 'mycred_reward_for_purchasing' );

        // WooCommerce settings
        $this->loader->add_filter( 'woocommerce_show_page_title', $plugin_public, 'hide_shop_title' );
        $this->loader->add_filter( 'loop_shop_columns', $plugin_public, 'get_loop_columns_count', 1, 10 );
        $this->loader->add_action( 'woocommerce_product_tabs', $plugin_public, 'add_product_terms_tab' );
        $this->loader->add_filter( 'woocommerce_loop_add_to_cart_link', $plugin_public, 'delete_buy_button_for_external_prizes_loop', 10, 3 );

        // Filter Prizes and Products
        $shops_permalink = get_option( 'wcvendors_vendor_shop_permalink' );
        if ( strpos( $_SERVER['REQUEST_URI'], $shops_permalink ) === false ) {
            $this->loader->add_action( 'woocommerce_product_query', $plugin_public, 'filter_prizes' );
            $this->loader->add_action( 'woocommerce_shortcode_products_query', $plugin_public, 'filter_products' );
        }

        /*add_action( 'init', function() use ($plugin_public) {

            $shops_permalink = get_option( 'wcvendors_vendor_shop_permalink' );
            if ( strpos( $_SERVER['REQUEST_URI'], $shops_permalink ) === false && is_tax( 'city' ) === false ) {
                add_action( 'woocommerce_product_query', array( $plugin_public, 'filter_prizes' ) );
                add_action( 'woocommerce_shortcode_products_query', array( $plugin_public, 'filter_products' ) );
            }

        } );*/

        //wp_mail( 'turgenoid@gmail.com', 'test tax', print_r( is_tax( 'city' ), true ) );

        // Products Purchasing
        $this->loader->add_filter( 'woocommerce_is_purchasable', $plugin_public, 'customize_purchasable', 20, 2 );
        $this->loader->add_filter( 'woocommerce_add_cart_item_data', $plugin_public, 'add_cart_item_data_for_zero_price', 10, 3 );
        $this->loader->add_action( 'woocommerce_before_calculate_totals', $plugin_public, 'set_zero_price_before_calculate_totals', 10, 1 );
        $this->loader->add_action( 'woocommerce_checkout_order_processed', $plugin_public, 'mark_prize_checkout_process', 10, 1 );
        $this->loader->add_action( 'woocommerce_checkout_create_order_line_item', $plugin_public, 'add_drawing_id_to_order_meta_data', 10, 4 );
        $this->loader->add_action( 'woocommerce_add_to_cart', $plugin_public, 'limit_multiple_prizes', 10, 6 );
        $this->loader->add_action( 'woocommerce_thankyou', $plugin_public, 'redirect_after_checkout' );

        // WC Vendors settings
        $this->loader->add_action( 'wcv_save_product', $plugin_public, 'save_products_meta_fields' );

        // Automatic Drawings
        $this->loader->add_action( 'drawings_cron', $plugin_public, 'run_drawings' );

        // Manual Drawings
        $this->loader->add_action( 'wp_ajax_get_certain_manual_drawing', $plugin_public, 'get_certain_manual_drawing_cb' );
        $this->loader->add_action( 'wp_ajax_save_certain_manual_drawing', $plugin_public, 'save_certain_manual_drawing_cb' );
        $this->loader->add_action( 'wp_ajax_send_notifications_for_certain_manual_drawing', $plugin_public, 'send_notifications_for_certain_manual_drawing_cb' );

        // Modifications for WooCommerce PDF Vouchers
        $this->loader->add_action( 'woo_vou_check_qrcode_top', $plugin_public, 'woo_vou_add_redeem_btn_on_top' );
        $this->loader->add_filter( 'woo_vou_pdf_template_inner_html', $plugin_public,'replace_drawing_date_shortcode_in_vaucher' , 10, 6 );
        $this->loader->add_filter( 'woo_vou_pdf_template_inner_html', $plugin_public, 'display_vendor_shop_name_in_voucher', 10, 6 );
        $this->loader->add_filter( 'woo_vou_unlimited_voucher_code', $plugin_public, 'custom_unlimited_vou_code', 10, 2 );

        // For Debug
        //$this->loader->add_action( 'wp_head', $plugin_public, 'debug' );
        //$this->loader->add_filter( 'woo_vou_unlimited_code_pattern', $plugin_public, 'check_vou_args' );

        // Deprecated
        //$this->loader->add_filter( 'query_vars', $plugin_public, 'add_query_vars' );
        //$this->loader->add_action( 'widgets_init', $plugin_public, 'add_widgets' );

    }

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    BWS_Sweepstakes_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
