<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/includes
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */
class BWS_Sweepstakes_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

        self::create_db_tables();

	}

	public static function create_db_tables() {

        require_once('class-bws-sweepstakes-drawing-settings.php');
        require_once('class-bws-sweepstakes-drawing.php');

        BWS_Sweepstakes_Drawing::create_table_in_db();
        BWS_Sweepstakes_Drawing_Settings::create_tables_in_db();

        add_option('bws_sweepstakes_db_version', BWS_SWEEPSTAKES_VERSION);

    }

}
