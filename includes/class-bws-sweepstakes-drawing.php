<?php

require_once('class-bws-sweepstakes-drawing-settings.php');

class BWS_Sweepstakes_Drawing {
    private $id;
    private $place;
    private $vendors = array();
    private $contestants = array();
    private $prizes = array();
    private $received_prizes = array();
    private $winnings;
    private $time;
    private $state;

    public static $drawings_table_prename = 'bws_sweepstakes_drawings';

    /*** STATIC METHODS ***/
    public static function create_table_in_db() {

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        global $wpdb;

        $drawing_table_name = $wpdb->prefix . self::$drawings_table_prename;
        $charset_collate = $wpdb->get_charset_collate();

        $sql = "CREATE TABLE " . $drawing_table_name . " (
          id bigint NOT NULL AUTO_INCREMENT,
          place tinytext NOT NULL,
          vendors longtext NOT NULL,
          contestants longtext NOT NULL,
          prizes longtext NOT NULL,
          received_prizes text NOT NULL,
          winnings text NOT NULL,
          time int(10) NOT NULL,
          state tinytext NOT NULL,
          UNIQUE KEY id (id)
        ) $charset_collate;";

        dbDelta($sql);

    }

    public static function create_new_drawing( $place, $vendors, $contestants, $state ) {

        $drawing = new BWS_Sweepstakes_Drawing();

        $drawing->set_place( $place );
        $drawing->set_vendors( $vendors );
        $drawing->set_contestants( $contestants );
        $drawing->add_prizes_from_vendors();
        $drawing->filter_external_prizes();
        $drawing->init_received_prizes();
        $drawing->find_winners();
        $drawing->time = time();
        $drawing->state = $state;

        return $drawing;

    }

    public function save_drawing_to_db() {

        global $wpdb;
        $drawings_table_name = $wpdb->prefix . self::$drawings_table_prename;

        $place_prepared = addslashes( serialize( $this->place ) );
        $vendors_prepared = addslashes( serialize( $this->vendors ) );
        $contestants_prepared = addslashes( serialize( $this->contestants ) );
        $prizes_prepared = addslashes( serialize( $this->prizes ) );
        $received_prizes_prepared = addslashes( serialize( $this->received_prizes ) );
        $winnings_prepared = addslashes( serialize( $this->winnings ) );


        $this->mark_prizes_last_drawing_date();

        $wpdb->insert( $drawings_table_name, array(
            'place'             =>  $place_prepared,
            'vendors'           =>  $vendors_prepared,
            'contestants'       =>  $contestants_prepared,
            'prizes'            =>  $prizes_prepared,
            'received_prizes'   =>  $received_prizes_prepared,
            'winnings'          =>  $winnings_prepared,
            'time'              =>  $this->time,
            'state'             =>  $this->state,
        ), array(
            '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s',
        ) );

        $this->id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM " . $drawings_table_name . " WHERE time = %d AND place = %s", $this->time, $place_prepared ) );

        /*$ds = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( $this->place );
        $ap_number = $ds->get_last_ap_id();

        if( $ap_number > count( $this->get_external_prizes() ) - 1 || $ap_number == 0 ) {
            $ap_number = 1;
        } else {
            $ap_number++;
        }

        $ds->set_last_ap_id( $ap_number );
        $ds->save_drawing_settings_to_db();*/

        if ( $this->state == 'done' ) {
            $ds = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( $this->place );
            $last_ap_id = $ds->get_last_ap_id();

            $external_prizes = $this->get_external_prizes();
            if ( $last_ap_id == 0 || $last_ap_id == $external_prizes[count($external_prizes)-1] ) {
                $external_prize_pos = 1;
            } else {
                $last_ap_pos = array_search( $last_ap_id, $external_prizes );
                $external_prize_pos = $last_ap_pos+1;
            }

            $ds->set_last_ap_id( $external_prizes[$external_prize_pos] );
            $ds->save_drawing_settings_to_db();
        }

        return $this->id;

    }

    private static function load_drawing( $row ) {

        $id = $row->id;
        $place = unserialize( stripslashes( $row->place ) );
        $vendors = unserialize( stripslashes( $row->vendors ) );
        $contestants = unserialize( stripslashes( $row->contestants ) );
        $prizes = unserialize( stripslashes( $row->prizes ) );
        $received_prizes = unserialize( stripslashes( $row->received_prizes ) );
        $winnings = unserialize( stripslashes( $row->winnings ) );
        $time = $row->time;
        $state = $row->state;

        $drawing = new BWS_Sweepstakes_Drawing();

        $drawing->set_id( $id );
        $drawing->set_place( $place );
        $drawing->set_vendors( $vendors );
        $drawing->set_contestants( $contestants );
        $drawing->set_prizes( $prizes );
        $drawing->set_received_prizes( $received_prizes );
        $drawing->set_winnings( $winnings );
        $drawing->set_time( $time );
        $drawing->set_state( $state );

        return $drawing;

    }

    public static function get_drawing_from_db( $drawing_id ) {

        global $wpdb;
        $drawings_table_name = $wpdb->prefix . self::$drawings_table_prename;
        $drawing_row = $wpdb->get_row( 'SELECT * FROM ' . $drawings_table_name . ' WHERE id = ' . $drawing_id );

        $drawing = self::load_drawing( $drawing_row );

        return $drawing;

    }

    public static function get_manual_drawings() {

        global $wpdb;
        $cds_table_name = $wpdb->prefix . BWS_Sweepstakes_Drawing_Settings::$cds_table_prename;
        $zds_table_name = $wpdb->prefix . BWS_Sweepstakes_Drawing_Settings::$zds_table_prename;

        $drawings_settings = array();

        /* Get Settings for Manual Drawings */

        // City
        $cds_results = $wpdb->get_results( "SELECT * FROM " . $cds_table_name . " WHERE type = 'manual'" );

        foreach ( $cds_results as $cds_row ) {

            $drawings_settings[] = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( array(
                'city'  => $cds_row->city,
                'state' => $cds_row->state,
            ) );

        }

        // Zip
        $zds_results = $wpdb->get_results( "SELECT * FROM " . $zds_table_name . " WHERE type = 'manual'" );

        foreach ( $zds_results as $zds_row ) {

            $drawings_settings[] = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( array(
                'zip'  => $zds_row->zip,
            ) );

        }

        /* Exclude not today Drawings */

        foreach ( $drawings_settings as $key => $ds ) {

            $boundaries = $ds->get_next_drawing_day_boundaries();
            $current_time = time();

            if ( $current_time > $boundaries['end_day'] || $current_time < $boundaries['begin_day'] ) {
                unset( $drawings_settings[$key] );
            }

        }

        /* Get Drawings for the places found */

        $drawings = array();

        foreach ( $drawings_settings as $ds ) {

            $boundaries = $ds->get_next_drawing_day_boundaries();
            $place = $ds->get_place();

            if ( !empty( $place['zip'] ) ) {



            } else if ( !empty( $place['city'] ) && !empty( $place['state'] ) ) {

                $vendors = BWS_Sweepstakes_Share_Functions::get_vendors_by_city_state( true, $place['city'], $place['state'], $boundaries['planned_time'] );
                $contestants = BWS_Sweepstakes_Share_Functions::get_contestants_by_city_state( $place['city'], $place['state'] );

                if ( count( $vendors ) == 0 || count( $contestants ) == 0 ) {
                    continue;
                }

                $drawings[] = self::create_new_drawing( $place, $vendors, $contestants );

            }

        }

        return $drawings;

    }

    public static function get_manual_drawings_for_widget() {

        global $wpdb;
        $cds_table_name = $wpdb->prefix . BWS_Sweepstakes_Drawing_Settings::$cds_table_prename;
        $zds_table_name = $wpdb->prefix . BWS_Sweepstakes_Drawing_Settings::$zds_table_prename;

        $drawings_settings = array();

        /* Get Settings for Manual Drawings */

        // City
        $cds_results = $wpdb->get_results( "SELECT * FROM " . $cds_table_name . " WHERE type = 'manual'" );

        foreach ( $cds_results as $cds_row ) {

            $drawings_settings[] = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( array(
                'city'  => $cds_row->city,
                'state' => $cds_row->state,
            ) );

        }

        // Zip
        $zds_results = $wpdb->get_results( "SELECT * FROM " . $zds_table_name . " WHERE type = 'manual'" );

        foreach ( $zds_results as $zds_row ) {

            $drawings_settings[] = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( array(
                'zip'  => $zds_row->zip,
            ) );

        }

        /* Exclude not today Drawings */

        foreach ( $drawings_settings as $key => $ds ) {

            $day = $ds->get_next_date()['day'];

            if ( $day != date( 'l' ) ) {
                unset( $drawings_settings[$key] );
            }

        }

        /* Get Drawings for the places found */

        $drawings = array();

        foreach ( $drawings_settings as $ds ) {

            $boundaries = $ds->get_next_drawing_day_boundaries();
            $place = $ds->get_place();

            if ( !empty( $place['zip'] ) ) {



            } else if ( !empty( $place['city'] ) && !empty( $place['state'] ) ) {

                $vendors = BWS_Sweepstakes_Share_Functions::get_vendors_by_city_state( true, $place['city'], $place['state'], $boundaries['planned_time'] );
                $contestants = BWS_Sweepstakes_Share_Functions::get_contestants_by_city_state( $place['city'], $place['state'] );

                if ( count( $vendors ) == 0 || count( $contestants ) == 0 ) {
                    continue;
                }

                $drawings[] = self::create_new_drawing( $place, $vendors, $contestants );

            }

        }

        return $drawings;

    }

    public static function get_drawing_id_by_boundaries( array $place, $day_timestamp ) {

        global $wpdb;

        $boundaries = array();
        $boundaries['begin_day'] = strtotime( 'midnight', $day_timestamp );
        $boundaries['end_day'] = strtotime( 'tomorrow', $boundaries['begin_day'] ) - 1;

        $drawings_table_name = $wpdb->prefix . self::$drawings_table_prename;
        $place_prepared = addslashes( serialize( $place ) );
        $drawing_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM " . $drawings_table_name . " WHERE time > %d AND time < %d AND place = %s", $boundaries['begin_day'], $boundaries['end_day'], $place_prepared ) );


        if ( is_null( $drawing_id ) ) {
            return false;
        } else {
            return $drawing_id;
        }

    }

    /**
     * Returns a drawing for the certain place
     *
     * @param $place
     */
    public static function get_next_drawing_by_place( array $place ) {

        $ds = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( $place );
        $boundaries = $ds->get_next_drawing_day_boundaries();
        $vendors = array();
        $contestants = array();

        if ( !empty( $place['zip'] ) ) {

            $vendors = BWS_Sweepstakes_Share_Functions::get_vendors_by_zip( true, $place['zip'], $boundaries['planned_time'] );
            $contestants = BWS_Sweepstakes_Share_Functions::get_contestants_by_zip( $place['zip'] );

        } else if ( !empty( $place['city'] ) && !empty( $place['state'] ) ) {

            $vendors = BWS_Sweepstakes_Share_Functions::get_vendors_by_city_state( true, $place['city'], $place['state'], $boundaries['planned_time'] );
            $contestants = BWS_Sweepstakes_Share_Functions::get_contestants_by_city_state( $place['city'], $place['state'] );

        }

        if ( count( $vendors ) == 0 || count( $contestants ) == 0 ) {
            return null;
        }


        $drawing = self::create_new_drawing( $place, $vendors, $contestants, 'temp' );

        return $drawing;

    }

    /*public static function get_next_manual_drawing_by_city_state( $city, $state ) {

        global $wpdb;
        $zip_codes = BWS_Sweepstakes_Share_Functions::get_zip_codes_by_contestants();
        $zip = $zip_codes[$state][$city][0];
        $settings = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( $zip, $city, $state );

        if( $settings->get_drawing_range() == 'zip' ) {
            return false;
        }

        $vendors = BWS_Sweepstakes_Share_Functions::get_vendors_by_city_state( true, $city, $state, $settings->get_next_drawing_timestamp() );
        $contestants = BWS_Sweepstakes_Share_Functions::get_contestants_by_city_state( $city, $state );
        $drawing = BWS_Sweepstakes_Drawing::create_new_drawing( $zip, $city, $state, $vendors, $contestants, 'city' );

        return $drawing;

    }

    /**
     * Get today's manual drawings
     *
    public static function get_today_manual_drawings() {

        global $wpdb;
        $drawings_table_name = $wpdb->prefix . self::$drawings_table_prename;
        $ds = BWS_Sweepstakes_Drawing_Settings::get_manual_drawings_settings();
        $drawings = array();

        foreach ( $ds as $key => $settings ) {

            if( unserialize( stripslashes( $settings->next_drawing_date ) )['day'] != date( 'l' ) ) {
                unset( $ds[$key] );
            }

        }

        $begin_day = strtotime( "midnight", time() );
        $end_day   = strtotime( "tomorrow", $begin_day ) - 1;

        foreach ( $ds as $key => $settings ) {

            $drawing_id = $wpdb->get_var( "SELECT id FROM " . $drawings_table_name . " WHERE city = '" . $settings->city . "' AND state = '" . $settings->state . "' AND drawing_time > " . $begin_day . " AND drawing_time < " . $end_day );
            if( !is_null( $drawing_id ) ) {

                unset( $ds[$key] );

            } else {

                $vendors = BWS_Sweepstakes_Share_Functions::get_vendors_by_city_state( true, $settings->city, $settings->state, 'current' );
                $contestants = BWS_Sweepstakes_Share_Functions::get_contestants_by_city_state( $settings->city, $settings->state );
                $drawing = self::create_new_drawing( $settings->zip_code, $settings->city, $settings->state, $vendors, $contestants, $settings->drawing_range );
                $drawings[] = $drawing;

            }

        }

        return $drawings;

    }*/

    /**
     * Get array of BWS_Sweepstakes_Drawing objects. They are actual drawings at current moment
     */
    public static function get_drawings( $simple_prizes_strict = true, $needle_time = 'current' ) {

        $drawings = array();

        $vendors = BWS_Sweepstakes_Share_Functions::get_vendors( $simple_prizes_strict, $needle_time );
        $contestants = BWS_Sweepstakes_Share_Functions::get_contestants();

        $drawings_places = array();

        foreach ( $vendors as $vendor ) {

            if ( !property_exists($vendor, 'prizes' ) ) {
                continue;
            }

            $contestants_fit = array();

            $ds = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( $vendor->zip_code, $vendor->city, $vendor->state );
            $drawing_range = $ds->get_drawing_range();

            if ( $drawing_range == 'zip' ) {

                foreach ( $contestants as $contestant ) {

                    if ( $vendor->zip_code == $contestant->zip_code && $contestant->points > 0 ) {

                        $contestants_fit[] = $contestant;

                    }

                }

            } else if ( $drawing_range == 'city' ) {

                foreach ( $contestants as $contestant ) {

                    if ( $vendor->city == $contestant->city && $vendor->state == $contestant->state && $contestant->points > 0 ) {

                        $contestants_fit[] = $contestant;

                    }

                }

            }

            if ( count( $contestants_fit ) == 0 ) {
                continue;
            }

            if ( !array_key_exists($vendor->zip_code, $drawings_places ) ) {

                $drawing_place = array(
                    'city'  =>  $vendor->city,
                    'state' =>  $vendor->state,
                    'range' =>  $drawing_range,
                );
                $drawings_places[$vendor->zip_code] = $drawing_place;

            }

        }

        foreach ( $drawings_places as $zip_code => $place ) {

            if( $place['range'] == 'zip' ) {

                $drawings[] = self::create_new_drawing( $zip_code, $place['city'], $place['state'], BWS_Sweepstakes_Share_Functions::get_vendors_by_zip( $simple_prizes_strict, $zip_code, $needle_time ), BWS_Sweepstakes_Share_Functions::get_contestants_by_zip( $zip_code ), $place['range'] );

            } else if( $place['range'] == 'city' ) {

                $drawings[] = self::create_new_drawing($zip_code, $place['city'], $place['state'], BWS_Sweepstakes_Share_Functions::get_vendors_by_city_state( $simple_prizes_strict, $place['city'], $place['state'], $needle_time ), BWS_Sweepstakes_Share_Functions::get_contestants_by_city_state( $place['city'], $place['state'] ), $place['range'] );

            }

        }

        return $drawings;

    }

    /**
     * Load certain drawings from the database
     *
     * @param $drawings_ids
     * @return array
     */
    public static function get_certain_drawings( $drawings_ids ) {

        foreach ( $drawings_ids as $drawing_id ) {

            $drawings[] = self::get_drawing_from_db( $drawing_id );

            return $drawings;

        }

    }

    public static function get_all_drawings_from_db() {

        global $wpdb;
        $drawings_table_name = $wpdb->prefix . self::$drawings_table_prename;
        $drawings_results = $wpdb->get_results( 'SELECT * FROM ' . $drawings_table_name, OBJECT_K );
        $drawings = array();

        foreach ( $drawings_results as $drawing_row ) {

            $drawing = self::load_drawing( $drawing_row );
            $drawings[] = $drawing;

        }

        return $drawings;

    }

    public static function is_drawing_exists( $id ) {

        global $wpdb;
        $drawings_table_name = $wpdb->prefix . self::$drawings_table_prename;
        $id = $wpdb->get_var( $wpdb->prepare( "SELECT * FROM " . $drawings_table_name . " WHERE id = %s AND state = %s", $id, 'done' ) );

        if ( empty( $id ) ) {
            return false;
        } else {
            return true;
        }

    }


    /*** PUBLIC METHODS ***/
    public function set_id( $id ) {
        $this->id = $id;
    }

    public function get_id() {
        return $this->id;
    }

    public function set_place( $place ) {
        $this->place = $place;
    }

    public function get_place() {
        return $this->place;
    }

    public function get_vendors() {
        return $this->vendors;
    }

    public function set_vendors( $vendors ) {
        $this->vendors = $vendors;
    }

    public function get_vendor_by_id( $vendor_id ) {

        foreach ( $this->vendors as $vendor ) {
            if ( $vendor->id == $vendor_id ) {
                return $vendor;
            }
        }

    }

    public function set_contestants( $contestants ) {
        $this->contestants = $contestants;
    }

    public function get_contestants() {
        return $this->contestants;
    }

    public function get_contestant_by_id( $contestant_id ) {

        foreach ( $this->contestants as $contestant ) {
            if ( $contestant->id == $contestant_id ) {
                return $contestant;
            }
        }

    }

    /**
     * Get prizes from @vendors field
     */
    public function add_prizes_from_vendors() {

        foreach ( $this->vendors as &$vendor ) {
            $vendors_prizes_ids = array();

            foreach ( $vendor->prizes as $prize ) {
                $this->prizes[] = $prize->ID;
                $vendors_prizes_ids[] = $prize->ID;
            }

            $vendor->prizes = $vendors_prizes_ids;
            unset( $vendor->products );

        }

    }

    public function set_prizes( $prizes ) {
        $this->prizes = $prizes;
    }

    public function get_prizes() {
        return $this->prizes;
    }

    public function mark_prizes_last_drawing_date() {

        foreach ( $this->prizes as $prize ) {
            update_post_meta( $prize, 'bws_sweepstakes_last_drawing_date', $this->time );
        }

    }

    public function set_received_prizes( $received_prizes ) {
        $this->received_prizes = $received_prizes;
    }

    public function init_received_prizes() {

        $received_prizes = array();

        foreach ( $this->prizes as $prize ) {
            $received_prizes[$prize] = false;
        }

        $this->received_prizes = $received_prizes;

    }

    public function update_received_prizes() {

        global $wpdb;
        $drawings_table_name = $wpdb->prefix . self::$drawings_table_prename;

        if ( empty( $this->id ) ) {
            return false;
        }

        $received_prizes = addslashes( serialize( $this->received_prizes ) );

        $result = $wpdb->update( $drawings_table_name,
            array( 'received_prizes' => $received_prizes ),
            array( 'id' => $this->id ),
            array( '%s' ),
            array( '%d' )
        );

        return $result;

    }

    public function get_received_prizes() {
        return $this->received_prizes;
    }

    public function set_time( $time ) {
        $this->time = $time;
    }

    public function get_time() {
        return $this->time;
    }

    public function set_winnings( $winnings ) {
        $this->winnings = $winnings;
    }

    /*public function find_auto_winner() {

        $total_points = 0;

        foreach ( $this->contestants as $contestant ) {
            $total_points += $contestant->points;
        }

        // Set win chances
        foreach ( $this->contestants as &$contestant ) {
            $contestant->chance = ( $contestant->points ) * 100 / $total_points;
        }

        // Set win intervals
        for ( $i = 0; $i < count( $this->contestants ); $i++ ) {

            if ( $i == 0 ) {
                $this->contestants[$i]->win_interval = array( 0, $this->contestants[$i]->chance );
            } else if ( $i == count( $this->contestants ) - 1 && count( $this->contestants ) > 1 ) {
                $this->contestants[$i]->win_interval = array( $this->contestants[$i-1]->win_interval[1], 100 );
            } else {
                $this->contestants[$i]->win_interval = array( $this->contestants[$i-1]->win_interval[1], $this->contestants[$i]->chance + $this->contestants[$i-1]->win_interval[1] );
            }

        }

        // Choose a winner
        $win_number = random_int( 1, 100000000000 ) / 1000000000;
        for ( $i = 0; $i < count ( $this->contestants ); $i++ ) {
            if ( $win_number > $this->contestants[$i]->win_interval[0] && $win_number <= $this->contestants[$i]->win_interval[1] ) {
                $winner_id = $this->contestants[$i]->id;
            }
        }

        $this->winners_id = $winner_id;

    }

    public function find_manual_winners( $winners_count ) {

        $contestants = $this->contestants;
        $winners_id = array();

        if( count( $contestants ) < $winners_count ) {
            $winners_count = count( $contestants );
        }

        for ( $j = 0; $j < $winners_count; $j++ ) {

            $total_points = 0;

            foreach ( $contestants as $contestant ) {
                $total_points += $contestant->points;
            }

            // Set win chances
            foreach ( $contestants as &$contestant ) {
                $contestant->chance = ( $contestant->points ) * 100 / $total_points;
            }

            // Set win intervals
            for ( $i = 0; $i < count( $contestants ); $i++ ) {

                if ( $i == 0 ) {
                    $contestants[$i]->win_interval = array( 0, $contestants[$i]->chance );
                } else if ( $i == count($contestants ) - 1 && count( $contestants ) > 1 ) {
                    $contestants[$i]->win_interval = array( $contestants[$i - 1]->win_interval[1], 100 );
                } else {
                    $contestants[$i]->win_interval = array( $contestants[$i - 1]->win_interval[1], $contestants[$i]->chance + $contestants[$i - 1]->win_interval[1] );
                }

            }

            // Choose a winner
            $win_number = random_int( 1, 100000000000 ) / 1000000000;
            for ( $i = 0; $i < count( $contestants ); $i++ ) {
                if ( $win_number > $contestants[$i]->win_interval[0] && $win_number <= $contestants[$i]->win_interval[1] ) {
                    $winner_id = $contestants[$i]->id;
                }
            }

            // Delete winner from alternates
            foreach ( $contestants as $key => $contestant ) {
                if( $winner_id == $contestant->id ) {
                    unset( $contestants[$key] );
                }
            }

            $winners_id[] = $winner_id;

        }

        $this->winners_id = $winners_id;

    }*/

    public function find_winners() {

        $contestants = $this->get_contestants();
        $prizes = $this->prizes;
        $winnings = array();
        $winners_count = count( $contestants );


        if ( $winners_count >= count( $prizes ) ) {

            $winnings = $this->find_winnings( $prizes, $contestants, $winners_count );

        } else if ( $winners_count < count( $prizes ) ) {

            $chunk_prizes = array_chunk( $prizes, $winners_count );

            // Get winnings for each part of prizes
            foreach ( $chunk_prizes as $chunk ) {

                $chunk_winnings = $this->find_winnings( $chunk, $contestants, $winners_count );

                // Add chunk winnings to the whole winnings
                foreach ( $chunk_winnings as $key => $winning ) {

                    if ( is_array( $winnings[$key] ) ) {
                        $winnings[$key] = array_merge( $winnings[$key], $chunk_winnings[$key] );
                    } else {
                        $winnings[$key] = $chunk_winnings[$key];
                    }


                }

            }

        }

        $this->winnings = $winnings;

    }


    private function find_winnings(array $prizes, array $contestants, $winners_count ) {

        $winnings = array();

        // Cloning
        foreach ( $contestants as $contestant ) {
            $contestants_safe[] = clone $contestant;
        }

        foreach ( $prizes as $prize ) {

            $contestants_safe = array_values( $contestants_safe );
            if( count( $contestants_safe ) == 1 ) {

                $winnings[$contestants_safe[0]->id][] = $prize;
                continue;

            }

            // Get sum of the points for all contestants
            $total_points = 0;
            foreach ( $contestants_safe as $contestant ) {
                $total_points += $contestant->points;
            }

            // Set win chances
            foreach ($contestants_safe as $contestant ) {
                $contestant->chance = ( $contestant->points ) * 100 / $total_points;
            }

            // Set win intervals
            for ( $i = 0; $i < $winners_count; $i++ ) {

                if ( $i == 0 ) {
                    $contestants_safe[$i]->win_interval = array( 0, $contestants_safe[$i]->chance );
                } else if ( $i == count( $contestants_safe ) - 1 && count( $contestants_safe ) > 1 ) {
                    $contestants_safe[$i]->win_interval = array( $contestants_safe[$i - 1]->win_interval[1], 100 );
                } else {
                    $contestants_safe[$i]->win_interval = array( $contestants_safe[$i - 1]->win_interval[1], $contestants_safe[$i]->chance + $contestants_safe[$i - 1]->win_interval[1] );
                }

            }

            // Choose a winner
            $win_number = random_int( 1, 100000000000 ) / 1000000000;
            for ( $i = 0; $i < $winners_count; $i++ ) {
                if ( $win_number > $contestants_safe[$i]->win_interval[0] && $win_number <= $contestants_safe[$i]->win_interval[1] ) {
                    $winner_id = $contestants_safe[$i]->id;
                }
            }

            // Delete winner from the array of contestants
            foreach ($contestants_safe as $key => $contestant ) {
                if( $winner_id == $contestant->id ) {
                    unset( $contestants_safe[$key] );
                }
            }

            $winnings[$winner_id][] = $prize;

        }

        return $winnings;

    }

    public function get_winnings() {
        return $this->winnings;
    }

    public function get_external_prizes() {

        $external_prizes = array();

        foreach ( $this->prizes as $prize ) {

            if( get_post_meta( $prize, '_external_prize', true ) == 'yes' ) {
                $external_prizes[] = $prize;
            }

        }

        return $external_prizes;

    }

    public function get_winners_without_prizes() {

        $winnings = $this->winnings;
        $received_prizes = $this->received_prizes;
        $winners = array();

        foreach ( $winnings as $winner_id => $prizes ) {

            foreach ($prizes as $prize ) {

                if ( $received_prizes[$prize] === false ) {
                    $winners[$winner_id][] = $prize;
                }

            }

        }

        return $winners;

    }

    public function set_state($state ) {
        $this->state = $state;
    }

    public function get_state() {
        return $this->state;
    }

    public function update_state( $state ) {

        global $wpdb;
        $drawings_table_name = $wpdb->prefix . self::$drawings_table_prename;

        if ( empty( $this->id ) ) {
            return false;
        }

        if( $state == 'done' ) {
            $this->set_next_last_ap_id();
        }

        $result = $wpdb->update( $drawings_table_name,
            array( 'state' => $state ),
            array( 'id' => $this->id ),
            array( '%s' ),
            array( '%d' )
        );

        return $result;

    }

    public function filter_external_prizes() {

        $ds = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( $this->place );
        $last_ap_id = $ds->get_last_ap_id();
        $external_prizes = $this->get_external_prizes();

        if ( $last_ap_id == 0 || $last_ap_id == $external_prizes[count($external_prizes)-1] ) {
            $external_prize_pos = 0;
        } else {
            $last_ap_pos = array_search( $last_ap_id, $external_prizes );
            $external_prize_pos = $last_ap_pos+1;
        }

        unset( $external_prizes[$external_prize_pos] );

        foreach ( $external_prizes as $external_prize ) {

            while ( false !== $key = array_search( $external_prize, $this->prizes ) ) {

                unset( $this->prizes[$key] );

            }

        }

    }

    private function set_next_last_ap_id() {

        $ds = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( $this->place );
        $last_ap_id = $ds->get_last_ap_id();
        $external_prizes = $this->get_external_prizes();

        if ( $last_ap_id == 0 || $last_ap_id == $external_prizes[count($external_prizes)-1] ) {
            $external_prize_pos = 0;
        } else {
            $last_ap_pos = array_search( $last_ap_id, $external_prizes );
            $external_prize_pos = $last_ap_pos+1;
        }

        $next_last_ap_id = $external_prizes[$external_prize_pos];

        $ds->set_last_ap_id( $next_last_ap_id );
        $ds->save_drawing_settings();

    }

}