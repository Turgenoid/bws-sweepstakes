<?php

class BWS_Sweepstakes_Drawing_Settings {
    private $id;
    private $place;
    private $type;
    private $next_date;
    private $last_ap_id;

    public static $cds_table_prename = 'bws_sweepstakes_city_drawings_settings';
    public static $zds_table_prename = 'bws_sweepstakes_zip_drawings_settings';

    /**
     * Create tables in the db for Drawings Settings
     */
    public static function create_tables_in_db() {

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();

        $cds_table_name = $wpdb->prefix . self::$cds_table_prename;

        $sql = "CREATE TABLE " . $cds_table_name . " (
          id bigint NOT NULL AUTO_INCREMENT,
          city tinytext NOT NULL,
          state tinytext NOT NULL,
          type tinytext NOT NULL,
          next_date tinytext NOT NULL,
          last_ap_id int NOT NULL,
          UNIQUE KEY id (id)
        ) $charset_collate;";

        dbDelta($sql);

        $zds_table_name = $wpdb->prefix . self::$zds_table_prename;

        $sql = "CREATE TABLE " . $zds_table_name . " (
          id bigint NOT NULL AUTO_INCREMENT,
          zip tinytext NOT NULL,
          type tinytext NOT NULL,
          next_date tinytext NOT NULL,
          last_ap_id int NOT NULL,
          UNIQUE KEY id (id)
        ) $charset_collate;";

        dbDelta($sql);

    }

    /*public static function create_new_drawing_settings( $zip_code, $city, $state, $next_drawing_date, $drawing_range, $ap_number ) {

        $ds = new BWS_Sweepstakes_Drawing_Settings();
        $ds->set_zipCode( $zip_code );
        $ds->set_city( $city );
        $ds->set_state( $state );
        $ds->set_next__date( $next_drawing_date );
        $ds->set_drawing_range( $drawing_range );
        $ds->set_last_ap_id( $ap_number );

        return $ds;

    }*/

    /**
     * Creates New Drawing Settings
     *
     * @param array $place
     * @param $type
     * @param $next_date
     * @param $last_ap_id
     * @return BWS_Sweepstakes_Drawing_Settings
     */
    public static function create_new_drawing_settings( array $place, $type, $next_date, $last_ap_id ) {

        $ds = new BWS_Sweepstakes_Drawing_Settings();
        $ds->set_place( $place );
        $ds->set_type( $type );
        $ds->set_next_date( $next_date );
        $ds->set_last_ap_id( $last_ap_id );

        return $ds;

    }

    /**
     * Saves the Drawing Settings to the DB
     */
    public function save_drawing_settings() {

        global $wpdb;

        if( is_array( $this->next_date ) ) {
            $next_date_prepared = addslashes( serialize( $this->next_date ) );
        } else {
            $next_date_prepared = $this->next_date;
        }

        if( !empty( $this->place['zip'] ) ) {

            $zds_table_name = $wpdb->prefix . self::$zds_table_prename;
            $ds_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM " . $zds_table_name . " WHERE zip = %d", $this->place['zip'] ) );

        } else if( !empty( $this->place['city'] ) && !empty( $this->place['state'] ) ) {

            $cds_table_name = $wpdb->prefix . self::$cds_table_prename;
            $ds_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM " . $cds_table_name . " WHERE city = %s AND state = %s", $this->place['city'], $this->place['state'] ) );

            if( is_null( $ds_id ) ) {

                $wpdb->insert( $cds_table_name, array(
                    'city'          =>  $this->place['city'],
                    'state'         =>  $this->place['state'],
                    'type'          =>  $this->type,
                    'next_date'     =>  $next_date_prepared,
                    'last_ap_id'    =>  $this->last_ap_id,
                ), array(
                    '%s', '%s', '%s', '%s', '%d',
                ) );

                $ds_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM " . $cds_table_name . " WHERE city = %s AND state = %s", $this->place['city'], $this->place['state'] ) );
                $this->id = $ds_id;

            } else {

                $ds_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM " . $cds_table_name . " WHERE city = %s AND state = %s", $this->place['city'], $this->place['state'] ) );
                $this->id = $ds_id;

                $wpdb->update( $cds_table_name, array(
                    'type'          =>  $this->type,
                    'next_date'     =>  $next_date_prepared,
                    'last_ap_id'    =>  $this->last_ap_id,
                ), array( 'id' => $this->id ), array( '%s', '%s', '%d' ), array( '%d' ) );

            }

        }

    }

    /**
     * Get Drawing Settings for a particular place
     *
     * @param array $place
     */
    public static function get_drawing_settings( array $place ) {

        global $wpdb;

        if ( !empty( $place['zip'] ) ) {

            $zds_table_name = $wpdb->prefix . self::$zds_table_prename;
            $row = $wpdb->get_row( "SELECT * FROM " . $zds_table_name . " WHERE zip = '" . $place['zip'] . "'" );

            if ( is_null( $row ) ) {

                $ds = self::create_new_drawing_settings( $place, 'auto', 'auto', 0 );
                $ds->save_drawing_settings();

                return $ds;

            } else {

                $next_date = $row->next_date;
                if ( $next_date != 'auto' ) {
                    $next_date = unserialize( stripslashes( $next_date ) );
                }

                $ds = self::create_new_drawing_settings(
                    array( 'zip' => $row->zip ),
                    $row->type,
                    $next_date,
                    $row->last_ap_id
                );

                $ds->set_id( $row->id );

                return $ds;

            }

        } else if ( !empty( $place['city'] ) && !empty( $place['state'] ) ) {

            $cds_table_name = $wpdb->prefix . self::$cds_table_prename;
            $row = $wpdb->get_row( "SELECT * FROM " . $cds_table_name . " WHERE city = '" . $place['city'] . "' AND state = '" . $place['state'] . "'" );

            if ( is_null( $row ) ) {

                $ds = self::create_new_drawing_settings( $place, 'auto', 'auto', 0 );
                $ds->save_drawing_settings();

                return $ds;

            } else {

                $next_date = $row->next_date;
                if ( $next_date != 'auto' ) {
                    $next_date = unserialize( stripslashes( $next_date ) );
                }

                $ds = self::create_new_drawing_settings(
                    array( 'city' => $row->city, 'state' => $row->state ),
                    $row->type,
                    $next_date,
                    $row->last_ap_id
                );

                $ds->set_id( $row->id );

                return $ds;

            }

        }

    }

    public function set_id( $id ) {
        $this->id = $id;
    }

    public function get_id() {
        return $this->id;
    }

    public function set_place( $place ) {
        $this->place = $place;
    }

    public function get_place() {
        return $this->place;
    }

    public function set_type( $type ) {
        $this->type = $type;
    }

    public function get_type() {
        return $this->type;
    }

    public function set_next_date( $next_date ) {
        $this->next_date = $next_date;
    }

    public function get_next_date() {
        return $this->next_date;
    }

    public function set_last_ap_id( $last_ap_id ) {
        $this->last_ap_id = $last_ap_id;
    }

    public function get_last_ap_id() {
        return $this->last_ap_id;
    }

    /*public function get_next_drawing_timestamp() {

        global $wpdb;
        $drawings_table_name = $wpdb->prefix . BWS_Sweepstakes_Drawing::$drawings_table_prename;

        if( !empty( $this->id ) ) {

            return false;

        } else if( $this->next_date == 'auto' ) {

            return wp_next_scheduled( 'drawings_cron' );

        } else if( is_array( $this->next_date ) ) {

            if( $this->next_date['day'] == date( 'l' ) ) {

                $day_timestamp = time();

            } else {

                $day_timestamp = strtotime( 'next ' . $this->next_date['day'] );

            }

            $day_str = date( 'M j', $day_timestamp );
            $next_drawing_time = strtotime( $day_str . ' ' . $this->next_date['time'] );

            $begin_day = strtotime( "midnight", $next_drawing_time );
            $end_day   = strtotime( "tomorrow", $begin_day ) - 1;

            $drawing_id = $wpdb->get_var( "SELECT id FROM " . $drawings_table_name . " WHERE city = '" . $this->city . "' AND state = '" . $this->state . "' AND drawing_time > " . $begin_day . " AND drawing_time < " . $end_day );

            if( is_null( $drawing_id ) ) {

                return $next_drawing_time;

            } else {

                $day_timestamp = strtotime( 'next ' . $this->next_date['day'] );
                $day_str = date( 'M j', $day_timestamp );
                $next_drawing_time = strtotime( $day_str . ' ' . $this->next_date['time'] );

                return $next_drawing_time;

            }

        } else {

            return false;

        }

    }*/

    /**
     * Returns array of boundaries for the next drawing day
     *
     * @return array
     */
    public function get_next_drawing_day_boundaries() {

        global $wpdb;

        $boundaries = array();

        if ( $this->next_date == 'auto' ) {

            $next_drawing_timestamp = wp_next_scheduled( 'drawings_cron' );
            $boundaries['begin_day']    = strtotime( 'midnight', $next_drawing_timestamp );
            $boundaries['planned_time'] = $next_drawing_timestamp;
            $boundaries['end_day']      = strtotime( 'tomorrow', $boundaries['begin_day'] ) - 1;

        } else if ( is_array( $this->next_date ) ) {

            if ( $this->next_date['day'] == date('l') ) {
                $day_timestamp = time();
            } else {
                $day_timestamp = strtotime( 'next ' . $this->next_date['day'] );
            }

            $day_str = date( 'M j', $day_timestamp );
            $next_drawing_timestamp = strtotime( $day_str . ' ' . $this->next_date['time'] );

            $boundaries['begin_day']    = strtotime( 'midnight', $next_drawing_timestamp );
            $boundaries['planned_time'] = $next_drawing_timestamp;
            $boundaries['end_day']      = strtotime( 'tomorrow', $boundaries['begin_day'] ) - 1;

            $drawings_table_name = $wpdb->prefix . BWS_Sweepstakes_Drawing::$drawings_table_prename;

            $place_prepared = addslashes( serialize( $this->place ) );
            $drawing_id = $wpdb->get_var( "SELECT id FROM " . $drawings_table_name . " WHERE place = '" . $place_prepared . "' AND time > " . $boundaries['begin_day'] . " AND time < " . $boundaries['end_day'] );

            if ( !is_null( $drawing_id ) ) {

                $day_timestamp = strtotime( 'next ' . $this->next_date['day'] );
                $day_str = date( 'M j', $day_timestamp );
                $next_drawing_timestamp = strtotime( $day_str . ' ' . $this->next_date['time'] );

                $boundaries['begin_day']    = strtotime( 'midnight', $next_drawing_timestamp );
                $boundaries['planned_time'] = $next_drawing_timestamp;
                $boundaries['end_day']      = strtotime( 'tomorrow', $boundaries['begin_day'] ) - 1;

            }

        }

        return $boundaries;

    }

    /*public static function get_drawing_settings( $zip_code, $city, $state ) {

        global $wpdb;
        $ds_table_name = $wpdb->prefix . self::$cds_table_prename;
        $ds_city_row = $wpdb->get_row( "SELECT * FROM " . $ds_table_name . " WHERE city = '" . $city . "' AND state = '" . $state . "' AND drawing_range = 'city'" );

        if( is_null( $ds_city_row ) ) {

            $ds_zip_row = $wpdb->get_row( "SELECT * FROM " . $ds_table_name . " WHERE zip = '" . $zip_code . "' AND drawing_range = 'zip'" );

            if( is_null( $ds_zip_row ) ) {

                $ds = self::create_new_drawing_settings( $zip_code, $city, $state, 'auto', 'zip', 0 );
                $ds->save_drawing_settings_to_db();

                return $ds;

            } else {

                $next_drawing_date = $ds_zip_row->next_drawing_date;
                if ( $next_drawing_date != 'auto' ) {
                    $next_drawing_date = unserialize( stripslashes( $next_drawing_date ) );
                }

                $ds = self::create_new_drawing_settings( $ds_zip_row->zip_code, $ds_zip_row->city, $ds_zip_row->state, $next_drawing_date, $ds_zip_row->drawing_range, $ds_zip_row->ap_number );

                return $ds;

            }

        } else {

            $next_drawing_date = $ds_city_row->next_drawing_date;
            if ( $next_drawing_date != 'auto' ) {
                $next_drawing_date = unserialize( stripslashes( $next_drawing_date ) );
            }

            $ds = self::create_new_drawing_settings( $ds_city_row->zip_code, $ds_city_row->city, $ds_city_row->state, $next_drawing_date, $ds_city_row->drawing_range, $ds_city_row->ap_number );

            return $ds;

        }

    }*/

    /*public static function get_manual_drawings_settings() {

        global $wpdb;
        $ds_table_name = $wpdb->prefix . self::$cds_table_prename;
        $md_settings = $wpdb->get_results( "SELECT * FROM " . $ds_table_name . " WHERE drawing_range = 'city'" );

        return $md_settings;

    }

    public function save_drawing_settings_to_db() {

        global $wpdb;
        $ds_table_name = $wpdb->prefix . self::$cds_table_prename;
        $ds_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM " . $ds_table_name . " WHERE zip_code = %d AND city = %s AND state = %s", $this->zip_code, $this->city, $this->state ) );

        if( is_array( $this->next_date ) ) {
            $next_drawing_date_prepared = addslashes( serialize( $this->next_date ) );
        } else {
            $next_drawing_date_prepared = $this->next_date;
        }


        if( is_null( $ds_id ) ) {

            $result = $wpdb->insert( $ds_table_name, array(
                'zip_code'          =>  $this->zip_code,
                'city'              =>  $this->city,
                'state'             =>  $this->state,
                'next_drawing_date' =>  $next_drawing_date_prepared,
                'drawing_range'     =>  $this->drawing_range,
                'ap_number'         =>  $this->last_ap_id,
            ), array(
                '%s', '%s', '%s', '%s', '%s', '%d',
            ) );

            $ds_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM " . $ds_table_name . " WHERE zip_code = %d AND city = %s AND state = %s", $this->zip_code, $this->city, $this->state ) );
            $this->id = $ds_id;

        } else {

            $ds_id = $wpdb->get_var( $wpdb->prepare( "SELECT id FROM " . $ds_table_name . " WHERE zip_code = %d AND city = %s AND state = %s", $this->zip_code, $this->city, $this->state ) );
            $this->id = $ds_id;
            $update_result = $wpdb->update( $ds_table_name,
                array(
                    'next_drawing_date' =>  $next_drawing_date_prepared,
                    'drawing_range'     =>  $this->drawing_range,
                    'ap_number'         =>  $this->last_ap_id,
                ),
                array( 'id' => $this->id ),
                array( '%s' ),
                array( '%d' )
            );

        }

    }

    public function set_id( $id ) {
        $this->id = $id;
    }

    public function get_id() {
        return $this->id;
    }

    public function set_zipCode($zip_code ) {
        $this->zip_code = $zip_code;
    }

    public function get_zipCode() {
        return $this->zip_code;
    }

    public function set_city( $city ) {
        $this->city = $city;
    }

    public function get_city() {
        return $this->city;
    }

    public function set_state( $state ) {
        $this->state = $state;
    }

    public function get_state() {
        return $this->state;
    }

    public function set_next__date($next_date ) {
        $this->next_date = $next_date;
    }

    public function set_next_drawing_date_by_day_time( $day, $time ) {
        $this->next_date = array(
            'day'   =>  $day,
            'time'  =>  $time,
        );
    }

    public function get_next__date() {
        return $this->next_date;
    }*/

    /*public function set_drawing_range( $drawing_range ) {
        $this->drawing_range = $drawing_range;
    }

    public function get_drawing_range() {
        return $this->drawing_range;
    }

    public function set_last_ap_id($last_ap_id ) {
        $this->last_ap_id = $last_ap_id;
    }

    public function get_last_ap_id() {
        return $this->last_ap_id;
    }*/

}