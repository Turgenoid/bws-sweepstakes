<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/includes
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */
class BWS_Sweepstakes_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
