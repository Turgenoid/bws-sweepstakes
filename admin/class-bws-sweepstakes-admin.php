<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/admin
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */
class BWS_Sweepstakes_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    /**
     * The option name to be used in this plugin
     *
     * @var string
     */
    protected $option_name = 'bws_sweepstakes';

    /**
     * The action name for saving zip-codes
     *
     * @var string
     */
    protected $save_csv_action = 'save_zip_codes_csv';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in BWS_Sweepstakes_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The BWS_Sweepstakes_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bws-sweepstakes-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in BWS_Sweepstakes_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The BWS_Sweepstakes_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/bws-sweepstakes-admin.js', array( 'jquery' ), $this->version, false );

        if( is_admin() ) {
            wp_enqueue_script( $this->plugin_name . '-ajax-save-manual-drawings', plugin_dir_url(__FILE__) . 'js/ajax-save-manual-drawings.js', array('jquery'), $this->version, true );
            wp_enqueue_script( $this->plugin_name . '-ajax-get-manual-drawings', plugin_dir_url(__FILE__) . 'js/ajax-get-manual-drawings.js', array('jquery'), $this->version, true );
        }

	}

    /**
     * Create "BWS Sweepstakes" section in Dashboard
     */
    public function add_menu() {

	    $this->plugin_main_page_hook_suffix = add_menu_page(
	        'Sweepstakes',
            'Sweepstakes',
            'manage_options',
            'bws-sweepstakes',
            array( $this, 'display_main_page' ),
            'dashicons-tickets-alt',
            30
        );

        $this->plugin_settings_page_hook_suffix = add_submenu_page(
            'bws-sweepstakes',
            'Dashboard',
            'Dashboard',
            'manage_options',
            'bws-sweepstakes',
            array( $this, 'display_main_page' )
        );

        $this->plugin_mail_page_hook_suffix = add_submenu_page(
            'bws-sweepstakes',
            'Broadcast Messages',
            'Broadcast Messages',
            'manage_options',
            'bws-sweepstakes-mail',
            array( $this, 'display_mail_page' )
        );

	    $this->plugin_settings_page_hook_suffix = add_submenu_page(
	        'bws-sweepstakes',
	        'Settings',
            'Settings',
            'manage_options',
            'bws-sweepstakes-settings',
            array( $this, 'display_settings_page' )
        );

    }

    /**
     * Display plugin main page
     */
    public function display_main_page() {

        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/bws-sweepstakes-main-page-admin-display.php';

    }

    /**
     * Display Mail Center
     */
    public function display_mail_page() {

        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/bws-sweepstakes-mail-center-page-display.php';

    }

    /**
     * Display plugin settings
     */
    public function display_settings_page() {

        include_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/bws-sweepstakes-settings-page-admin-display.php';

    }

    /**
     * Register settings section & settings fields for Manual Drawings
     */
    public function register_manual_drawings_settings() {

        add_settings_section(
            $this->option_name . '_manual_drawings',
            'Manual Drawings',
            array( $this, 'manual_drawings_cb' ),
            $this->plugin_name . '-manual-drawings-settings'
        );

        add_settings_field(
            $this->option_name . '_manual_drawings_list',
            'Select a city',
            array( $this, 'manual_drawings_list_cb' ),
            $this->plugin_name . '-manual-drawings-settings',
            $this->option_name . '_manual_drawings',
            array( 'label_for' => $this->option_name . '_manual_drawings_list' )
        );

        add_settings_field(
            $this->option_name . '_manual_drawings_checkbox',
            'Is this a manual drawing?',
            array( $this, 'manual_drawings_checkbox_cb' ),
            $this->plugin_name . '-manual-drawings-settings',
            $this->option_name . '_manual_drawings',
            array( 'label_for' => $this->option_name . '_manual_drawings_checkbox' )
        );

        // Drawing date
        add_settings_field(
            $this->option_name . '_manual_drawings_start_day',
            'Drawing day',
            array( $this, 'manual_drawings_start_day_cb' ),
            $this->plugin_name . '-manual-drawings-settings',
            $this->option_name . '_manual_drawings',
            array( 'label_for' => $this->option_name . '_manual_drawings_start_day' )
        );

        // Drawing time
        add_settings_field(
            $this->option_name . '_manual_drawings_start_time',
            'Drawing time (UTC)',
            array( $this, 'manual_drawings_start_time_cb' ),
            $this->plugin_name . '-manual-drawings-settings',
            $this->option_name . '_manual_drawings',
            array( 'label_for' => $this->option_name . '_manual_drawings_start_time' )
        );

    }

    /**
     *  Register settings sections & settings fields for Automatic Drawings
     */
    public function register_drawings_settings() {

        add_settings_section(
            $this->option_name . '_drawings',
            'Automatic Drawings',
            array( $this, 'drawings_cb' ),
            $this->plugin_name . '-drawings-settings'
        );

        // Drawing date
        add_settings_field(
            $this->option_name . '_start_drawing_day',
            'Drawing day',
            array( $this, 'start_drawing_day_cb' ),
            $this->plugin_name . '-drawings-settings',
            $this->option_name . '_drawings',
            array( 'label_for' => $this->option_name . '_start_drawing_day' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_start_drawing_day' );

        // Drawing time
        add_settings_field(
            $this->option_name . '_start_drawing_time',
            'Drawing time (UTC)',
            array( $this, 'start_drawing_time_cb' ),
            $this->plugin_name . '-drawings-settings',
            $this->option_name . '_drawings',
            array( 'label_for' => $this->option_name . '_start_drawing_time' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_start_drawing_time' );

        // Switch (turn on / turn off)
        add_settings_field(
            $this->option_name . '_drawing_state',
            'Drawing state',
            array( $this, 'drawing_state_cb' ),
            $this->plugin_name . '-drawings-settings',
            $this->option_name . '_drawings',
            array( 'label_for' => $this->option_name . '_drawing_state' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_drawing_state' );

    }

    /**
     *  Display the drawings status
     */
    public function drawings_cb() {

        $drawing_state = get_option( $this->option_name . '_drawing_state' );
        $start_drawing_day = get_option( $this->option_name . '_start_drawing_day' );
        $start_drawing_time = get_option( $this->option_name . '_start_drawing_time' );

        // Get date for next drawing
        $current_timestamp = time();
        $start_drawing_date = strtotime( 'next ' . $start_drawing_day );

        $next_drawing_date = date( 'M j', $start_drawing_date );

        $next_timestamp_really = wp_next_scheduled( 'drawings_cron' );

        if ( $drawing_state == 'enabled' ) {

            if ( strlen( $start_drawing_day ) > 0  && strlen( $start_drawing_time ) > 0 ) {

                $next_timestamp = strtotime( $next_drawing_date . ' ' . $start_drawing_time );


                // Set the CRON TASK
                if ( $next_timestamp != $next_timestamp_really ) {

                    if ( $next_timestamp_really ) {

                        wp_unschedule_event( $next_timestamp_really, 'drawings_cron' );

                    }

                    wp_schedule_event( $next_timestamp, 'weekly', 'drawings_cron' );

                }

                echo '<p class="success">Next drawing will start at ' . $next_drawing_date . ', ' . $start_drawing_time . ' (UTC)</p>';

            } else {

                echo '<p class="info">Please, set the drawing start date & time</p>';

            }


        } else {

            // Stop the CRON TASK
            if ( $next_timestamp_really ) {

                wp_unschedule_event( $next_timestamp_really, 'drawings_cron' );

            }

            echo '<p class="error">Drawings stopped</p>';

        }

    }

    /**
     * Render the treshold drawing date input for this plugin
     */
    public function start_drawing_day_cb() {

        $drawing_day = get_option( $this->option_name . '_start_drawing_day' );

        echo "<select name='" . $this->option_name . "_start_drawing_day'>";
        echo "<option disabled>Choose a start drawing day</option>";
        echo "<option value='Monday' " . ($drawing_day == 'Monday' ? selected : '') . ">Monday</option>";
        echo "<option value='Tuesday' " . ($drawing_day == 'Tuesday' ? selected : '') . ">Tuesday</option>";
        echo "<option value='Wednesday' " . ($drawing_day == 'Wednesday' ? selected : '') . ">Wednesday</option>";
        echo "<option value='Thursday' " . ($drawing_day == 'Thursday' ? selected : '') . ">Thursday</option>";
        echo "<option value='Friday' " . ($drawing_day == 'Friday' ? selected : '') . ">Friday</option>";
        echo "<option value='Saturday' " . ($drawing_day == 'Saturday' ? selected : '') . ">Saturday</option>";
        echo "<option value='Sunday' " . ($drawing_day == 'Sunday' ? selected : '') . ">Sunday</option>";
        echo "</select>";

    }

    /**
     * Render the treshold drawing time input for this plugin
     */
    public function start_drawing_time_cb() {

        $start_drawing_time = get_option( $this->option_name . '_start_drawing_time' );
        echo '<input type="time" name="' . $this->option_name . '_start_drawing_time' . '" id="' . $this->option_name . '_start_drawing_time' . '" value="' . $start_drawing_time . '">';

    }

    /**
     * Render the treshold drawing switch radio input for this plugin
     */
    public function drawing_state_cb() {

        $drawing_state = get_option( $this->option_name . '_drawing_state' );
        $drawing_state  = isset( $drawing_state ) ? $drawing_state : 'disabled';

        ?>
        <fieldset>
            <label>
                <input type="radio" name="<?php echo $this->option_name . '_drawing_state' ?>" id="<?php echo $this->option_name . '_drawing_state' ?>" value="enabled" <?php echo $drawing_state == 'enabled' ? 'checked' : ''; ?>>
                Enabled
            </label>
            <br>
            <label>
                <input type="radio" name="<?php echo $this->option_name . '_drawing_state' ?>" value="disabled" <?php echo $drawing_state == 'disabled' ? 'checked' : ''; ?>>
                Disabled
            </label>
        </fieldset>
        <?php

    }

    /**
     * Register setting sections for Pages
     */
    public function register_plugin_pages_settings() {

        add_settings_section(
            $this->option_name . '_pages',
            'Pages',
            '', //array( $this, 'plugin_pages_cb' ),
            $this->plugin_name . '-pages-settings'
        );

        // Redirect Page
        add_settings_field(
            $this->option_name . '_redirect_page',
            'Redirect Page',
            array( $this, 'redirect_page_cb' ),
            $this->plugin_name . '-pages-settings',
            $this->option_name . '_pages',
            array( 'label_for' => $this->option_name . '_redirect_page' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_redirect_page' );

        // Users Dashboard Page
        add_settings_field(
            $this->option_name . '_users_dashboard_page',
            'Members Dashboard Page',
            array( $this, 'users_dashboard_page_cb' ),
            $this->plugin_name . '-pages-settings',
            $this->option_name . '_pages',
            array( 'label_for' => $this->option_name . '_users_dashboard_page' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_users_dashboard_page' );

        // Instructions on how to earn more points
        add_settings_field(
            $this->option_name . '_instruction_page',
            'Instruction Page for Members Dashboard',
            array( $this, 'instruction_page_cb' ),
            $this->plugin_name . '-pages-settings',
            $this->option_name . '_pages',
            array( 'label_for' => $this->option_name . '_instruction_page' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_instruction_page' );

        // Instructions on how to earn more points
        add_settings_field(
            $this->option_name . '_referral_page',
            'Referral Page',
            array( $this, 'referral_page_cb' ),
            $this->plugin_name . '-pages-settings',
            $this->option_name . '_pages',
            array( 'label_for' => $this->option_name . '_referral_page' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_referral_page' );

        // Vouchers tracking page
        add_settings_field(
            $this->option_name . '_voucher_tracking_page',
            'Voucher Tracking Page',
            array( $this, 'voucher_tracking_page_cb' ),
            $this->plugin_name . '-pages-settings',
            $this->option_name . '_pages',
            array( 'label_for' => $this->option_name . '_voucher_tracking_page' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_voucher_tracking_page' );

        // Sales Page
        add_settings_field(
            $this->option_name . '_sales_page',
            'Sales Page',
            array( $this, 'sales_page_cb' ),
            $this->plugin_name . '-pages-settings',
            $this->option_name . '_pages',
            array( 'label_for' => $this->option_name . '_sales_page' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_sales_page' );

        // Vendor Application Page
        add_settings_field(
            $this->option_name . '_vendor_application_page',
            'Vendor Application Page',
            array( $this, 'vendor_application_page_cb' ),
            $this->plugin_name . '-pages-settings',
            $this->option_name . '_pages',
            array( 'label_for' => $this->option_name . '_vendor_application_page' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_vendor_application_page' );

        // URL to Facebook Live
        add_settings_field(
            $this->option_name . '_drawing_facebook_live',
            'Facebook Live URL',
            array( $this, 'drawing_facebook_live_cb' ),
            $this->plugin_name . '-pages-settings',
            $this->option_name . '_pages',
            array( 'label_for' => $this->option_name . '_drawing_facebook_live' )
        );
        register_setting( $this->plugin_name, $this->option_name . '_drawing_facebook_live', 'validate_single_text_field' );

    }

    /**
     *  Display the field to select the Redirect Page
     */
    public function redirect_page_cb() {

        $redirect_page = get_option( $this->option_name . '_redirect_page' );

        echo "<select name='" . $this->option_name . "_redirect_page'>";
        echo "<option disabled>Choose a Redirect Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $redirect_page ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>This sets the page that will redirect users to either the Sellers Dashboard or the Members Dashboard, depending on their role. Use it in your menus. This page should contain the following shortcode. <strong>[bws_sweepstakes_redirect_page]</strong></p>';

    }

    /**
     *  Display the field to select the Users Dashboard Page
     */
    public function users_dashboard_page_cb() {

        $users_dashboard_page = get_option( $this->option_name . '_users_dashboard_page' );

        echo "<select name='" . $this->option_name . "_users_dashboard_page'>";
        echo "<option disabled>Choose a Contestants Dashboard Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $users_dashboard_page ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>This sets the page used to display the front end member dashboard. This page should contain the following shortcode. <strong>[bws_sweepstakes_members_dashboard]</strong></p>';

    }

    /**
     *  Display the field to select the Instruction Page
     */
    public function instruction_page_cb() {

        $instruction_page = get_option( $this->option_name . '_instruction_page' );

        echo "<select name='" . $this->option_name . "_instruction_page'>";
        echo "<option disabled>Choose an Instruction Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $instruction_page ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>This sets the page used to display the Instruction on how to get earn more points.';

    }

    /**
     *  Display the field to select the Referral Page
     */
    public function referral_page_cb() {

        $referral_page = get_option( $this->option_name . '_referral_page' );

        echo "<select name='" . $this->option_name . "_referral_page'>";
        echo "<option disabled>Choose an Instruction Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $referral_page ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>The page that is displayed by the referral link.';

    }

    /**
     *  Display the field to select the Voucher Tracking Page
     */
    public function voucher_tracking_page_cb() {

        $voucher_tracking_page = get_option( $this->option_name . '_voucher_tracking_page' );

        echo "<select name='" . $this->option_name . "_voucher_tracking_page'>";
        echo "<option disabled>Choose an Voucher Tracking Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $voucher_tracking_page ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>This sets the page used to display the front end voucher tracking page. This page should contain the following shortcode. <strong>[bws_sweepstakes_voucher_tracking_page]</strong>.';

    }

    /**
     *  Display the field to select the Sales Page
     */
    public function sales_page_cb() {

        $sales_page = get_option( $this->option_name . '_sales_page' );

        echo "<select name='" . $this->option_name . "_sales_page'>";
        echo "<option disabled>Choose an Voucher Tracking Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $sales_page ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>This sets the page used to display the front end sales page. This page should contain the following shortcode. <strong>[bws_lra_vendor_get_started]</strong>.';

    }

    /**
     *  Display the field to select the Sales Page
     */
    public function vendor_application_page_cb() {

        $vendor_application_page = get_option( $this->option_name . '_vendor_application_page' );

        echo "<select name='" . $this->option_name . "_vendor_application_page'>";
        echo "<option disabled>Choose an Vendor Application Page</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $vendor_application_page ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>This sets the page used to display the front end Vendor Application Page. This page should contain the following shortcode. <strong>[bws_lra_vendor_application_form]</strong>.';

    }

    /**
     *  Display the field to enter the Facebook Live URL
     */
    public function drawing_facebook_live_cb() {
        $drawing_facebook_live = get_option( $this->option_name . '_drawing_facebook_live' );
        echo '<input type="text" name="' . $this->option_name . '_drawing_facebook_live' . '" id="' . $this->option_name . '_drawing_facebook_live' . '" value="' . sanitize_text_field( $drawing_facebook_live ) . '" placeholder="https://facebook.com" size=35">';
    }

    /**
     *  Register settings section & settings fields for Zip-Codes
     */
    public function register_zip_codes_settings() {

        add_settings_section(
            $this->option_name . '_zip_codes',
            'Zip-Codes',
            array( $this, 'zip_codes_cb' ),
            $this->plugin_name . '-zip-codes-settings'
        );

        add_settings_field(
            $this->option_name . '_zip_codes_file',
            'Import from CSV',
            array( $this, 'zip_codes_file_cb' ),
            $this->plugin_name . '-zip-codes-settings',
            $this->option_name . '_zip_codes',
            array( 'label_for' => $this->option_name . '_zip_codes_file' )
        );

    }

    /**
     * Shows a list of known cities and their zip-codes
     */
    public function zip_codes_cb() {

        $zip_codes = get_option( $this->option_name . '_zip_codes_values' );

        $cities_2d = array();

        foreach ( $zip_codes as $state => $cities ) {

            foreach ( $cities as $city => $zip_codes ) {

                foreach ( $zip_codes as $zip_code ) {

                    $cities_2d[$city][] = $zip_code;

                }

            }

        }

        $zip_codes = $cities_2d;

        echo '<script>var cities = ' . json_encode( $zip_codes ) . '</script>';
        ?>
        <table class="form-table">
            <tbody>

            <?php
            if ( $zip_codes != false ) {
                ?>

                <tr>
                    <th scope="row">
                        <label for="zip-city">City</label>
                    </th>
                    <td>
                        <input type="text" id="zip-city" name="zip-city" value="<?php echo key( $zip_codes ); ?>">
                        <a href="#check" class="button button-small" id="check-city">Check the city</a>
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label>Used Zip-Codes</label>
                    </th>
                    <td>
                        <p id="zip_codes_values">
                            <?php
                            $first_city_zip_codes = array_shift( $zip_codes );
                            echo implode( ', ', $first_city_zip_codes );
                            ?>
                        </p>
                    </td>
                </tr>

                <?php
            } else {
                ?>

                <tr>
                    <p class="info">Please, upload Zip-Codes</p>
                </tr>

                <?php
            }
            ?>

            </tbody>
        </table>
        <?php

    }

    /**
     * Render the threshold zip-code csv file input
     */
    public function zip_codes_file_cb() {

        echo '<input type="file" name="' . $this->option_name . '_zip_codes_file' . '" id="' . $this->option_name . '_zip_codes_file' . '">';

    }

    /**
     * Handles requests from the settings page from the zip-codes section
     */
    public function handle_zip_codes_settings() {
        if ( !wp_verify_nonce( $_POST['_wpnonce'] ) || strpos( $_POST['_wp_http_referer'], '/wp-admin/admin.php?page=bws-sweepstakes-settings' ) === false ) {

            $msg = 'verify-error';

        } else {

            // If need to upload zip-codes
            if (isset($_POST['upload']) && strlen($_FILES[$this->option_name . '_zip_codes_file']["tmp_name"]) > 0) {

                $csv = file_get_contents($_FILES[$this->option_name . '_zip_codes_file']["tmp_name"]);

                $rows = explode("\n", $csv);

                if (count($rows) == 0) {
                    return;
                }

                $csv_rows = array();

                foreach ($rows as $row) {
                    $csv_rows[] = str_getcsv($row, ',');
                }

                $zip_codes_values = get_option($this->option_name . '_zip_codes_values');
                if (!is_array($zip_codes_values)) {
                    $zip_codes_values = array();
                }

                foreach ($csv_rows as $row) {

                    if (!isset($zip_codes_values[$row[2]][$row[1]][0]) || !in_array($row[0], $zip_codes_values[$row[2]][$row[1]])) {

                        $zip_code = BWS_Sweepstakes_Share_Functions::zip_code_add_zeros(strval($row[0]));

                        $zip_codes_values[$row[2]][$row[1]][] = $zip_code;

                    }

                }

                update_option($this->option_name . '_zip_codes_values', $zip_codes_values, false);

                $msg = 'uploaded';

                $this->create_zip_codes_csv();

                // If need to delete all zip-codes
            } else if ($_POST['delete'] == 'Delete all') {

                delete_option($this->option_name . '_zip_codes_values');

                $msg = 'deleted';

            }

        }

        $url = urldecode( $_POST['_wp_http_referer'] );
        $url = add_query_arg( 'msg', $msg, $url );

        echo $url;

        wp_safe_redirect( $url );
        exit;
    }

    /**
     * Create CSV for export Zip-Codes
     */
    public function create_zip_codes_csv() {

        $file = fopen( wp_upload_dir()['basedir'] . '/zip-codes.csv', 'w' );

        $zip_codes_values = get_option( $this->option_name . '_zip_codes_values' );

        foreach( $zip_codes_values as $state => $cities ) {

            foreach ( $cities as $city => $zip_codes ) {

                foreach ( $zip_codes as $zip_code ) {

                    $fields = array($zip_code, $city, $state);

                    fputs( $file, implode( $fields, ',' ) . "\n" );

                }

            }

        }

        fclose( $file );

    }

    /**
     * Validate single text field
     *
     * @param $input
     * @return mixed|void
     */
    public function validate_single_text_field( $input ) {

        $output = strip_tags( stripslashes( $input ) );

        return apply_filters( $this->option_name . '_validate_single_text_field', $output, $input );

    }

    /**
     * Display the cities list for the Manual Drawings section
     */
    public function manual_drawings_list_cb() {

        echo '<select id="' . $this->option_name . '_manual_drawings_city" name="'. $this->option_name .'_manual_drawings_list">';

        $zip_codes = BWS_Sweepstakes_Share_Functions::get_zip_codes_by_contestants();

        foreach ( $zip_codes as $state => $cities ) {
            foreach ( $cities as $city => $zip_codes ) {

                echo '<option value="' . $city . ';' . $state .'">'. $city . ', ' . $state . '</option>';

            }
        }

        echo '</select>';

    }

    /**
     * Display the manual drawing checkbox for the Manual Drawings section
     */
    public function manual_drawings_checkbox_cb() {

        echo '<input type="checkbox" id="' . $this->option_name . '_manual_drawings_checkbox">';

    }

    /**
     * Render the treshold manual drawing date input for this plugin
     */
    public function manual_drawings_start_day_cb() {

        $drawing_day = get_option( $this->option_name . '_start_drawing_day' );

        echo "<select id='" . $this->option_name . "_manual_drawings_start_day' name='" . $this->option_name . "_manual_drawings_start_day'>";
        echo "<option disabled>Choose a start drawing day</option>";
        echo "<option value='Monday' " . ($drawing_day == 'Monday' ? selected : '') . ">Monday</option>";
        echo "<option value='Tuesday' " . ($drawing_day == 'Tuesday' ? selected : '') . ">Tuesday</option>";
        echo "<option value='Wednesday' " . ($drawing_day == 'Wednesday' ? selected : '') . ">Wednesday</option>";
        echo "<option value='Thursday' " . ($drawing_day == 'Thursday' ? selected : '') . ">Thursday</option>";
        echo "<option value='Friday' " . ($drawing_day == 'Friday' ? selected : '') . ">Friday</option>";
        echo "<option value='Saturday' " . ($drawing_day == 'Saturday' ? selected : '') . ">Saturday</option>";
        echo "<option value='Sunday' " . ($drawing_day == 'Sunday' ? selected : '') . ">Sunday</option>";
        echo "</select>";

    }

    /**
     * Render the treshold manual drawing time input for this plugin
     */
    public function manual_drawings_start_time_cb() {

        $start_drawing_time = get_option( $this->option_name . '_start_drawing_time' );
        echo '<input type="time" name="' . $this->option_name . '_manual_drawings_start_time' . '" id="' . $this->option_name . '_manual_drawings_start_time' . '" value="' . $start_drawing_time . '">';

    }

    /*public function redirect_after_registration_page_cb() {

        $after_registration_page_id = get_option( $this->option_name . '_redirect_after_registration_page' );

        echo "<select name='" . $this->option_name . "_redirect_after_registration_page'>";
        echo "<option disabled>Choose a Page for redirect users</option>";
        if( $pages = get_pages() ){
            foreach( $pages as $page ){
                echo '<option value="' . $page->ID . '" ' . selected( $page->ID, $after_registration_page_id ) . '>' . $page->post_title . '</option>';
            }
        }
        echo "</select><br>";
        echo '<p>The page for redirect users after registration.';

    }*/

    /**
     * Register section for users points
     */
    public function register_users_points_settings() {

        add_settings_section(
            $this->option_name . '_users_points',
            'Users Awards',
            array( $this, 'users_points_cb' ),
            $this->plugin_name . '-users-points-settings'
        );

        // Points for Referring someone
        add_settings_field(
            $this->option_name . '_up_referring',
            'Referring someone',
            array( $this, 'up_referring_cb' ),
            $this->plugin_name . '-users-points-settings',
            $this->option_name . '_users_points',
            array( 'label_for' => $this->option_name . '_up_referring' )
        );

        // Points for Viewing product pages
        add_settings_field(
            $this->option_name . '_up_viewing',
            'Viewing product pages',
            array( $this, 'up_viewing_cb' ),
            $this->plugin_name . '-users-points-settings',
            $this->option_name . '_users_points',
            array( 'label_for' => $this->option_name . '_up_viewing' )
        );

        // Points for Purchasing products
        add_settings_field(
            $this->option_name . '_up_purchasing',
            'Purchasing products',
            array( $this, 'up_purchasing_cb' ),
            $this->plugin_name . '-users-points-settings',
            $this->option_name . '_users_points',
            array( 'label_for' => $this->option_name . '_up_purchasing' )
        );

    }

    /**
     * Display the description for the Users Awards section
     */
    public function users_points_cb() {
        echo '<p>Points that members can get for these actions.</p>';
    }

    /**
     * Display the field to set the award for referring users
     */
    public function up_referring_cb() {

        $mycred_pref_hooks = get_option( 'mycred_pref_hooks' );
        echo '<input type="text" name="' . $this->option_name . '_ref_creds" id="' . $this->option_name . '_ref_creds" value="' . $mycred_pref_hooks['hook_prefs']['affiliate']['signup']['creds'] . '">';

    }

    /**
     * Display the field to set the award for viewing products
     */
    public function up_viewing_cb() {

        $mycred_pref_hooks = get_option( 'mycred_pref_hooks' );
        echo '<input type="text" name="' . $this->option_name . '_view_creds" id="' . $this->option_name . '_view_creds" value="' . $mycred_pref_hooks['hook_prefs']['view_contents']['product']['creds'] . '">';

    }

    /**
     * Display the field to set the award for purchasing products
     */
    public function up_purchasing_cb() {

        $mycred_pref_hooks = get_option( 'mycred_pref_hooks' );
        echo '<input type="text" name="' . $this->option_name . '_purchase_creds" id="' . $this->option_name . '_purchase_creds" value="' . $mycred_pref_hooks['hook_prefs']['purchase_product']['creds'] . '">';

    }

    /**
     * Save the Users Awards settings
     */
    public function handle_awards_points_settings() {

        if ( !wp_verify_nonce( $_POST['_wpnonce'] ) || strpos( $_POST['_wp_http_referer'], '/wp-admin/admin.php?page=bws-sweepstakes-settings' ) === false ) {

            $msg='verify-error';

        } else if ( !isset( $_POST[$this->option_name . '_ref_creds'] ) || !is_numeric( $_POST[$this->option_name . '_ref_creds'] ) ||
            !isset( $_POST[$this->option_name . '_view_creds'] ) || !is_numeric( $_POST[$this->option_name . '_view_creds'] ) ||
            !isset( $_POST[$this->option_name . '_purchase_creds'] ) || !is_numeric( $_POST[$this->option_name . '_purchase_creds'] ) ) {

            $msg='value-error';

        } else {

            $mycred_pref_hooks = get_option( 'mycred_pref_hooks', true );
            $mycred_pref_hooks['hook_prefs']['affiliate']['signup']['creds'] = $_POST[$this->option_name . '_ref_creds'];
            $mycred_pref_hooks['hook_prefs']['view_contents']['product']['creds'] = $_POST[$this->option_name . '_view_creds'];
            $mycred_pref_hooks['hook_prefs']['purchase_product']['creds'] = $_POST[$this->option_name . '_purchase_creds'];
            update_option( 'mycred_pref_hooks', $mycred_pref_hooks );

            $msg = 'success';

        }

        $url = urldecode( $_POST['_wp_http_referer'] );
        $url = add_query_arg( 'msg', $msg, $url );
        wp_safe_redirect( $url );

    }

    /*public function up_referring_db_cb( $option ) {

        $mycred_pref_hooks = get_option( 'mycred_pref_hooks' );
        $mycred_pref_hooks['hook_prefs']['affiliate']['signup']['creds'] = $option;
        return $mycred_pref_hooks;

    }*/

    /**
     * Register section for users registration
     */
    /*public function register_users_registration_settings() {

        add_settings_section(
            $this->option_name . '_users_registration',
            'Users Registration',
            array( $this, 'users_registration_cb' ),
            $this->plugin_name . '-users-registration-settings'
        );

        // The page for redirect users after registration
        add_settings_field(
            $this->option_name . '_redirect_after_registration_page',
            'Redirect Page',
            array( $this, 'redirect_after_registration_page_cb' ),
            $this->plugin_name . '-users-registration-settings',
            $this->option_name . '_users_registration',
            array( 'label_for' => $this->option_name . '_redirect_after_registration_page' )
        );

    }*/

    /*public function handle_users_registration_settings() {

        if ( !wp_verify_nonce( $_POST['_wpnonce'] ) || strpos( $_POST['_wp_http_referer'], '/wp-admin/admin.php?page=bws-sweepstakes-settings' ) === false ) {

            $msg='verify-error';

        } else if ( !isset( $_POST[$this->option_name . '_redirect_after_registration_page'] ) || !is_numeric( $_POST[$this->option_name . '_redirect_after_registration_page']  ) ) {

            $msg='value-error';

        } else {

            $dar_settings = get_option( 'dar_settings', true );
            $dar_settings['redirection'] = get_permalink( $_POST[$this->option_name . '_redirect_after_registration_page'] );
            update_option( 'dar_settings', $dar_settings );

            $msg = 'success';

        }

        $url = urldecode( $_POST['_wp_http_referer'] );
        $url = add_query_arg( 'msg', $msg, $url );
        wp_safe_redirect( $url );

    }*/

    /**
     * Register weekly CRON interval
     *
     * @param $schedules
     * @return mixed
     */
    public function register_weekly_cron($schedules ) {

        $schedules['weekly'] = array(
            'interval' => 60 * 60 * 24 * 7,
            'display' => 'Weekly'
        );

        return $schedules;

    }

    /**
     * Add a section to display the settings of the simple prizes
     */
    public function add_simple_prize_custom_fields() {

        global $post;

        $post_id = $post->ID;

        $drawing_last_run_date = get_post_meta( $post_id, $this->option_name . '_drawing_last_run_date', true );
        $drawing_end_date = get_post_meta( $post_id, $this->option_name . '_drawing_end_date', true );
        $endless_drawing = get_post_meta( $post_id, $this->option_name . '_endless_drawing', true );

        if ( empty( $drawing_last_run_date ) ) {
            $last_run_end_date_delta = $drawing_end_date - time();
        } else {
            $last_run_end_date_delta = $drawing_end_date - $drawing_last_run_date;
        }

        $drawing_end_date_str = date( 'Y-m-d', intval( $drawing_end_date ) );

        if ( $endless_drawing == 'yes' ) {
            $drawing_info = "This is endless drawing.";
        } else if ( $last_run_end_date_delta > 0 ) {
            $days = floor( $last_run_end_date_delta / (60 * 60 * 24) );
            $drawing_info = "$days day(s) left till the end of the drawing.";
        } else {
            $drawing_info = "The time for the drawing runs out. The drawing is stopped.";
        }

        //echo "<div id='prize_panel' class='panel woocommerce_options_panel'>";
        echo '<div class="options_group show_if_simple_prize">';
        echo "<span class='drawings_count_label'>$drawing_info</span>";

        woocommerce_wp_select( array(
            'id'            => $this->option_name . '_drawing_frequency',
            'label'         => 'Drawing frequency',
            'options'       => array(
                '1'             => __( 'Every week', 'bws-sweepstakes' ),
                '2'             => __( 'Every 2 weeks', 'bws-sweepstakes' ),
                '4'             => __( 'Every 4 weeks', 'bws-sweepstakes' ),
                'stop'          => __( 'Stop drawing', 'bws-sweepstakes' ),
            )
        ) );

        echo "<div class='bws_sweepstakes_drawing_end_date_fieldset'>";
        woocommerce_wp_checkbox( array(
                'id'    =>  $this->option_name . '_endless_drawing',
                'label' =>  'Endless drawing',
            )
        );

        echo "<p class='form-field bws_sweepstakes_drawing_end_date_field'>";
        echo "<label for='". $this->option_name ."_drawing_end_date'>Drawing end date</label>";
        echo '<input type="text" name="' . $this->option_name . '_drawing_end_date" id="' . $this->option_name . '_drawing_end_date" pattern="' . esc_attr( apply_filters( 'woocommerce_date_input_html_pattern', '[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])' ) ) . '" placeholder="YYYY-MM-DD" maxlength="10" class="short" value="' . $drawing_end_date_str . '">';
        echo "</p>";
        echo '</div>';
        echo '</div>';
        //echo '</div>';

    }

    /**
     * Add a scetion to display the settings of the external prizes
     */
    public function add_external_prize_custom_fields() {
        global $post;

        $settings = array(
            //'quicktags'     =>  array( 'buttons' => 'strong,em,del,ul,ol,li,close' ), // note that spaces in this list seem to cause an issue
            'media_buttons'  =>  false,
        );

        echo '<div class="options_group show_if_external">';
        echo '<p class="external-terms-label">Terms & Conditions</p>';
        /*woocommerce_wp_textarea_input( array(
            'id'    =>  $this->option_name . '_external_terms',
            'label' =>  'Terms & Conditions',
        ) );*/

        wp_editor( get_post_meta( $post->ID, $this->option_name . '_external_terms', true ), $this->option_name . '_external_terms', $settings );
        echo '</div>';

    }

    /**
     * Add product type "prize"
     *
     * @param $product_types
     * @return mixed
     */
    public function add_prize_product_type( $product_types ) {

        $product_types['simple_prize'] = array(
            'id'            =>  '_simple_prize',
            'wrapper_class' =>  'show_if_simple',
            'label'         =>  __( 'Prize', 'bws-sweepstakes' ),
            'description'   =>  __( 'Products are marked as prizes.', 'bws-sweepstakes' ),
            'default'       =>  'no'
        );

        $product_types['external_prize'] = array(
            'id'            =>  '_external_prize',
            'wrapper_class' =>  'show_if_external',
            'label'         =>  __( 'Prize', 'bws-sweepstakes' ),
            'description'   =>  __( 'Products are marked as prizes.', 'bws-sweepstakes' ),
            'default'       =>  'no'
        );

        return $product_types;

    }

    /**
     * Save meta fields for products
     *
     * @param $post_id
     */
    public function save_products_meta_fields( $post_id ) {
        BWS_Sweepstakes_Share_Functions::save_products_meta_fields( $post_id );
    }

    /**
     * Saving fields for simple prizes
     *
     * @param $post_id
     *
     * TODO: Create verification for fields (SQL Injection etc)
     *
    public function save_simple_prize_fields( $post_id ) {

        if ( isset( $_POST['_simple_prize'] ) ) {

            $drawing_frequency = $_POST[$this->option_name . '_drawing_frequency'];
            $drawing_end_date_str = $_POST[$this->option_name . '_drawing_end_date'];
            $endless_drawing = $_POST[$this->option_name . '_endless_drawing'];
            $drawing_end_date = strtotime( $drawing_end_date_str );

            $drawing_frequency_old = get_post_meta( $post_id, $this->option_name . '_drawing_frequency', true );
            $drawing_end_date_old = get_post_meta( $post_id, $this->option_name . '_drawing_end_date', true );
            $endless_drawing_old = get_post_meta( $post_id, $this->option_name . '_endless_drawing', true );

            if ( !empty( $drawing_end_date_old ) && !empty( $drawing_frequency_old ) && !empty( $endless_drawing_old ) && $drawing_frequency == $drawing_frequency_old && $drawing_end_date == $drawing_end_date_old && $endless_drawing == $endless_drawing_old ) {
                return;
            }

            update_post_meta( $post_id, '_simple_prize', 'yes' );
            update_post_meta( $post_id, $this->option_name . '_drawing_last_run_date', 0 );
            update_post_meta( $post_id, $this->option_name . '_drawing_frequency', $drawing_frequency );

            if ( isset(  $_POST[$this->option_name . '_endless_drawing'] ) ) {
                update_post_meta( $post_id, $this->option_name . '_endless_drawing', 'yes' );
            } else {
                update_post_meta( $post_id, $this->option_name . '_endless_drawing', 'no' );
            }

            update_post_meta( $post_id, $this->option_name . '_drawing_end_date', $drawing_end_date );

        } else {

            update_post_meta( $post_id, '_simple_prize', 'no' );

        }

    }

    public function save_external_prize_fields( $post_id ) {

        if ( isset( $_POST['_external_prize'] ) ) {

            update_post_meta( $post_id, '_external_prize', 'yes' );

        } else {

            update_post_meta( $post_id, '_external_prize', 'no' );

        }

    }

    /**
     * Save discount percentage value
     *
     * @param $post_id
     *
    public function save_discount_percentage( $post_id ) {

        if ( isset( $_POST['_regular_price'] ) && isset( $_POST['_sale_price'] ) && $_POST['_regular_price'] != "" && $_POST['_sale_price'] != "" ) {

            $discount_percentage = 100 - round( ( $_POST['_sale_price'] * 100 ) / $_POST['_regular_price'] );
            update_post_meta( $post_id, '_discount_percentage', $discount_percentage );

        } else {

            update_post_meta( $post_id, '_discount_percentage', 0 );

        }

    }*/

    /**
     * Save terms&conditions text to the database
     *
     * @param $post_id
     */
    public function save_terms_for_external_prize( $post_id ) {

        $product = wc_get_product( $post_id );
        $terms = (isset( $_POST[$this->option_name . '_external_terms'] ) ? $_POST[$this->option_name . '_external_terms'] : '');
        $product->update_meta_data( $this->option_name . '_external_terms', sanitize_post_field( $this->option_name . '_external_terms', $terms, $post_id, 'display' ) );
        $product->save();

    }

    /**
     * Register a new taxonomy for cities
     */
    public function custom_taxonomy_city()  {

        $labels = array(
            'name'                       => 'Cities',
            'singular_name'              => 'City',
            'menu_name'                  => 'Cities',
            'all_items'                  => 'All Cities',
            'parent_item'                => null,
            'parent_item_colon'          => null,
            'new_item_name'              => 'New City Name',
            'add_new_item'               => 'Add New City',
            'edit_item'                  => 'Edit City',
            'update_item'                => 'Update City',
            'separate_items_with_commas' => 'Separate City with commas',
            'search_items'               => 'Search Cities',
            'add_or_remove_items'        => 'Add or remove Cities',
            'choose_from_most_used'      => 'Choose from the most used Cities',
        );

        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => false,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        );

        register_taxonomy( 'city', 'product', $args );
        //register_taxonomy_for_object_type( 'city', 'product' );

    }

    /**
     * Register a notification template for winners
     */
    public function register_winner_notification_template() {

        $post_exists = post_exists( '[{{{site.name}}}] You won in the drawing' );

        if ( $post_exists != 0 && get_post_status( $post_exists ) == 'publish' )
            return;

        $notification_post_template = array(
            'post_title'    =>  __( '[{{{site.name}}}] You won in the drawing', $this->plugin_name ),
            'post_content'  =>  __( "user name: {{user.name}}\nuser email: {{user.email}}\nuser zip: {{user.zip}}\nuser points: {{user.points}}\nuser reflink: {{{user.reflink}}}\nwinners names: {{winners.names}}\nwinners prizelist: {{{winners.prizelist}}}\ndrawing date: {{drawing.date}}", $this->plugin_name ),
            'post_excerpt'  =>  __( '{{recipient.email}}, good job!', $this->plugin_name ),
            'post_status'   =>  'publish',
            'post_type'     =>  bp_get_email_post_type(),
        );

        $post_id = wp_insert_post( $notification_post_template );

        if ( $post_id ) {

            $tt_ids = wp_set_object_terms( $post_id, 'winner_notification', bp_get_email_tax_type() );
            foreach ( $tt_ids as $tt_id ) {

                $term = get_term_by( 'term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type() );
                wp_update_term( (int) $term->term_id, bp_get_email_tax_type(), array(
                    'description'   =>  'The Winner receives notification after the draw.',
                ) );

            }

        }

    }

    /**
     * Register a notification template for vendors
     */
    public function register_vendor_notification_template() {

        $post_exists = post_exists( '[{{{site.name}}}] The Winner is found for one of your prizes' );

        if ( $post_exists != 0 && get_post_status( $post_exists ) == 'publish' )
            return;

        $notification_post_template = array(
            'post_title'    =>  __( '[{{{site.name}}}] The Winner is found for one of your prizes', $this->plugin_name ),
            'post_content'  =>  __( "seller zip: {{seller.zip}}\nseller prizelist: {{{seller.prizelist}}}\nuser name: {{user.name}}\nuser email: {{user.email}}\nuser zip: {{user.zip}}\nuser points: {{user.points}}\nuser reflink: {{{user.reflink}}}\nwinners names: {{winners.names}}\nwinners prizelist: {{{winners.prizelist}}}\ndrawing date: {{drawing.date}}", $this->plugin_name ),
            'post_excerpt'  =>  __( '{{winners.names}}: {{{seller.prizelist}}}, good job!', $this->plugin_name ),
            'post_status'   =>  'publish',
            'post_type'     =>  bp_get_email_post_type(),
        );

        $post_id = wp_insert_post( $notification_post_template );

        if ( $post_id ) {

            $tt_ids = wp_set_object_terms( $post_id, 'vendor_notification', bp_get_email_tax_type() );
            foreach ( $tt_ids as $tt_id ) {

                $term = get_term_by( 'term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type() );
                wp_update_term( (int) $term->term_id, bp_get_email_tax_type(), array(
                    'description'   =>  'The Seller receives notification after the draw.',
                ) );

            }

        }

    }

    /**
     * Register a notification template for contestants
     */
    public function register_contestant_notification_template() {

        $post_exists = post_exists( '[{{{site.name}}}] The Winner on your location is found' );

        if ( $post_exists != 0 && get_post_status( $post_exists ) == 'publish' )
            return;

        $notification_post_template = array(
            'post_title'    =>  __( '[{{{site.name}}}] The Winner on your location is found', $this->plugin_name ),
            'post_content'  =>  __( "user name: {{user.name}}\nuser email: {{user.email}}\nuser zip: {{user.zip}}\nuser points: {{user.points}}\nuser reflink: {{{user.reflink}}}\nwinners names: {{winners.names}}\nwinners prizelist: {{{winners.prizelist}}}\ndrawing date: {{drawing.date}}", $this->plugin_name ),
            'post_excerpt'  =>  __( '{{recipient.email}}, we found the Winner on your location! {{winners.names}} {{{winners.prizelist}}}', $this->plugin_name ),
            'post_status'   =>  'publish',
            'post_type'     =>  bp_get_email_post_type(),
        );

        $post_id = wp_insert_post( $notification_post_template );

        if ( $post_id ) {

            $tt_ids = wp_set_object_terms( $post_id, 'contestant_notification', bp_get_email_tax_type() );
            foreach ( $tt_ids as $tt_id ) {

                $term = get_term_by( 'term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type() );
                wp_update_term( (int) $term->term_id, bp_get_email_tax_type(), array(
                    'description'   =>  'The Member receives notification after the draw.',
                ) );

            }

        }

    }

    /**
     * Register a message template for vendors broadcast messages
     */
    public function register_broadcast_message_to_vendor_template() {

        $post_exists = post_exists( '[{{{site.name}}}] {{message.subject}}' );

        if ( $post_exists != 0 && get_post_status( $post_exists ) == 'publish' )
            return;

        $notification_post_template = array(
            'post_title'    =>  __( '[{{{site.name}}}] {{message.subject}}', $this->plugin_name ),
            'post_content'  =>  __( "seller zip: {{seller.zip}}\nseller prizelist: {{{seller.prizelist}}}\nuser name: {{user.name}}\nuser email: {{user.email}}\nuser zip: {{user.zip}}\nuser points: {{user.points}}\nuser reflink: {{{user.reflink}}}\nmessage subject: {{{message.subject}}}\nmessage content: {{{message.content}}}", $this->plugin_name ),
            'post_excerpt'  =>  __( '{{message.content}}', $this->plugin_name ),
            'post_status'   =>  'publish',
            'post_type'     =>  bp_get_email_post_type(),
        );

        $post_id = wp_insert_post( $notification_post_template );

        if ( $post_id ) {

            $tt_ids = wp_set_object_terms( $post_id, 'vendor_broadcast_message', bp_get_email_tax_type() );
            foreach ( $tt_ids as $tt_id ) {

                $term = get_term_by( 'term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type() );
                wp_update_term( (int) $term->term_id, bp_get_email_tax_type(), array(
                    'description'   =>  'A Seller receives the broadcast message.',
                ) );

            }

        }

    }

    /**
     * Register a message template for contestants broadcast messages
     */
    public function register_broadcast_message_to_contestant_template() {

        $post_exists = post_exists( '[{{{site.name}}}] {{{message.subject}}}' );

        if ( $post_exists != 0 && get_post_status( $post_exists ) == 'publish' )
            return;

        $notification_post_template = array(
            'post_title'    =>  __( '[{{{site.name}}}] {{message.subject}}', $this->plugin_name ),
            'post_content'  =>  __( "user name: {{user.name}}\nuser email: {{user.email}}\nuser zip: {{user.zip}}\nuser points: {{user.points}}\nuser reflink: {{{user.reflink}}}\nmessage subject: {{{message.subject}}}\nmessage content: {{{message.content}}}", $this->plugin_name ),
            'post_excerpt'  =>  __( '{{message.content}}', $this->plugin_name ),
            'post_status'   =>  'publish',
            'post_type'     =>  bp_get_email_post_type(),
        );

        $post_id = wp_insert_post( $notification_post_template );

        if ( $post_id ) {

            $tt_ids = wp_set_object_terms( $post_id, 'contestant_broadcast_message', bp_get_email_tax_type() );
            foreach ( $tt_ids as $tt_id ) {

                $term = get_term_by( 'term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type() );
                wp_update_term( (int) $term->term_id, bp_get_email_tax_type(), array(
                    'description'   =>  'A Member receives the broadcast message.',
                ) );

            }

        }

    }

    /**
     * Register a notification template about Points Receiving
     */
    public function register_notification_about_receive_points_template() {

        $post_exists = post_exists( '[{{{site.name}}}] {{user.name}}, you received {{user.newpoints}} points!' );

        if ( $post_exists != 0 && get_post_status( $post_exists ) == 'publish' )
            return;

        $notification_post_template = array(
            'post_title'    =>  __( '[{{{site.name}}}] {{user.name}}, you received {{user.newpoints}} points!', $this->plugin_name ),
            'post_content'  =>  __( "user name: {{user.name}}\nuser email: {{user.email}}\nuser zip-code: {{user.zip}}\nuser points: {{user.points}}\nuser points received: {{user.newpoints}}\nuser reflink: {{user.reflink}}", $this->plugin_name ),
            'post_excerpt'  =>  __( '{{user.name}}, you received {{user.newpoints}} points!', $this->plugin_name ),
            'post_status'   =>  'publish',
            'post_type'     =>  bp_get_email_post_type(),
        );

        $post_id = wp_insert_post( $notification_post_template );

        if ( $post_id ) {

            $tt_ids = wp_set_object_terms( $post_id, 'receive_points_notification', bp_get_email_tax_type() );
            foreach ( $tt_ids as $tt_id ) {

                $term = get_term_by( 'term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type() );
                wp_update_term( (int) $term->term_id, bp_get_email_tax_type(), array(
                    'description'   =>  'A Member receives points.',
                ) );

            }

        }

    }

    /**
     * Send broadcast messages
     */
    public function handle_broadcast_message() {

        if ( !empty($_POST) && check_admin_referer( 'send_broadcast_message', 'safesaver' ) ) {

            if ( !empty( $_POST['bws_sweepstakes_mc_subject'] ) && !empty( $_POST['bws_sweepstakes_mc_message'] ) && !empty( $_POST['bws_sweepstakes_mc_recipients_type'] ) ) {

                // Sanitize input
                $message_subject = sanitize_text_field( $_POST['bws_sweepstakes_mc_subject'] );
                $message_content = sanitize_textarea_field( $_POST['bws_sweepstakes_mc_message'] );
                $recipients_type = $_POST['bws_sweepstakes_mc_recipients_type'];
                $recipients_zips = sanitize_text_field( $_POST['bws_sweepstakes_mc_recipients_zips'] );

                if( strlen( $recipients_zips ) > 0 ) {
                    $recipients_zips = explode( ' ', $recipients_zips );
                } else {
                    $recipients_zips = null;
                }

                /*** BROADCAST MESSAGE FOR VENDORS ***/
                if ( $recipients_type == 'vendors' ) {

                    $sending_result = $this->send_vendor_broadcast_message( $message_subject, $message_content, $recipients_zips );

                    if ( $sending_result == 0 ) {
                        $msg = 'sent';
                    } else {
                        $msg = 'error';
                        $ec = $sending_result;
                    }

                /*** BROADCAST MESSAGE FOR CONTESTANTS ***/
                } else if ( $recipients_type == 'contestants' ) {

                    $sending_result = $this->send_contestant_broadcast_message( $message_subject, $message_content, $recipients_zips );

                    if ( $sending_result == 0 ) {
                        $msg = 'sent';
                    } else {
                        $msg = 'error';
                        $ec = $sending_result;
                    }

                }

                $url = urldecode( $_POST['_wp_http_referer'] );
                $url = add_query_arg( 'msg', $msg, $url );
                $url = add_query_arg( 'ec', $ec, $url );

                wp_safe_redirect($url);
                exit;

            } else {

                $msg = 'ii';

                $url = urldecode( $_POST['_wp_http_referer'] );
                $url = add_query_arg( 'msg', $msg, $url );

                wp_safe_redirect( $url );
                exit;

            }

        }

    }

    /**
     * Send the notification to a winner
     *
     * @param $drawing
     * @param bool $certain_winner_id
     */
    public function send_winner_notification( $drawing, $certain_winner_id = false ) {

        $winners_ids = array();
        $winnings = $drawing->get_winnings();
        $winners_names = array();

        foreach ( $winnings as $winner_id => $prizes ) {
            $winners_ids[] = $winner_id;
            $winners_names[] = xprofile_get_field_data(1, $winner_id ) . " " . xprofile_get_field_data(6, $winner_id );
        }

        $winners_names = implode( ', ', $winners_names );

        if ( $certain_winner_id !== false ) {

            $prize_list = "<ul>";

            foreach ( $winnings as $winner_id => $prizes ) {

                $prize_list .= "<li>" . xprofile_get_field_data(1, $winner_id ) . " " . xprofile_get_field_data(6, $winner_id ) . " won ";
                if ( $certain_winner_id == $winner_id ) {

                    for ( $i = 0; $i < count( $prizes ); $i++ ) {
                        $prize_list .= "<a href='" . get_permalink($prizes[$i]) . "'>" . get_the_title($prizes[$i]) . "</a> (<a href='" . home_url() . "/checkout/?add-to-cart=" . $prizes[$i] . "&drawing=" . $drawing->get_id() . "'>Get this Prize</a>)";

                        if ( $i < (count( $prizes ) - 1) ) {
                            $prize_list .= ", ";
                        }

                    }

                } else {

                    for ( $i = 0; $i < count( $prizes ); $i++ ) {
                        $prize_list .= "<a href='" . get_permalink($prizes[$i]) . "'>" . get_the_title($prizes[$i]) . "</a>";

                        if ( $i < (count( $prizes ) - 1) ) {
                            $prize_list .= ", ";
                        }

                    }

                }

                $prize_list .= "</li>";

            }

            $prize_list .= "</ul>";

            $referral_page_id = get_option($this->option_name . '_referral_page');

            $args = array(
                'tokens' => array(
                    'user.name' => xprofile_get_field_data(1, $certain_winner_id) . ' ' . xprofile_get_field_data(6, $certain_winner_id),
                    'user.email' => get_user_by('id', $certain_winner_id)->user_email,
                    'user.zip' => (string)BWS_Sweepstakes_Share_Functions::zip_code_add_zeros(xprofile_get_field_data(5, $certain_winner_id)),
                    'user.points' => mycred_get_users_balance($certain_winner_id),
                    'user.reflink' => do_shortcode('[mycred_affiliate_link url="' . get_permalink($referral_page_id) . '" user_id="' . $certain_winner_id . '"]'),
                    'winners.names' => $winners_names,
                    'winners.prizelist' => $prize_list,
                    'drawing.date' => date('Y-m-d', $drawing->get_time()),
                )
            );

            bp_send_email('winner_notification', (int)$certain_winner_id, $args);

        } else {

            foreach ($winners_ids as $winner_iter) {

                $prize_list = "<ul>";

                foreach ( $winnings as $winner_id => $prizes ) {

                    $prize_list .= "<li>" . xprofile_get_field_data(1, $winner_id ) . " " . xprofile_get_field_data(6, $winner_id ) . " won ";
                    if ( $winner_iter == $winner_id ) {

                        for ( $i = 0; $i < count( $prizes ); $i++ ) {
                            $prize_list .= "<a href='" . get_permalink($prizes[$i]) . "'>" . get_the_title($prizes[$i]) . "</a> (<a href='" . home_url() . "/checkout/?add-to-cart=" . $prizes[$i] . "&drawing=" . $drawing->get_id() . "'>Get this Prize</a>)";

                            if ( $i < (count( $prizes ) - 1) ) {
                                $prize_list .= ", ";
                            }

                        }

                    } else {

                        for ( $i = 0; $i < count( $prizes ); $i++ ) {
                            $prize_list .= "<a href='" . get_permalink($prizes[$i]) . "'>" . get_the_title($prizes[$i]) . "</a>";

                            if ( $i < (count( $prizes ) - 1) ) {
                                $prize_list .= ", ";
                            }

                        }

                    }

                    $prize_list .= "</li>";

                }

                $prize_list .= "</ul>";

                $referral_page_id = get_option($this->option_name . '_referral_page');

                $args = array(
                    'tokens' => array(
                        'user.name' => xprofile_get_field_data(1, $winner_iter) . ' ' . xprofile_get_field_data(6, $winner_iter),
                        'user.email' => get_user_by('id', $winner_iter)->user_email,
                        'user.zip' => (string)BWS_Sweepstakes_Share_Functions::zip_code_add_zeros(xprofile_get_field_data(5, $winner_iter)),
                        'user.points' => mycred_get_users_balance($winner_iter),
                        'user.reflink' => do_shortcode('[mycred_affiliate_link url="' . get_permalink($referral_page_id) . '" user_id="' . $winner_iter . '"]'),
                        'winners.names' => $winners_names,
                        'winners.prizelist' => $prize_list,
                        'drawing.date' => date('Y-m-d', $drawing->get_time()),
                    )
                );

                bp_send_email('winner_notification', (int)$winner_iter, $args);

            }

        }

    }

    /**
     * Send the notification to a vendor
     *
     * @param $drawing
     */
    public function send_vendors_notifications( $drawing ) {

        $winnings = $drawing->get_winnings();
        $winners_names = array();

        $prize_list = "<ul>";

        foreach ( $winnings as $winner_id => $prizes ) {

            $prize_list .= "<li>" . xprofile_get_field_data(1, $winner_id ) . " " . xprofile_get_field_data(6, $winner_id ) . " won ";
            $winners_names[] = xprofile_get_field_data(1, $winner_id ) . " " . xprofile_get_field_data(6, $winner_id );

            for ( $i = 0; $i < count( $prizes ); $i++ ) {
                $prize_list .= "<a href='" . get_permalink( $prizes[$i] ) . "'>" . get_the_title( $prizes[$i] ) . "</a>";

                if ( $i < (count( $prizes ) - 1) ) {
                    $prize_list .= ", ";
                }

            }

            $prize_list .= "</li>";

        }

        $prize_list .= "</ul>";

        $winners_names = implode( ', ', $winners_names );

        foreach ( $drawing->get_vendors() as $vendor ) {

            $current_vendor_prize_list = "<ul>";
            foreach ( $drawing->get_vendor_by_id( $vendor->id )->prizes as $prize ) {
                $current_vendor_prize_list .= "<li><a href='" . get_permalink( $prize ) . "'>" . get_the_title( $prize ) . "</a></li>";
            }
            $current_vendor_prize_list .= "</ul>";

            $referral_page_id = get_option( $this->option_name . '_referral_page' );

            $args = array(
                'tokens'    =>  array(
                    'seller.zip'            =>  (string) BWS_Sweepstakes_Share_Functions::zip_code_add_zeros( xprofile_get_field_data(5, $vendor->id ) ),
                    'seller.prizelist'      =>  $current_vendor_prize_list,
                    'user.name'             =>  xprofile_get_field_data(1, $vendor->id ) . ' ' . xprofile_get_field_data(6, $vendor->id ),
                    'user.email'            =>  get_user_by( 'id', $vendor->id )->user_email,
                    'user.zip'              =>  (string) BWS_Sweepstakes_Share_Functions::zip_code_add_zeros( xprofile_get_field_data(5, $vendor->id ) ),
                    'user.points'           =>  mycred_get_users_balance ( $vendor->id ),
                    'user.reflink'          =>  do_shortcode( '[mycred_affiliate_link url="' . get_permalink( $referral_page_id ) . '" user_id="' . $vendor->id . '"]' ),
                    'winners.names'         =>  $winners_names,
                    'winners.prizelist'     =>  $prize_list,
                    'drawing.date'          =>  date( 'Y-m-d', $drawing->get_time() ),
                )
            );

            bp_send_email( 'vendor_notification', (int) $vendor->id, $args );

        }

    }

    /**
     * Send the notification to a contestant
     *
     * @param $drawing
     */
    public function send_contestants_notifications( $drawing ) {

        $winnings = $drawing->get_winnings();
        $winners_names = array();

        $prize_list = "<ul>";

        foreach ( $winnings as $winner_id => $prizes ) {

            $prize_list .= "<li>" . xprofile_get_field_data(1, $winner_id ) . " " . xprofile_get_field_data(6, $winner_id ) . " won ";
            $winners_names[] = xprofile_get_field_data(1, $winner_id ) . " " . xprofile_get_field_data(6, $winner_id );

            for ( $i = 0; $i < count( $prizes ); $i++ ) {
                $prize_list .= "<a href='" . get_permalink( $prizes[$i] ) . "'>" . get_the_title( $prizes[$i] ) . "</a>";

                if ( $i < (count( $prizes ) - 1) ) {
                    $prize_list .= ", ";
                }

            }

            $prize_list .= "</li>";

        }

        $prize_list .= "</ul>";

        $winners_names = implode( ', ', $winners_names );

        foreach ( $drawing->get_contestants() as $contestant ) {

            $referral_page_id = get_option( $this->option_name . '_referral_page' );

            $args = array(
                'tokens'    =>  array('user.name'         =>  xprofile_get_field_data(1, $contestant->id ) . ' ' . xprofile_get_field_data(6, $contestant->id ),
                    'user.email'        =>  get_user_by( 'id', $contestant->id )->user_email,
                    'user.zip'          =>  (string) BWS_Sweepstakes_Share_Functions::zip_code_add_zeros( xprofile_get_field_data(5, $contestant->id ) ),
                    'user.points'       =>  mycred_get_users_balance ( $contestant->id ),
                    'user.reflink'      =>  do_shortcode( '[mycred_affiliate_link url="' . get_permalink( $referral_page_id ) . '" user_id="' . $contestant->id . '"]' ),
                    'winners.names'     =>  $winners_names,
                    'winners.prizelist' =>  $prize_list,
                    'drawing.date'      =>  date( 'Y-m-d', $drawing->get_time() ),
                )
            );

            bp_send_email( 'contestant_notification', (int) $contestant->id, $args );

        }

    }

    /**
     * Send a broadcast message to a vendor
     *
     * @param $message_subject
     * @param $message_content
     * @param $recipients_zips
     * @return int
     */
    private function send_vendor_broadcast_message( $message_subject, $message_content, $recipients_zips ) {

        $vendors = array();

        $errors_count = 0;

        if ( count( $recipients_zips ) > 0 ) {

            foreach ( $recipients_zips as $zip ) {
                $vendors = array_merge( $vendors, BWS_Sweepstakes_Share_Functions::get_vendors( $zip ) );
            }

        } else {

            $vendors = BWS_Sweepstakes_Share_Functions::get_vendors();

        }

        foreach ( $vendors as $vendor ) {

            $current_vendor_prize_list = "<ul>";
            foreach ( $vendor->prizes as $prize ) {
                $current_vendor_prize_list .= "<li><a href='" . get_permalink( $prize ) . "'>" . get_the_title( $prize ) . "</a></li>";
            }
            $current_vendor_prize_list .= "</ul>";

            $referral_page_id = get_option( $this->option_name . '_referral_page' );

            $args = array(
                'tokens' => array(
                    'seller.zip' => BWS_Sweepstakes_Share_Functions::zip_code_add_zeros( $vendor->zip_code ),
                    'seller.prizelist' => $current_vendor_prize_list,
                    'user.name' => xprofile_get_field_data( 1, $vendor->id ) . ' ' . xprofile_get_field_data( 6, $vendor->id ),
                    'user.email' => get_user_by('id', $vendor->id )->user_email,
                    'user.zip' => (string) BWS_Sweepstakes_Share_Functions::zip_code_add_zeros( $vendor->zip_code ),
                    'user.points' => mycred_get_users_balance( $vendor->id ),
                    'user.reflink' => do_shortcode( '[mycred_affiliate_link url="' . get_permalink( $referral_page_id ) . '" user_id="' . $vendor->id . '"]' ),
                )
            );

            $message_subject_tokenized = bp_core_replace_tokens_in_text( $message_subject, $args['tokens'] );
            $message_content_tokenized = bp_core_replace_tokens_in_text( $message_content, $args['tokens'] );

            $args['tokens']['message.subject'] = stripslashes( $message_subject_tokenized );
            $args['tokens']['message.content'] = stripslashes( $message_content_tokenized );

            $return = bp_send_email( 'vendor_broadcast_message', (int) $vendor->id, $args );

            if ( $return !== true ) {
                $errors_count++;
            }

        }

        return $errors_count;

    }

    /**
     * Send a broadcast message to a contestant
     *
     * @param $message_subject
     * @param $message_content
     * @param $recipients_zips
     * @return int
     */
    private function send_contestant_broadcast_message( $message_subject, $message_content, $recipients_zips ) {

        $contestants = array();

        $errors_count = 0;

        if ( count( $recipients_zips ) > 0 ) {

            foreach ( $recipients_zips as $zip ) {
                $contestants = array_merge( $contestants, BWS_Sweepstakes_Share_Functions::get_contestants( $zip ) );
            }

        } else {

            $contestants = BWS_Sweepstakes_Share_Functions::get_contestants();

        }

        $referral_page_id = get_option( $this->option_name . '_referral_page' );

        foreach ( $contestants as $contestant ) {

            $args = array(
                'tokens' => array(
                    'user.name' => xprofile_get_field_data( 1, $contestant->id ) . ' ' . xprofile_get_field_data( 6, $contestant->id ),
                    'user.email' => get_user_by('id', $contestant->id )->user_email,
                    'user.zip' => (string) BWS_Sweepstakes_Share_Functions::zip_code_add_zeros( $contestant->zip_code ),
                    'user.points' => mycred_get_users_balance( $contestant->id ),
                    'user.reflink' => do_shortcode( '[mycred_affiliate_link url="' . get_permalink( $referral_page_id ) . '" user_id="' . $contestant->id . '"]' ),
                )
            );

            $message_subject_tokenized = bp_core_replace_tokens_in_text( $message_subject, $args['tokens'] );
            $message_content_tokenized = bp_core_replace_tokens_in_text( $message_content, $args['tokens'] );

            $args['tokens']['message.subject'] = stripslashes( $message_subject_tokenized );
            $args['tokens']['message.content'] = stripslashes( $message_content_tokenized );

            $return = bp_send_email( 'contestant_broadcast_message', (int) $contestant->id, $args );

            if ( $return !== true ) {
                $errors_count++;
            }
        }

        return $errors_count;

    }

    /**
     * Send the notification about Points Receiving
     *
     * @param $user_id
     * @param $current_balance
     * @param $amount
     * @param $type
     */
    public function send_notification_about_receive_points( $user_id, $current_balance, $amount, $type ) {

        if ( $amount < 0 ) {
            return;
        }

        $referral_page_id = get_option( $this->option_name . '_referral_page' );

        $args = array(
            'tokens'    =>  array(
                'user.name'         =>  xprofile_get_field_data(1, $user_id ) . ' ' . xprofile_get_field_data(6, $user_id ),
                'user.email'        =>  get_user_by( 'id', $user_id )->user_email,
                'user.zip'          =>  (string) BWS_Sweepstakes_Share_Functions::zip_code_add_zeros( xprofile_get_field_data(5, $user_id ) ),
                'user.points'       =>  mycred_get_users_balance ( $user_id ),
                'user.newpoints'    =>  $amount,
                'user.reflink'      =>  do_shortcode( '[mycred_affiliate_link url="' . get_permalink( $referral_page_id ) . '" user_id="' . $user_id . '"]' ),
            )
        );

        bp_send_email( 'receive_points_notification', (int) $user_id, $args );

    }

    /**
     * Return FALSE
     *
     * @return bool
     */
    public function get_false() {
        return false;
    }

    /**
     * Save Manual Drawings by AJAX request
     */
    public function save_manual_drawings_cb() {

        $place = explode( ';', $_POST['cs'] );
        $place = array( 'city'  =>  $place[0], 'state'  =>  $place[1] );
        $ds = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( $place );

        if( $_POST['is_md'] == 'yes' && !is_null( $_POST['day'] ) && !is_null( $_POST['time'] ) ) {

            $next_date = array( 'day'   =>  $_POST['day'], 'time'   =>  $_POST['time'] );
            $ds->set_type( 'manual' );
            $ds->set_next_date( $next_date );

        } else {

            $ds->set_type( 'auto' );
            $ds->set_next_date( 'auto' );

        }

        $ds->save_drawing_settings();

        die;

    }

    /**
     * Get Manual Drawings by AJAX request
     */
    public function get_manual_drawings_cb() {

        if ( isset( $_POST['cs'] ) ) {

            $place = explode( ';', $_POST['cs'] );
            $place = array( 'city'  =>  $place[0], 'state'  =>  $place[1] );
            $ds = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( $place );

            $data = array(
                'next_date'         =>  $ds->get_next_date(),
                'type'              =>  $ds->get_type(),
            );

            echo json_encode( $data );

        }

        die;

    }

    /**
     * Resend the notification for certain winner invoked by AJAX request
     */
    public function resend_notifications_for_certain_winner_cb() {

        $winner_id = $_POST['winner'];
        $drawing_id = $_POST['drawing'];

        $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( $drawing_id );
        $this->send_winner_notification( $drawing, $winner_id );

        die;

    }

    /*
     *
     *
     * DEPRECATED
     *
     *
     */

    public function add_prize_tab_for_products( $tabs ) {

        $tabs['prize'] = array(
            'label'         => __( 'Prize', 'bws-sweepstakes' ), // The name of your panel
            'target'        => 'prize_panel', // Will be used to create an anchor link so needs to be unique
            'class'         => array( 'prize_tab', 'show_if_simple', 'show_if_variable', 'show_if_prize' ), // Class for your panel tab - helps hide/show depending on product type
            'priority'      => 80, // Where your panel will appear. By default, 70 is last item
        );
        return $tabs;

    }

    public function debug() {
        update_option( 'bws_plugin_error',  ob_get_contents() );
    }

    function admin_add_wysiwyg_terms_textarea()
    {
        ?>
        <script type="text/javascript">/* <![CDATA[ */
            jQuery(function($){
                var i=1;
                $('#bws_sweepstakes_external_terms').each(function(e)
                {
                    var id = $(this).attr('id');
                    if (!id)
                    {
                        id = 'customEditor-' + i++;
                        $(this).attr('id',id);
                    }
                    tinyMCE.execCommand('mceAddControl', false, id);
                });
            });
            /* ]]> */</script>
        <?php
    }

    /**
     * Register notification template for certain winner
     *
    public function register_certain_winner_notification_template() {

    $post_exists = post_exists( '[{{{site.name}}}] You won in the drawing' );

    if ( $post_exists != 0 && get_post_status( $post_exists ) == 'publish' )
    return;

    $notification_post_template = array(
    'post_title'    =>  __( '[{{{site.name}}}] You won in the drawing', $this->plugin_name ),
    'post_content'  =>  __( "user name: {{user.name}}\nuser email: {{user.email}}\nuser zip: {{user.zip}}\nuser points: {{user.points}}\nuser reflink: {{{user.reflink}}}\nwinners names: {{winners.names}}\nwinners prizelist: {{{winners.prizelist}}}\ndrawing date: {{drawing.date}}", $this->plugin_name ),
    'post_excerpt'  =>  __( '{{recipient.email}}, good job!', $this->plugin_name ),
    'post_status'   =>  'publish',
    'post_type'     =>  bp_get_email_post_type(),
    );

    $post_id = wp_insert_post( $notification_post_template );

    if ( $post_id ) {

    $tt_ids = wp_set_object_terms( $post_id, 'winner_notification', bp_get_email_tax_type() );
    foreach ( $tt_ids as $tt_id ) {

    $term = get_term_by( 'term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type() );
    wp_update_term( (int) $term->term_id, bp_get_email_tax_type(), array(
    'description'   =>  'The Winner receives notification after the draw.',
    ) );

    }

    }

    }*/

    /**
     * Save referrer id to user_meta
     *
     * @param $user_id
     * @param $IP
     * @param $new_user_id
     */
    public function save_ref_user_id( $user_id, $IP, $new_user_id ) {
        update_user_meta( $user_id, 'ref_user_id', $new_user_id );
    }

}