<?php
/**
 * Provide a admin area view for the plugin Mail Center
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/admin/partials
 */
?>

<div class="wrap">
    <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

    <?php
    if ( !empty( $_GET['msg']) ) {

        if ( $_GET['msg'] == 'sent' ) {

            echo "<div class='broadcast-handle-message success-bh'><p><strong>Broadcast message sent.</strong></p></div>";

        } else if ( $_GET['msg'] == 'ii' ) {

            echo "<div class='broadcast-handle-message ii-bh'><p><strong>All fields are required.</strong></p></div>";

        } else if ( $_GET['msg'] == 'error' ) {

            echo "<div class='broadcast-handle-message error-bh'><p><strong>Error.</strong></p></div>";

        }

    }
    ?>

    <form action="<?php echo admin_url('admin-post.php'); ?>" method="post" id="mc-form">

        <input type="hidden" name="action" value="handle_broadcast_message">

        <?php wp_nonce_field( 'send_broadcast_message', 'safesaver' ); ?>

        <table class="form-table">
            <tbody>

                <tr>
                    <th scope="row">
                        <label for="bws_sweepstakes_mc_recipients_type">Recipients</label>
                    </th>
                    <td>
                        <fieldset>
                            <label>
                                <input type="radio" value="vendors" name="bws_sweepstakes_mc_recipients_type">
                                Sellers
                            </label>
                            <br>
                            <label>
                                <input type="radio" value="contestants" name="bws_sweepstakes_mc_recipients_type">
                                Members
                            </label>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="bws_sweepstakes_mc_recipients_zips">Recipients Zip-Codes (separated by space)</label>
                    </th>
                    <td>
                        <input type="text" name="bws_sweepstakes_mc_recipients_zips" class="mc" placeholder="00544 04929 ...">
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="bws_sweepstakes_mc_subject">Subject</label>
                    </th>
                    <td>
                        <input type="text" name="bws_sweepstakes_mc_subject" class="mc" placeholder="Another letter...">
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="bws_sweepstakes_mc_message">Message</label>
                    </th>
                    <td>
                        <textarea name="bws_sweepstakes_mc_message" class="mc-message" placeholder="Broadcast message..."></textarea>
                    </td>
                </tr>

            </tbody>
        </table>
        <p class="submit">

        <?php submit_button( 'Send Broadcast Message', 'primary spaced', 'upload' , false ); ?>
        <a href="#clear" id="mc-clear" class="button button-secondary">Clear Fields</a>

        </p>
    </form>
</div>
