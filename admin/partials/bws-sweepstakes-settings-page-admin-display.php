<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/admin/partials
 */

?>

<div class="wrap">
    <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

    <form id="manual-drawings-form">
        <?php
        do_settings_sections( $this->plugin_name . '-manual-drawings-settings' );
        ?>
        <p class="submit">
            <a href="#save" class="button button-primary" id="manual-drawings-save">Save Changes</a>
        </p>
    </form>

    <form action="options.php" method="post">
        <?php
        settings_fields( $this->plugin_name );
        do_settings_sections( $this->plugin_name . '-drawings-settings' );
        submit_button();
        do_settings_sections( $this->plugin_name . '-pages-settings' );
        submit_button();
        ?>
    </form>

    <form action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post" enctype="multipart/form-data">
        <?php
        //settings_fields( $this->plugin_name );
        wp_nonce_field();
        wp_referer_field();
        do_settings_sections( $this->plugin_name . '-zip-codes-settings' );
        $redirect = urlencode( $_SERVER['REQUEST_URI'] );
        ?>

        <p class="submit">

            <input type="hidden" name="action" value="handle_zip_codes_settings">

            <?php
            submit_button( 'Upload', 'primary spaced', 'upload' , false );
            submit_button( 'Delete all', 'delete spaced', 'delete' , false );
            ?>

            <a href="<?php echo wp_upload_dir()['baseurl'] ?>/zip-codes.csv" class="button button-secondary" target="_blank">Download</a>

        </p>
    </form>

    <form action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post">
    <?php
    //settings_fields( $this->plugin_name );
    wp_nonce_field();
    wp_referer_field();
    do_settings_sections( $this->plugin_name . '-users-points-settings' );
    ?>

        <input type="hidden" name="action" value="handle_awards_points_settings">

    <?php submit_button(); ?>
    </form>

    <!--<form action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post">
        <?php
        wp_nonce_field();
        wp_referer_field();
        do_settings_sections( $this->plugin_name . '-users-registration-settings' );
        ?>

        <input type="hidden" name="action" value="handle_users_registration_settings">

        <?php submit_button(); ?>
    </form>-->
</div>
