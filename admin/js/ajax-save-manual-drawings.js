jQuery(function($){

    var button = $('#manual-drawings-save');

    button.click(function(){

        var checkbox = $('#bws_sweepstakes_manual_drawings_checkbox');
        var is_md;

        if(checkbox.is(':checked')) {
            is_md = 'yes';
        } else {
            is_md = 'no';
        }

        var day = $('#bws_sweepstakes_manual_drawings_start_day').val();
        var time = $('#bws_sweepstakes_manual_drawings_start_time').val();
        var cs = $('#bws_sweepstakes_manual_drawings_city').val();

        $.ajax({

            type:'POST',
            url:ajaxurl,
            data:{
                action:     'save_manual_drawings',
                is_md:      is_md,
                day:        day,
                time:       time,
                cs:         cs,
            },

            beforeSend:function(xhr){
                button.attr('readonly','readonly').text('Saving...');
            },

            success:function(results){
                button.removeAttr('readonly').text('Saved');
            }

        });
    });

});