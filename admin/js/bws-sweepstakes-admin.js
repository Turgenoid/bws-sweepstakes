(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

    $(window).load(function() {
        /**
         * Check zip-codes by city
         */
        $('#check-city' ).click(function () {
            var city = $('#zip-city').val();

            $('#zip_codes_values').html(cities[city].join(', '));
        });

        /**
         * Option group hide/show on Add New Product screen
         */
        prize_options_sh();

        $('#_simple_prize').click(function () {
            prize_options_sh();
            $('#_external_prize').prop('checked', false);
        });

        $('#_external_prize').click(function () {
            $('#_simple_prize').prop('checked', false);
        });

        $('#product-type').change(function() {
            prize_options_sh();
        });

        // Enable datepicker
        if ($('#bws_sweepstakes_drawing_end_date').length) {

            $('#bws_sweepstakes_drawing_end_date').datepicker({
                    defaultDate: '',
                    dateFormat: 'yy-mm-dd',
                    numberOfMonths: 1,
                    showButtonPanel: true,
                }
            );

        }

        /**
         * Endless drawing & End date hide/show on Add New Product screen
         */
        var checkbox = $('#bws_sweepstakes_endless_drawing');
        var frequency = $('#bws_sweepstakes_drawing_frequency');

        end_date_fieldset_sh( checkbox, frequency );

        // Frequency
        $('#bws_sweepstakes_drawing_frequency').on('change', function () {

            end_date_fieldset_sh(checkbox, frequency);

        });

        // Checkbox
        $('#bws_sweepstakes_endless_drawing').on('change', function () {

            end_date_sh(checkbox, frequency);

        });

        // Reset broadcast messages form for Mail Center
        $('#mc-clear').click(function (event) {

            $('#mc-form')[0].reset();

        });
    });

    function prize_options_sh() {

        if ($('#_simple_prize').is(':checked') && $('.show_if_simple.tips').is(':visible')) {
            $('.show_if_simple_prize').css('display', 'block');
        } else {
            $('.show_if_simple_prize').css('display', 'none');
        }

    }

    function end_date_sh( checkbox, frequency ) {

        if (!checkbox.is(':checked') && frequency.val() != 'stop') {
            $('.bws_sweepstakes_drawing_end_date_field').show();
        } else {
            $('.bws_sweepstakes_drawing_end_date_field').hide();
        }

    }

    function end_date_fieldset_sh(checkbox, frequency) {
        if (frequency.val() == 'stop' ) {
            $('.bws_sweepstakes_drawing_end_date_fieldset').hide();
        } else {
            $('.bws_sweepstakes_drawing_end_date_fieldset').show();
            end_date_sh(checkbox, frequency);
        }
    }

})(jQuery);