jQuery(function($){

    $(window).load(function() {

        var cs_element = $('#bws_sweepstakes_manual_drawings_city');
        var button = $('#manual-drawings-save');

        get_manual_drawing();

        cs_element.change(function () {
            get_manual_drawing();
        });

        function get_manual_drawing() {

                var cs = cs_element.val();
                var checkbox = $('#bws_sweepstakes_manual_drawings_checkbox');
                var day = $('#bws_sweepstakes_manual_drawings_start_day');
                var time = $('#bws_sweepstakes_manual_drawings_start_time');

                $.ajax({

                    type: 'POST',
                    url: ajaxurl,
                    data: {
                        action: 'get_manual_drawings',
                        cs:     cs,
                    },

                    beforeSend:function(xhr){
                        button.attr('readonly','readonly').text('Loading...');
                    },

                    success: function (results) {
                        var data = JSON.parse(results);

                        if(data.type === 'manual') {
                            checkbox.prop('checked', true);
                        } else {
                            checkbox.prop('checked', false);
                        }

                        day.val(data.next_date.day);
                        time.val(data.next_date.time);

                        button.removeAttr('readonly').text('Save Changes');

                        manual_drawings_date_sh();

                    }

                });

        }

        /**
         * Date&Time hide/show for settings of the Manual Drawings
         */
        manual_drawings_date_sh();

        $('#bws_sweepstakes_manual_drawings_checkbox').change( function() {
            manual_drawings_date_sh();
        });

        /* Updates 'Save Changes' text after changing values of elements */
        var md_elements = [
            $('#bws_sweepstakes_manual_drawings_checkbox'),
            $('#bws_sweepstakes_manual_drawings_start_day'),
            $('#bws_sweepstakes_manual_drawings_start_time')
        ];

        $.each(md_elements, function(key, element) {
            element.change(function() {
                button.removeAttr('readonly').text('Save Changes');
            });
        });

        function manual_drawings_date_sh() {

            if ($('#bws_sweepstakes_manual_drawings_checkbox').is(':checked')) {

                $('#bws_sweepstakes_manual_drawings_start_day').parents('tr').css('display', 'table-row');
                $('#bws_sweepstakes_manual_drawings_start_time').parents('tr').css('display', 'table-row');

            } else {

                $('#bws_sweepstakes_manual_drawings_start_day').parents('tr').css('display', 'none');
                $('#bws_sweepstakes_manual_drawings_start_time').parents('tr').css('display', 'none');

            }

        }

    });

});