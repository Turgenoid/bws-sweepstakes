jQuery(function($) {

    $(window).load(function () {

        let cs_element = $('#place-selection-md');
        let pw_button = $('#pick-winners-button');
        let n_button = $('#send-notifications-button');
        let winners_list = $('#winners-list');

        cs_element.change(function () {
            let cs = cs_element.val();

            if(cs == 'do-select') {
                clear_widget();
            } else {
                get_manual_drawing();
            }
        });

        pw_button.click(function () {
            show_winners_slowly();
            save_manual_drawing();
        });

        n_button.click(function() {
            send_notifications();
        });

        function get_manual_drawing() {

            let cs = cs_element.val();

            $.ajax({

                type: 'POST',
                url: ajaxurl,
                data: {
                    action: 'get_certain_manual_drawing',
                    cs:     cs,
                },

                beforeSend:function(xhr){
                    winners_list.empty();
                    pw_button.prop('disabled', true);
                    pw_button.attr('readonly','readonly').text('Loading...');
                },

                success: function (results) {
                    let data = JSON.parse(results);

                    for(var i = 0; i < data.count; i++) {
                        winners_list.append(data[i]);
                    }

                    pw_button.prop('disabled', false);
                    pw_button.removeAttr('readonly').text('Pick Winners');

                    if(data.state == 'temp') {
                        pw_button.show();
                        n_button.hide();
                    } else {
                        pw_button.hide();
                        show_winners_slowly();
                        n_button.show();
                    }

                }

            });

        }

        function save_manual_drawing() {

            let cs = cs_element.val();

            $.ajax({

                type: 'POST',
                url: ajaxurl,
                data: {
                    action: 'save_certain_manual_drawing',
                    cs:     cs,
                },

                beforeSend:function(xhr){
                    pw_button.prop('disabled', true);
                    pw_button.attr('readonly','readonly').text('Picking...');
                },

                success: function (results) {

                    pw_button.prop('disabled', false);
                    pw_button.removeAttr('readonly').text('Pick Winners');
                    pw_button.hide();

                    if(results == 'saved') {
                        n_button.show();
                    }

                }

            });

        }

        function send_notifications() {

            let cs = cs_element.val();

            $.ajax({

                type: 'POST',
                url: ajaxurl,
                data: {
                    action: 'send_notifications_for_certain_manual_drawing',
                    cs:     cs,
                },

                beforeSend:function(xhr){
                    n_button.prop('disabled', true);
                    n_button.attr('readonly','readonly').text('Sending...');
                },

                success: function (results) {

                    n_button.prop('disabled', false);
                    n_button.removeAttr('readonly').text('Send Notifications');

                }

            });

        }

        function clear_widget() {
            winners_list.empty();
            pw_button.hide();
            n_button.hide();
        }

    });

    function show_winners_slowly() {
        // Show winners slowly
        $('#winners-list > li').each(function(i, elem) {
            $(elem).delay((i+1)*2000).show('fast');
        });
    }

});