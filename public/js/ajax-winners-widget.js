jQuery(function($) {

    $(window).load(function () {

        $('.resend-notification-button').click(function () {

            let ipoint = $(this);
            let winner = $(this).attr('winner');
            let drawing = $(this).attr('drawing');

            resend_notifications(winner, drawing, ipoint);

        });

        function resend_notifications(winner, drawing, ipoint) {

            $.ajax({

                type: 'POST',
                url: ajaxurl,
                data: {
                    action: 'resend_notifications_for_certain_winner',
                    winner: winner,
                    drawing: drawing,
                },

                beforeSend: function (xhr) {
                    ipoint.prop('disabled', true);
                    ipoint.prop('readonly', 'readonly').text('Sending...');
                },

                success: function (results) {

                    ipoint.prop('disabled', false);
                    ipoint.removeAttr('readonly').text('Resend Notifications');
                    ipoint.hide();

                }

            });

        }

    });

});