<?php
/**
 * myCRED Hook for Purchase Product
 * @version 1.2
 */
class BWS_Sweepstakes_Purchase_Product_Hook extends myCRED_Hook {

    /**
     * Construct
     * Used to set the hook id and default settings.
     */
    function __construct( $hook_prefs, $type ) {

        parent::__construct( array(
            'id'       => 'purchase_product',
            'defaults' => array(
                'creds'   => 100,
                'log'     => '%plural% for Purchase Product'
            )
        ), $hook_prefs, $type );

    }

    /**
     * Run
     * Fires by myCRED when the hook is loaded.
     * Used to hook into any instance needed for this hook
     * to work.
     *
     * TODO: Need to develop the logic
     */
    public function run() {


    }

    /**
     * Hook Settings
     * Needs to be set if the hook has settings.
     */
    public function preferences() {

        // Our settings are available under $this->prefs
        $prefs = $this->prefs;
        ?>
        <!-- First we set the amount -->
        <label class="subheader"><?php echo $this->core->plural(); ?></label>
        <ol>
            <li>
                <div class="h2"><input type="text" name="<?php echo $this->field_name( 'creds' ); ?>" id="<?php echo $this->field_id( 'creds' ); ?>" value="<?php echo esc_attr( $prefs['creds'] ); ?>" size="8" /></div>
            </li>
        </ol>

        <!-- Then the log template -->
        <label class="subheader"><?php _e( 'Log template', 'mycred' ); ?></label>
        <ol>
            <li>
                <div class="h2"><input type="text" name="<?php echo $this->field_name( 'log' ); ?>" id="<?php echo $this->field_id( 'log' ); ?>" value="<?php echo esc_attr( $prefs['log'] ); ?>" class="long" /></div>
            </li>
        </ol>
        <?php

    }

    /**
     * Sanitize Preferences
     * If the hook has settings, this method must be used
     * to sanitize / parsing of settings.
     */
    public function sanitise_preferences( $data ) {

        $data['creds'] = ( !empty( $data['creds'] ) ) ? $data['creds'] : $this->defaults['creds'];

        return $data;

    }

}