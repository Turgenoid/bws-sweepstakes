<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Implement Widget for Location
 *
 * Class BWS_Sweepstakes_Widget_Location
 */
class BWS_Sweepstakes_Widget_Location extends WP_Widget {


    /**
     * BWS_Sweepstakes_Widget_Location constructor. Create Widget.
     */
    function __construct() {
        parent::__construct(
            'bws_location_widget',
            'Location',
            array( 'description' => 'Allows you to display the location.' )
        );
    }

    /**
     * Widget Front-End
     *
     * @param array $args
     * @param array $instance
     */
    public function widget( $args, $instance ) {
        $title = apply_filters('widget_title', $instance['title']);
        $posts_per_page = $instance['posts_per_page'];

        echo $args['before_widget'];

        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

        $zip_query_var = get_query_var('zip');
        $zc_query_var = get_query_var('zc');

        // For main Shop Page
        if ( empty( $zip_query_var ) && empty( $zc_query_var ) ) {

            echo '<p><a href="' . home_url() . '">Choose</a> a specific city</p>';

        // For pages with selected zip-code
        } else if ( !empty( $zip_query_var ) ) {

            $zip_obj = BWS_Sweepstakes_Share_Functions::get_zip_code_data( $zip_query_var );

            if ( $zip_obj !== false ) {

                $current_zip_code = "";
                if ( !empty( $zc_query_var ) ) {
                    $current_zip_code = "<strong>$zip_query_var</strong>, ";
                }

                echo $current_zip_code . "<a href='?zip=$zip_query_var'>$zip_obj->city</a>, $zip_obj->state";

                $zip_codes_filtered = BWS_Sweepstakes_Share_Functions::get_zip_codes_by_contestants();

                echo "<ul class='sidebar-zip'>";

                foreach ( $zip_codes_filtered[$zip_obj->state][$zip_obj->city] as $zip_code ) {

                    echo "<li><a href='?zip=$zip_code&zc=done'>$zip_code</a></li>";

                }

                echo "</ul>";

            }

            echo '<p class="another-city"><a href="' . home_url() . '">Choose</a> another city</p>';

        } else {

            echo '<p class="another-city"><a href="' . home_url() . '">Choose</a> another location</p>';

        }

        echo $args['after_widget'];
    }

    /**
     * Widget Back-End
     *
     * @param array $instance
     * @return string|void
     */
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
    }

    /**
     * Save Widget settings
     *
     * @param array $new_instance
     * @param array $old_instance
     * @return array
     */
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $instance['posts_per_page'] = ( is_numeric( $new_instance['posts_per_page'] ) ) ? $new_instance['posts_per_page'] : '5'; // по умолчанию выводятся 5 постов
        return $instance;
    }

}