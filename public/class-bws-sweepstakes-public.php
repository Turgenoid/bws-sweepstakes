<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    BWS_Sweepstakes
 * @subpackage BWS_Sweepstakes/public
 * @author     Oleg Sokolov <turgenoid@gmail.com>
 */
class BWS_Sweepstakes_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

    /**
     * @var string
     */
	private $option_name = 'bws_sweepstakes';

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/bws-sweepstakes-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in BWS_Sweepstakes_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The BWS_Sweepstakes_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

        // Enqueue script for clipboard
        if( get_the_ID() ==  get_option( $this->option_name . '_users_dashboard_page' ) ) {
            wp_enqueue_script( $this->plugin_name . '-clipboard-lib', plugin_dir_url( __FILE__ ) . 'js/clipboard.min.js', array(), $this->version, false );
        }

		wp_enqueue_script( $this->plugin_name . '-public-js', plugin_dir_url( __FILE__ ) . 'js/bws-sweepstakes-public.js', array( 'jquery' ), $this->version, true );

        wp_enqueue_script( $this->plugin_name . '-ajax-manual-drawings-widget', plugin_dir_url(__FILE__) . 'js/ajax-manual-drawings-widget.js', array('jquery'), $this->version, true );
        wp_enqueue_script( $this->plugin_name . '-ajax-winners-widget', plugin_dir_url(__FILE__) . 'js/ajax-winners-widget.js', array('jquery'), $this->version, true );

	}

    /**
     * Register shortcodes for the plugin
     */
    public function register_shortcodes() {

        add_shortcode( 'bws_sweepstakes_members_dashboard', array( $this, 'register_users_dashboard_shortcode' ) );
        add_shortcode( 'bws_sweepstakes_voucher_tracking_page', array( $this, 'register_voucher_tracking_page_shortcode' ) );
        add_shortcode( 'bws_sweepstakes_redirect_page', array( $this, 'redirect_to_right_dashboard' ) );
        add_shortcode( 'bws_sweepstakes_manual_drawings_widget', array( $this, 'render_manual_drawings_widget' ) );
        add_shortcode( 'bws_sweepstakes_winners_widget', array( $this, 'render_winners_widget' ) );

    }

    /**
     * Display Users Dashboard
     */
    public function register_users_dashboard_shortcode() {

        $users_dashboard_page = get_option( $this->option_name . '_users_dashboard_page' );

        if( get_the_ID() != $users_dashboard_page  ) {
            return;
        }

        if ( !is_user_logged_in() ) {
            wp_redirect( wp_login_url() );
        } else if ( ( !in_array( 'subscriber', get_userdata( get_current_user_id() )->roles ) && !in_array( 'participant', get_userdata( get_current_user_id() )->roles ) ) ) {
            echo '<p class="users-dashboard-error">This Dashboard available only for Members! Please, use the WP Dashboard for admins and the Sellers Dashboard for Sellers!</p>';
            return;
        }

        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/bws-sweepstakes-users-dashboard.php' );

    }

    /**
     * Get users ratings for Users Dashboard
     *
     * @param $user_id
     * @param $side_elements_count
     * @return array
     */
    public function get_users_ratings_for_dashboard( $user_id, $side_elements_count ) {

        $contestants = BWS_Sweepstakes_Share_Functions::get_contestants();
        $contestants_count = count( $contestants );

        for( $i = 0; $i < $contestants_count; $i++ ) {
            for( $j = $i + 1; $j < $contestants_count; $j++ ) {

                if( $contestants[$i]->points < $contestants[$j]->points ) {

                    $temp = $contestants[$j];
                    $contestants[$j] = $contestants[$i];
                    $contestants[$i] = $temp;

                }

            }
        }

        foreach ( $contestants as $key => $contestant ) {

            if( $contestant->id == $user_id ) {
                $user_position = $key;
                break;
            }

        }

        $result_contestants = array();

        $left_side_elements_count = floor( $side_elements_count / 2 );
        $right_side_elements_count = ceil( $side_elements_count / 2 );

        $left_user = $user_position - $left_side_elements_count;
        $right_user = $user_position + $right_side_elements_count;

        $left_user = ($left_user >= 0) ? $left_user : 0;
        $right_user = ($right_user < $contestants_count) ? $right_user : $contestants_count - 1;

        for( $i = $left_user; $i <= $right_user; $i++ ) {

            $contestants[$i]->rank_position = $i + 1;
            $result_contestants[] = $contestants[$i];

        }

        return $result_contestants;

    }

    /**
     * Display Voucher Tracking Page
     */
    public function register_voucher_tracking_page_shortcode() {

        $voucher_tracking_page = get_option( $this->option_name . '_voucher_tracking_page' );

        if( get_the_ID() != $voucher_tracking_page  ) {
            return;
        }

        if ( !is_user_logged_in() ) {
            wp_redirect( wp_login_url() );
        }

        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/bws-sweepstakes-voucher-tracking-page.php' );

    }

    /**
     * Redirect a user to his Dashboard by the role
     */
    public function redirect_to_right_dashboard() {

        $redirect_page = get_option( $this->option_name . '_redirect_page' );
        $users_dashboard_page = get_option( $this->option_name . '_users_dashboard_page' );
        $vendors_dashboard_page = get_option( 'wcvendors_dashboard_page_id' );

        if( get_the_ID() != $redirect_page  ) {
            return;
        }

        if ( !is_user_logged_in() ) {

            wp_redirect( wp_login_url() );

        } else if( ( in_array( 'subscriber', get_userdata( get_current_user_id() )->roles ) || in_array( 'participant', get_userdata( get_current_user_id() )->roles ) ) ) {

            wp_redirect( get_permalink( $users_dashboard_page ) );

        } else if( ( in_array( 'vendor', get_userdata( get_current_user_id() )->roles ) ) ) {

            wp_redirect( get_permalink( $vendors_dashboard_page ) );

        } else if( in_array( 'administrator', get_userdata( get_current_user_id() )->roles ) ) {

            wp_redirect( admin_url() );

        } else {

            wp_redirect( home_url() );

        }

    }

    /**
     * Display widget for Manual Drawings
     *
     * @return false|string|void
     */
    public function render_manual_drawings_widget() {

        if( !in_array( 'administrator', get_userdata( get_current_user_id() )->roles ) )
            return;

        ob_start();

        $drawings = BWS_Sweepstakes_Drawing::get_manual_drawings_for_widget();
        ?>
        <div class="manual-drawings-widget">
            <select class='place-selection' id="place-selection-md" name="place">
                <option value="do-select">Choose the place</option>
                <?php
                foreach ( $drawings as $drawing ) {
                    $place = $drawing->get_place();
                    echo "<option value='" . $place['city'] . ";" . $place['state'] . "'>" . $place['city'] . ", " . $place['state'] . "</option>";
                }
                ?>
            </select>

            <button class="md-widget-button" id="pick-winners-button">Pick Winners</button>
            <button class="md-widget-button" id="send-notifications-button">Send Notifications</button>
        </div>

        <?php
        echo "<ul id='winners-list'>";
        echo "</ul>";
        $output = ob_get_contents();
        ob_end_clean();

        return $output;

    }

    /**
     * Display widget for winners
     *
     * @return false|string|void
     */
    public function render_winners_widget() {

        if( !is_admin() )
            return;

        ob_start();

        $drawings = BWS_Sweepstakes_Drawing::get_all_drawings_from_db();

        echo "<div class='winners-widget'>";

        foreach ( $drawings as $drawing ) {

            $winners_without_prizes = $drawing->get_winners_without_prizes();
            $winnings = $drawing->get_winnings();
            $dt = new DateTime();
            $dt->setTimestamp( $drawing->get_time() );
            $tz = new DateTimeZone( 'America/Denver' );
            $dt->setTimezone( $tz );
            $datetime = $dt->format( 'l, M jS, Y \a\t g:i A (T)' );
            $drawing_id = $drawing->get_id();

            echo "<div class='drawing-winners'>";

            echo "<h2>$datetime</h2>";

            foreach ( $winnings as $winner_id => $prizes ) {

                $winner_name = xprofile_get_field_data(1, $winner_id ) . " " . xprofile_get_field_data(6, $winner_id );

                echo "<div class='winner'>";

                echo "<div class='winner-header'>";

                echo "<h3>$winner_name</h3>";

                if ( key_exists( $winner_id, $winners_without_prizes ) && in_array( 'administrator', get_userdata( get_current_user_id() )->roles ) ) {
                    echo "<button class='resend-notification-button' winner='$winner_id' drawing='$drawing_id'>Resend Notification</button>";
                }

                echo "</div><ul>";

                foreach ( $prizes as $prize_id ) {

                    $prize_object = get_post( $prize_id );
                    $title = $prize_object->post_title;
                    $permalink = get_permalink( $prize_id );

                    $retail_price = get_post_meta( $prize_id, '_regular_prize', true  );
                    if ( empty( $retail_price ) ) {
                        $retail_price = 'Free';
                    }

                    echo "<li><a href='$permalink' target='_blank'>$title</a> - <span class='prize-retail-price'>[$retail_price]</span> -";

                    if ( in_array( $prize_id, $winners_without_prizes[$winner_id] ) ) {
                        $prize_state = '<span class="prize-waiting">[Waiting to Receive!]</span>';
                    } else {
                        $prize_state = '<span class="prize-received">[Received]</span>';
                    }

                    echo " <span class='prize-state'>$prize_state</span>";

                    echo "</li>";

                }

                echo "</ul>";

                echo "</div>";

            }

            echo "</div>";

        }

        echo "</div>";

        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/bws-sweepstakes-winners-widget.php' );

        $output = ob_get_contents();
        ob_end_clean();

        return $output;

    }

    /**
     * Body class for Users Dashboard
     *
     * @param $classes
     * @return array
     */
    public function add_class_for_body_users_dashboard( $classes ) {

        $users_dashboard_page_id = get_option( $this->option_name . '_users_dashboard_page' );
        if ( get_the_ID() == $users_dashboard_page_id ) {
            $classes[] = 'users-dashboard';
        }

        return $classes;

    }

    /**
     * Body class for Voucher Tracking Page
     *
     * @param $classes
     * @return array
     */
    public function add_class_for_body_voucher_tracking_page( $classes ) {

        $voucher_tracking_page = get_option( $this->option_name . '_voucher_tracking_page' );
        if ( get_the_ID() == $voucher_tracking_page ) {
            $classes[] = 'voucher-tracking-page';
        }

        return $classes;

    }

    /**
     * Body class for Sales Page
     *
     * @param $classes
     * @return array
     */
    public function add_class_for_body_sales_page( $classes ) {

        $sales_page_id = get_option( $this->option_name . '_sales_page' );
        if ( get_the_ID() == $sales_page_id ) {
            $classes[] = 'sales-page';
        }

        return $classes;

    }

    /**
     * Redirect users after login to the right dashboard
     *
     * @param $redirect
     * @param $user
     * @return false|string
     */
    public function redirect_users_after_login_to_dashboard( $redirect, $user ) {

        $redirect_page_id = url_to_postid( $redirect );
        $checkout_page_id = wc_get_page_id( 'checkout' );
        $redirect_page = get_option( $this->option_name . '_redirect_page' );

        if( $redirect_page_id == $checkout_page_id ) {
            return $redirect;
        }

        return get_permalink( $redirect_page );

    }

    /**
     * Redirect after BuddyPress login
     *
     * @return false|string
     */
    public function redirect_after_buddypress_login() {

        $redirect_page = get_option( $this->option_name . '_redirect_page' );

        return get_permalink( $redirect_page );

    }

    /**
     * Add custom hooks for myCRED plugin
     *
     * @param $installed
     * @return mixed
     */
	public function mycred_add_hooks( $installed ) {

        $installed['purchase_product'] = array(
            'title'        => 'Points for purchase product',
            'description'  => 'Points gave for purchase products.',
            'callback'     => array( 'BWS_Sweepstakes_Purchase_Product_Hook' )
        );

        return $installed;

    }

    /**
     * Load custom hooks for myCRED plugin
     */
    public function mycred_load_hooks() {

        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/class-bws-sweepstakes-mycred-custom-hooks.php';

    }

    /**
     * Get order for purchasing products
     *
     * @param $order_id
     */
    function mycred_reward_for_purchasing( $order_id ) {

        // Make sure we do not crash our site if myCRED gets disabled
        if ( ! function_exists( 'mycred' ) ) return;

        $mycred_pref_hooks = get_option( 'mycred_pref_hooks' );
        $purchase_award_points = $mycred_pref_hooks['hook_prefs']['purchase_product']['creds'];
        $purchase_award_desc = $mycred_pref_hooks['hook_prefs']['purchase_product']['log'];

        // The point type to payout
        $point_type = MYCRED_DEFAULT_TYPE_KEY;

        // Load myCRED
        $mycred     = mycred( $point_type );

        // Get Order
        $order      = wc_get_order( $order_id );
        $items      = $order->get_items();

        // Make sure we only get paid once per order
        if ( $mycred->has_entry( 'purchasing_product', $order_id, $order->user_id ) ) return;

        $reward     = 0;

        foreach ( $items as $item ) {
            //$product_id        = absint( $item['product_id'] );
            $reward += $purchase_award_points;
        }

        // Zero points = no payout
        if ( $reward == 0 ) return;

        // The payout
        $mycred->add_creds(
            'purchasing_product',
            $order->user_id,
            $reward,
            $purchase_award_desc,
            $order_id,
            array( 'ref_type' => 'post' )
        );

    }

    /**
     * Hide title for shop page
     *
     * @return bool
     */
    public function hide_shop_title() {
        return false;
    }

    /**
     * Get columns count for prizes
     */
    public function get_loop_columns_count( $number_columns ) {

        return 3;

    }

    /**
     * Add the Terms&Conditions tab to the WooCommerce product
     *
     * @param $product_data_tabs
     */
    public function add_product_terms_tab( $product_data_tabs ) {

        if( get_post_meta( get_the_ID(), '_external_prize', true ) != 'yes' ) {
            return;
        }

        $product_data_tabs['terms-tab'] = array(
            'title'     =>  __( 'Terms & Conditions', $this->option_name ),
            'priority'  =>  5,
            'callback'  =>  array( $this, 'terms_tab_content' ),
        );

        return $product_data_tabs;

    }

    /**
     * Show content for the Terms&Conditions tab
     */
    public function terms_tab_content() {

        echo get_post_meta( get_the_ID(), $this->option_name . '_external_terms', true );

    }

    /**
     * Delete the 'Buy' button for external prizes
     *
     * @param $button
     * @param $product
     * @param $args
     */
    public function delete_buy_button_for_external_prizes_loop( $button, $product, $args ) {

        if( FALSE === strripos( $args['class'], 'product_type_external' ) ) {

            return $button;

        } else {

            return;

        }

    }

    /**
     * Filter prizes on the Shop Page
     *
     * @param $query
     */
    public function filter_prizes( $query ) {

        if( is_tax( 'city' ) === false ) {

            $drawing = BWS_Sweepstakes_Drawing::get_next_drawing_by_place(array('city' => 'Albuquerque', 'state' => 'New Mexico'));
            if ($drawing === null) {
                $prizes_ids = array(0);
            } else {
                $prizes_ids = $drawing->get_prizes();
            }

            $query->set('post__in', $prizes_ids);

        }

    }

    /**
     * Filter products on the Shop Page
     *
     * @param $query_args
     * @return mixed
     */
    public function filter_products( $query_args ) {

        // Get vendors from Albuquerque
        $vendors = BWS_Sweepstakes_Share_Functions::get_vendors_by_city_state( false, 'Albuquerque', 'New Mexico', 'current' );
        $products_ids = array( 0 );

        foreach ( $vendors as $vendor ) {

            foreach ( $vendor->products as $product ) {

                $products_ids[] = $product->ID;

            }

        }

        $meta_query = array(
            'relation'  =>  'OR',
            array(
                array(
                    'key'       =>  '_simple_prize',
                    'value'     =>  'no',
                    'compare'   =>  'LIKE',
                ),
                array(
                    'key'       =>  '_external_prize',
                    'value'     =>  'no',
                    'compare'   =>  'LIKE',
                ),
            ),
            array(
                array(
                    'key'       =>  '_simple_prize',
                    'compare'   =>  'NOT EXISTS',
                ),
                array(
                    'key'       =>  '_external_prize',
                    'compare'   =>  'NOT EXISTS',
                ),
            )
        );

        $query_args['post__in'] = $products_ids;
        $query_args['meta_query'] = $meta_query;
        $query_args['orderby'] = 'meta_value_num';
        $query_args['meta_key'] = '_discount_percentage';
        $query_args['order'] = 'DESC';

        return $query_args;

    }

    /**
     * Customize the 'purchasable' property of a product
     *
     * @param $purchasable
     * @param $product
     * @return bool
     */
    public function customize_purchasable( $purchasable, $product ) {

        if( $product->exists() && ( 'publish' === $product->get_status() || current_user_can( 'edit_post', $product->get_id() ) ) )
            $purchasable = true;

        return $purchasable;

    }

    /**
     * Add to cart item data about zero price
     *
     * @param $cart_item_data
     * @param $product_id
     * @param $variation_id
     */
    public function add_cart_item_data_for_zero_price( $cart_item_data, $product_id, $variation_id ) {

        $drawing_id = sanitize_key( $_GET['drawing'] );

        if ( empty( $drawing_id ) || empty( $product_id ) ) {
            return;
        }

        if ( !BWS_Sweepstakes_Drawing::is_drawing_exists( $drawing_id ) ) {
            return;
        }

        $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( $drawing_id );
        $orders_received_prizes = $drawing->get_received_prizes();

        $cart_item_data['drawing_id'] = $drawing_id;

        if ( $orders_received_prizes[$product_id] === false ) {
            $cart_item_data['is_prize'] = true;
        }

        return $cart_item_data;

    }

    /**
     * Set zero prices for products in the cart
     *
     * @param $cart_object
     */
    public function set_zero_price_before_calculate_totals( $cart_object ) {

        global $woocommerce;

        foreach ( $cart_object->get_cart() as $key => $value ) {

            if ( empty( $value['drawing_id'] ) ) {
                continue;
            }

            $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( $value['drawing_id'] );

            $is_winner = false;
            if ( key_exists( get_current_user_id(), $drawing->get_winnings() ) && isset( $value['is_prize'] ) ) {
                $value['data']->set_price( 0 );
                $value['data']->set_sold_individually( true );
            }

        }

    }

    /**
     * Mark received prizes
     *
     * @param $order_id
     */
    public function mark_prize_checkout_process( $order_id ) {

        $items = WC()->cart->get_cart();

        foreach ( $items as $item => $value ) {

            if ( empty( $value['drawing_id'] ) ) {
                continue;
            }

            $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( $value['drawing_id'] );
            $product_id = $value['product_id'];
            $orders_received_prizes = $drawing->get_received_prizes();
            $orders_received_prizes[$product_id] = $order_id;
            $drawing->set_received_prizes( $orders_received_prizes );
            $drawing->update_received_prizes();

        }

    }

    /**
     * Save the drawing id to order metadata
     *
     * @param $item
     * @param $cart_item_key
     * @param $values
     * @param $order
     */
    public function add_drawing_id_to_order_meta_data( $item, $cart_item_key, $values, $order ) {

        if( !empty( $values['drawing_id'] ) ) {
            $item->add_meta_data( '_drawing_id', $values['drawing_id'] );
        }

    }

    /**
     * Limit multiple prizes
     *
     * @param $cart_item_key
     * @param $product_id
     * @param $quantity
     * @param $variation_id
     * @param $variation
     * @param $cart_item_data
     */
    public function limit_multiple_prizes( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ) {

        if( $cart_item_data['is_prize'] ) {
            WC()->cart->set_quantity( $cart_item_key, 1 );
        }

    }

    /**
     * Do redirect after checkout if purchase is successful
     *
     * @param $order_id
     */
    public function redirect_after_checkout( $order_id ) {

        $order = new WC_Order( $order_id );

        $redirect_page = get_option( $this->option_name . '_redirect_page' );
        $url = get_permalink( $redirect_page );

        if ( $order->status != 'failed' ) {
            wp_redirect( $url );
            exit;
        }

    }

    /**
     * Save meta fields for products
     *
     * @param $post_id
     */
    public function save_products_meta_fields( $post_id ) {
        BWS_Sweepstakes_Share_Functions::save_products_meta_fields( $post_id );
    }

    /**
     * Run drawings
     */
    public function run_drawings() {

        $drawings = BWS_Sweepstakes_Drawing::get_drawings( true );

        /*include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-bws-sweepstakes-drawing.php' );
        $drawings = array();

        $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( 1 );
        $drawings[] = $drawing;

        $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( 2 );
        $drawings[] = $drawing;

        //print_r( $drawings );*/

        /*foreach ( $drawings as $drawing ) {

            $winner_id = $drawing->get_winner_id();
            $winner_chances = $drawing->get_contestant_by_id( $winner_id )->chance;
            $winner_chances = floor( $winner_chances );

            echo "<h3>Drawing on <p style='color: #0b608a; display: inline-block;'>" . self::zip_code_add_zeros( $drawing->get_zip_code() ) . "</p></h3>Winner: <strong>" . xprofile_get_field_data( '1', $winner_id ) . ' ' . xprofile_get_field_data( '6', $winner_id ) . "</strong><br>His chance was <strong>$winner_chances%</strong><br><br>";

            echo '<h4>Prizes:</h4><ul>';
            foreach ( $drawing->get_prizes() as $prize ) {
                echo '<li><a href="' . get_permalink( $prize->ID ) . '">' . $prize->post_title . '</a></li>';
            }

            echo "</ul><br><br><br>";

            echo "Drawing saved in DB with id #" . $drawing->save_drawing_to_db();
            do_action( $this->option_name . '_after_drawing', $drawing );

        }*/

        foreach ( $drawings as $drawing ) {

            $drawing->save_drawing_to_db();
            do_action( $this->option_name . '_after_drawing', $drawing );

        }

        /*
        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'admin/includes/class-bws-sweepstakes-drawing.php' );

        $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( 1 );

        print_r( $drawing->get_prizes() );*/

    }

    /**
     * Get the certain manual drawing by the AJAX request
     */
    public function get_certain_manual_drawing_cb() {

        $place = explode( ';', $_POST['cs'] );
        $place = array( 'city' => $place[0], 'state' => $place[1] );
        $data = array();

        $drawing_id = BWS_Sweepstakes_Drawing::get_drawing_id_by_boundaries( $place, time() );

        if ( $drawing_id === false ) {
            $drawing = BWS_Sweepstakes_Drawing::get_next_drawing_by_place( $place );
            $data['state'] = 'temp';
            $drawing->save_drawing_to_db();
        } else {
            $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( $drawing_id );
            if ( $drawing->get_state() == 'done' ) {
                $data['state'] = 'done';
            } else {
                $data['state'] = 'temp';
            }

        }

        $winnings = $drawing->get_winnings();

        foreach ( $winnings as $winner_id => $winning ) {

            $user_name = xprofile_get_field_data(1, $winner_id ) . ' ' . xprofile_get_field_data(6, $winner_id );
            $prizes = "";

            for( $i = 0; $i < count( $winning ); $i++ ) {
                $prizes .= '<a href="' . get_permalink( $winning[$i] ) . '" target="_blank">' . get_post( $winning[$i] )->post_title . '</a>';
                if ( $i != (count( $winning ) - 1) ) {
                    $prizes .= ', ';
                }
            }

            $winner_line = '<li>' . $user_name . " won " . $prizes . '</li>';
            $data[] = $winner_line;

        }

        $data['count'] = count( $winnings );

        echo json_encode( $data );

        die;

    }

    /**
     * Save the certain manual drawing by the AJAX request
     */
    public function save_certain_manual_drawing_cb() {

        $place = explode( ';', $_POST['cs'] );
        $place = array( 'city' => $place[0], 'state' => $place[1] );
        $data = array();

        $drawing_id = BWS_Sweepstakes_Drawing::get_drawing_id_by_boundaries( $place, time() );
        $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( $drawing_id );
        $drawing->set_time( time() );
        $result = $drawing->update_state( 'done' );

        if ( $result === false ) {
            echo 'error';
        } else {
            echo 'saved';
        }

        die;

    }

    /**
     * Send notifications for the certain manual drawing by AJAX request
     */
    public function send_notifications_for_certain_manual_drawing_cb() {

        $place = explode( ';', $_POST['cs'] );
        $place = array( 'city' => $place[0], 'state' => $place[1] );

        $drawing_id = BWS_Sweepstakes_Drawing::get_drawing_id_by_boundaries( $place, time() );
        $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( $drawing_id );

        do_action( $this->option_name . '_after_drawing', $drawing );

        die;

    }

    /**
     * Add the 'Redeem' button on top of the Check Voucher Page
     */
    public function woo_vou_add_redeem_btn_on_top() {

        echo '<tr class="woo-vou-voucher-code-submit-wrap">
  			<td>
  				<input type="submit" id="woo_vou_voucher_code_submit" name="woo_vou_voucher_code_submit" class="button-primary" value="'.__( "Redeem", "woovoucher" ).'"/>
  				<div class="woo-vou-loader woo-vou-voucher-code-submit-loader"><img src="' . WOO_VOU_IMG_URL . '/ajax-loader.gif"/></div>
  			</td>			
  		</tr>';

    }

    /**
     * Add drawing date to vouchers
     *
     * @param $voucher_template_html
     * @param $orderid
     * @param $item_key
     * @param $items
     * @param $voucodes
     * @param $productid
     * @return mixed
     */
    public function replace_drawing_date_shortcode_in_vaucher( $voucher_template_html, $orderid, $item_key, $items, $voucodes, $productid ) {

        $drawing_id = wc_get_order_item_meta( $item_key, '_drawing_id' );

        if( empty( $drawing_id ) ) {
            $drawing_date = "";
        } else {
            $drawing = BWS_Sweepstakes_Drawing::get_drawing_from_db( $drawing_id );
            $drawing_time = $drawing->get_time();
            $drawing_date = date( 'Y-m-d', $drawing_time );
        }

        $voucher_template_html = str_replace( '{drawing_date}', $drawing_date, $voucher_template_html );

        return $voucher_template_html;

    }

    /**
     * Replace the {shop_name} shortcode in vouchers
     *
     * @param $voucher_template_html
     * @param $orderid
     * @param $item_key
     * @param $items
     * @param $voucodes
     * @param $product_id
     * @return mixed
     */
    public function display_vendor_shop_name_in_voucher( $voucher_template_html, $orderid, $item_key, $items, $voucodes, $product_id ) {

        $vendor_id = get_post_meta( $product_id, '_woo_vou_vendor_user', true );
        $shop_name = get_user_meta( $vendor_id, 'pv_shop_name', true );

        $voucher_template_html = str_replace( '{shop_name}', $shop_name, $voucher_template_html );

        return $voucher_template_html;

    }

    /**
     * Custom voucher codes for coupons
     *
     * @param $voucode
     * @param $args
     * @return string
     */
    public function custom_unlimited_vou_code( $voucode, $args ) {

        $user_id = get_post_meta( $args['order_id'], '_customer_user', true );
        $vendor_id = get_post_meta( $args['data_id'], '_woo_vou_vendor_user', true );
        $shop_name = get_user_meta( $vendor_id, 'pv_shop_name', true );
        $shop_name = str_replace( ' ', '', $shop_name );
        $shop_name = mb_strtolower( $shop_name );

        $voucode = get_user_by( 'id', $user_id )->user_login[0] . $args['order_id'] . '-' . $shop_name;

        return $voucode;

    }

    /*
     *
     *
     *
     * DEPRECATED
     *
     *
     *
     */

    /**
     * Copy First Name and Last Name from wp_bp_xprofile_data to wp_usermeta
     * and save Zip-Code
     *
     *
    public function save_xp_to_umeta( $user_id ) {

        $first_name = xprofile_get_field_data( 1, $user_id );
        $last_name = xprofile_get_field_data( 6, $user_id );
        $zip_code = xprofile_get_field_data( 5, $user_id );

        update_user_meta( $user_id, 'first_name', sanitize_text_field( $first_name ) );
        update_user_meta( $user_id, 'last_name', sanitize_text_field( $last_name ) );
        update_user_meta( $user_id, 'billing_first_name', sanitize_text_field( $first_name ) );
        update_user_meta( $user_id, 'billing_last_name', sanitize_text_field( $last_name ) );
        update_user_meta( $user_id, 'billing_postcode', sanitize_text_field( $zip_code ) );

    }

    /**
     * Save Zip-Code from registration form to database
     *
     * @param $user_id
     *
    public function save_zip_code( $user_id ) {

        if ( isset( $_POST['zip_code'] ) ) {
            $zip_code = strval( $_POST['zip_code'] );
        } else {
            $zip_code = strval( xprofile_get_field_data( 5, $user_id ) );
        }

        $zip_code = strval( $zip_code );
        $zero_count = 5 - strlen( $zip_code );
        $zeros = "";

        for ( $digit_number = 0; $digit_number < $zero_count; $digit_number++ ) {
            $zeros .= "0";
        }

        $zip_code = $zeros . $zip_code;

        update_user_meta( $user_id, 'billing_postcode', sanitize_text_field( $zip_code ) );
        xprofile_set_field_data( 5, $user_id, sanitize_text_field( $zip_code ) );

    }

    /**
     * Add zip-code field to register form
     *
     * WP Social Login add-on
     *
    public function add_zip_code_field() {
        $zip_code = ( ! empty( $_POST['zip_code'] ) ) ? sanitize_text_field( $_POST['zip_code'] ) : '';
        ?>

        <p>
            <label for="zip_code"><?php _e( 'Zip-Code', 'bws-sweepstakes' ) ?><br />
                <input type="text" name="zip_code" id="zip_code" class="input" value="<?php echo esc_attr(  $zip_code  ); ?>" size="25" /></label>
        </p>

        <?php
    }

    /**
     * Validate the zip-code field
     *
    public function validate_zip_code_field( $errors, $sanitized_user_login, $user_email ) {

        if ( empty( $_POST['zip_code'] ) || ! empty( $_POST['zip_code'] ) && trim( $_POST['zip_code'] ) == '' ) {
            $errors->add('zip_code_error', sprintf('<strong>%s</strong>: %s', __('ERROR', 'bws-sweepstakes'), __('You must include a zip code.', 'mydomain')));
        }

        return $errors;
    }

    /**
     * Save the zip-code field to database
     *
    public function public_function_save_zip_code_field( $user_id ) {

        if ( ! empty( $_POST['zip_code'] ) ) {
            update_user_meta( $user_id, 'zip_code', sanitize_text_field( $_POST['zip_code'] ) );
        }

    }*/

    /**
     * Register query variables
     *
     * @param $vars
     * @return array
     *
    public function add_query_vars( $vars ) {

        $vars[] = 'zip';
        $vars[] = 'city';
        $vars[] = 'zc';
        $vars[] = 'drawing';

        return $vars;

    }*/

    /**
     * Filter prizes
     *
     * @param $query
     *
    public function filter_prizes( $query ) {

        if ( is_admin() || !$query->is_main_query() || ( !is_shop() && !is_product_category() ) ) {
            return;
        }

        $next_drawing = wp_next_scheduled( 'drawings_cron' );

        $meta_query = $query->get('meta_query');

        $meta_query[] = array(
            'key'       =>  '_prize',
            'value'     =>  'yes',
            'compare'   =>  '==',
        );

        $meta_query[] = array(
            'relation'  =>  'OR',
            array(
                'relation'  =>  'AND',
                array(
                    'key'       =>  $this->option_name . '_drawing_end_date',
                    'value'     =>  $next_drawing,
                    'compare'   =>  '>',
                    'type'      =>  'NUMERIC'
                ),
                array(
                    'key'       =>  $this->option_name . '_endless_drawing',
                    'value'     =>  'yes',
                    'compare'   =>  '!='
                )
            ),
            array(
                array(
                    'key'       =>  $this->option_name . '_endless_drawing',
                    'value'     =>  'yes',
                    'compare'   =>  '=='
                )
            )
        );

        $meta_query[] = array(
            'key'       =>  $this->option_name . '_drawing_frequency',
            'value'     =>  'stop',
            'compare'   =>  '!='
        );

        $query->set( 'meta_query', $meta_query );

        $query->set( 'orderby', 'meta_value_num' );
        $query->set( 'meta_key', '_regular_price' );
        $query->set( 'order', 'DESC' );

        $zip = get_query_var( 'zip' );
        $city = get_query_var( 'city' );

        $vendors = BWS_Sweepstakes_Share_Functions::get_vendors();
        $vendors_ids = array( 0 );

        if ( !empty( $zip ) ) {

            foreach ( $vendors as $vendor ) {
                if ( $zip == $vendor->zip_code ) {
                    $vendors_ids[] = $vendor->id;
                }
            }

            $query->set( 'author__in', $vendors_ids );

        } else if ( !empty( $city ) ) {

            foreach ( $vendors as $vendor ) {
                if ( $city == $vendor->city ) {
                    $vendors_ids[] = $vendor->id;
                }
            }

            $query->set( 'author__in', $vendors_ids );

        }

        $query->set( 'orderby', 'meta_value_num' );
        $query->set( 'meta_key', '_regular_price' );
        $query->set( 'order', 'DESC' );

        //return $query;

    }*/

    /**
     * Sort prizes
     *
     * @param $query_args
     * @return mixed
     */
    public function sort_prizes( $query_args ) {

        $query_args['orderby'] = 'meta_value_num';
        $query_args['meta_key'] = '_regular_price';
        $query_args['order'] = 'DESC';

        return $query_args;

    }

    public function save_zip_to_cat_url( $list_args ) {

        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/class-bws-product-cat-list-walker.php' );

        return $list_args;

    }

    /**
     * Register additional widgets
     */
    public function add_widgets(  ) {

        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/class-bws-widget-product-categories.php' );
        include_once( plugin_dir_path( dirname( __FILE__ ) ) . 'public/includes/class-bws-widget-location.php' );

        register_widget( 'BWS_Widget_Product_Categories' );
        register_widget( 'BWS_Sweepstakes_Widget_Location' );

    }

    /**
     * Saving prize fields
     *
     * @param $post_id
     *
     * TODO: Create verification for fields (SQL Injection etc)
     *
    public function save_prize_fields( $post_id ) {

        if ( isset( $_POST['_prize'] ) ) {

            $drawing_frequency = $_POST[$this->option_name . '_drawing_frequency'];
            $drawing_end_date_str = $_POST[$this->option_name . '_drawing_end_date'];
            $endless_drawing = $_POST[$this->option_name . '_endless_drawing'];
            $drawing_end_date = strtotime( $drawing_end_date_str );

            $drawing_frequency_old = get_post_meta( $post_id, $this->option_name . '_drawing_frequency', true );
            $drawing_end_date_old = get_post_meta( $post_id, $this->option_name . '_drawing_end_date', true );
            $endless_drawing_old = get_post_meta( $post_id, $this->option_name . '_endless_drawing', true );

            if ( !empty( $drawing_end_date_old ) && !empty( $drawing_frequency_old ) && !empty( $endless_drawing_old ) && $drawing_frequency == $drawing_frequency_old && $drawing_end_date == $drawing_end_date_old && $endless_drawing == $endless_drawing_old ) {
                return;
            }

            update_post_meta( $post_id, '_prize', 'yes' );
            update_post_meta( $post_id, $this->option_name . '_drawing_last_run_date', 0 );
            update_post_meta( $post_id, $this->option_name . '_drawing_frequency', $drawing_frequency );

            if ( isset(  $_POST[$this->option_name . '_endless_drawing'] ) ) {
                update_post_meta( $post_id, $this->option_name . '_endless_drawing', 'yes' );
            } else {
                update_post_meta( $post_id, $this->option_name . '_endless_drawing', 'no' );
            }

            update_post_meta( $post_id, $this->option_name . '_drawing_end_date', $drawing_end_date );

        } else {

            update_post_meta( $post_id, '_prize', 'no' );

        }

    }*/

    public function debug() {

        $users = new WP_User_Query( array(
            'search'         => esc_attr( 'te' ).'*',
            'search_columns' => array(
                'user_login',
            ),
        ) );
        $users_found = $users->get_results();

        print_r(  $users_found  );

    }

    public function check_vou_args( $args ) {

        $mail = print_r( $args, true );
        wp_mail( 'turgenoid@gmail.com', 'test_vou', $mail );
        return $args;

    }

    public function show_next_drawing_time() {

        $ds = BWS_Sweepstakes_Drawing_Settings::get_drawing_settings( array( 'city' => 'Albuquerque', 'state' => 'New Mexico' ) );
        $boundaries = $ds->get_next_drawing_day_boundaries();

        $dt = new DateTime();
        $dt->setTimestamp( $boundaries['planned_time'] );
        $tz = new DateTimeZone( 'America/Denver' );
        $dt->setTimezone( $tz );
        $datetime = $dt->format( 'l, M jS, Y \a\t g:i A (T)' );

        echo '<div class="next-drawing-time"><h2>Next drawing: ' . $datetime . '</h2></div>';

    }

    public function vendor_shop_query( $q, $that ) {

        $vendor_shop = urldecode( get_query_var( 'vendor_shop' ) );
        if ( empty( $vendor_shop ) ) {
            return;
        }

        $vendor_id   = WCV_Vendors::get_vendor_id( $vendor_shop );
        if ( !$vendor_id ) {
            $q->set_404();
            status_header( 404 );

            return;
        }

        add_filter( 'woocommerce_page_title', array( 'WCV_Vendor_Shop', 'page_title' ) );

        $q->set( 'author', $vendor_id );

    }

    public static function add_rewrite_rules() {

        $permalink = untrailingslashit( WC_Vendors::$pv_options->get_option( 'vendor_shop_permalink' ) );

        // Remove beginning slash
        if ( substr( $permalink, 0, 1 ) == '/' ) {
            $permalink = substr( $permalink, 1, strlen( $permalink ) );
        }

        add_rewrite_tag( '%vendor_shop%', '([^&]+)' );

        add_rewrite_rule( $permalink . '/([^/]*)/page/([0-9]+)', 'index.php?post_type=product&vendor_shop=$matches[1]&paged=$matches[2]', 'top' );
        add_rewrite_rule( $permalink . '/([^/]*)', 'index.php?post_type=product&vendor_shop=$matches[1]', 'top' );

    }

}


