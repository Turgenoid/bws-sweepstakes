<?php
$user_id = get_current_user_id();
$user_winnings = BWS_Sweepstakes_Share_Functions::get_user_winnings( $user_id );
$users_for_rating_table = $this->get_users_ratings_for_dashboard( $user_id, 9 );
$referral_page_id = get_option( $this->option_name . '_referral_page' );
$referral_link = do_shortcode( '[mycred_affiliate_link url="' . get_permalink( $referral_page_id ) . '"]' );
$instruction_page_id = get_option( $this->option_name . '_instruction_page' );
$vendors_dashboard_page_id = get_option( 'wcvendors_dashboard_page_id' );
$mycred_points = mycred_get_users_reference_sum( $user_id );
$log = new myCRED_Query_Log( array(
    'ref'       =>  'signup_referral',
    'user_id'   =>  $user_id,
) );
$referrals = $log->results;
for ( $i = 0; $i < count( $referrals ); $i++ ) {
    if ( get_user_by( 'id', $referrals[$i]->ref_id ) === false ) {
        unset( $referrals[$i] );
    }
}

function rank_is_user( $user_id, $user ) {
    if ( $user_id == $user->id ) {
        return 'class="dashboard-owner"';
    }
}
?>

<div class="users-dashboard-wrapper">
    <p><a href="<?php echo get_permalink( $vendors_dashboard_page_id ); ?>">Become</a> a Seller!</p>
    <div>
        <h3>Winnings</h3>

        <?php
        if ( empty( $user_winnings ) ) {
            echo '<p>Sorry, but you don\'t have any winnings. Maybe luck next time?</p>';
        } else {
            echo '<table>';
            echo '<tr><th>Prize</th><th>Winning date</th><th>Link</th></tr>';

            foreach ( $user_winnings as $winning ) {

                echo '<tr>';
                echo '<td><a href="' . get_permalink( $winning['prize_id'] ) . '" target="_blank">' . get_the_title( $winning['prize_id'] ) . '</a></td>';
                echo '<td>' . date( 'Y-m-d', $winning['drawing_time'] ) . '</td>';

                if( isset( $winning['link_to_download'] ) ) {
                    echo '<td><a href="'. $winning['link_to_download'] .'">Download this Voucher</a></td>';
                } else {
                    echo '<td><a href="' . home_url() . "/checkout/?add-to-cart=" . $winning['prize_id'] . "&drawing=" . $winning['drawing_id'] . '">Get this Prize*</a></td>';
                }

                echo '</tr>';

            }

            echo '</table>';
            echo '<p>* To download prize vouchers, click "Get this prize". This will take you to a checkout page where you must "purchase" the vouchers at no cost. Then you\'ll be redirected back to this page where you can download the prize voucher.</p>';
        }
        ?>

    </div>

    <div>
        <h3>Your Points</h3>

        <table class="users-ratings">
            <tr>
                <th>Referring Friends</th>
                <th>Viewing Products</th>
                <th>Purchasing Products</th>
                <th>Total</th>
            </tr>
            <tr>
                <td><span class="points-number"><?php echo (empty( $mycred_points['signup_referral'] ) ? 0 : $mycred_points['signup_referral']); ?></span></td>
                <td><span class="points-number"><?php echo (empty( $mycred_points['view_content'] ) ? 0 : $mycred_points['view_content']); ?></span></td>
                <td><span class="points-number"><?php echo (empty( $mycred_points['purchasing_product'] ) ? 0 : $mycred_points['purchasing_product']); ?></span></td>
                <td><span class="points-number"><?php echo mycred_get_users_balance( $user_id ); ?></span></td>
            </tr>
        </table>

        <table class="users-ratings">
            <tr><th>Position</th><th>Name</th><th>Points</th></tr>
            <?php

            foreach ( $users_for_rating_table as $user ) {

                echo '<tr '. rank_is_user( $user_id, $user ) .'>';
                echo '<td>'. $user->rank_position .'</td><td>'. $user->first_name .'</td><td>'. $user->points .'</td>';
                echo '</tr>';

            }

            ?>
        </table>

        <h3>Your Referrals</h3>
        <?php

        if ( count( $referrals ) > 0 ) {

            echo "<ol>";

            foreach ( $referrals as $referral ) {
                $first_name = xprofile_get_field_data(1, $referral->ref_id);
                $last_name = xprofile_get_field_data(6, $referral->ref_id);
                echo "<li>$first_name $last_name</li>";
            }

            echo "</ol>";

        } else {

            echo "<p>Sorry, but you have not invited anyone yet. Give your referral link to your friends!</p>";

        }

        ?>
        <h3>Earn More Points</h3>
        <!--<p>1 point = 1 successful referral </p>-->
        <?php echo get_post( $instruction_page_id )->post_content; ?>
        <div class="referral-link-wrapper">
        <h4>Your referral link</h4>
            <input id="user-referral-link" value="<?php echo $referral_link; ?>">
            <button class="button" id="copy-referral-link-button" data-clipboard-target="#user-referral-link">Copy</button>
        </div>
    </div>

</div>