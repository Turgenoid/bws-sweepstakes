<?php
$user_id = get_current_user_id();
$user_vouchers = BWS_Sweepstakes_Share_Functions::get_user_vouchers( $user_id );
?>
<div class="vtp-wrapper">

    <div class="vouchers-board">
    <?php

    foreach( $user_vouchers as $voucher ) {

        $expiration_date = ( $voucher['expiration_date'] == 'endless' || $voucher['expiration_date'] == 'Never' ) ? 'Never' : date( 'F j, Y', strtotime( $voucher['expiration_date'] ) );

        echo '<div class="voucher-wrapper">';
        echo '<h4>' . $voucher['product_name'] . '</h4>';
        //echo '<p>Status: <b>' . $voucher['status'] . '</b></p>';
        echo '<p>Code: <b>' . $voucher['code'] . '</b></p>';

        if( !empty( $voucher['store'] ) )
            echo '<p>Store: <b>' . $voucher['store'] . '</b></p>';

        echo '<p>Date of purchase: <b>' . date( 'F j, Y', strtotime( $voucher['purchase_date'] ) ) . '</b></p>';
        echo '<p>Expiration date: <b>' . $expiration_date . '</b></p><br>';

        if( !empty( $voucher['vendor_address'] ) )
            echo '<p>Redemption address:<br><b>' . $voucher['vendor_address'] . '</b></p><br>';

        echo '<p><a href="' . $voucher['download_url'] . '" target="_blank">Download</a></p>';
        echo '</div>';
        
    }
    
    ?>
    </div>

</div>
